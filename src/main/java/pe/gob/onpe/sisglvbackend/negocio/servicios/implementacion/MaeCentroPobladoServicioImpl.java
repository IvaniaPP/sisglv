package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeCentroPobladoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeCentroPobladoServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class MaeCentroPobladoServicioImpl implements MaeCentroPobladoServicio{


	@Autowired
	private MaeCentroPobladoMapper maeCentroPobladoMapper;
	
	@Override
	public void listarCentroPobladoPorUbigeo(MaeCentroPoblado centroPoblado) throws Exception {
		maeCentroPobladoMapper.listarCentroPobladoPorUbigeo(centroPoblado);
		Funciones.validarOperacionConBaseDatos(centroPoblado, MaeCentroPobladoServicioImpl.class, "listarCentroPobladoPorUbigeo");
		
	}
	
	@Override
	public void listarCentroPobladoPorIdUbigeo(MaeCentroPoblado centroPoblado) throws Exception {
		maeCentroPobladoMapper.listarCentroPobladoPorIdUbigeo(centroPoblado);
		Funciones.validarOperacionConBaseDatos(centroPoblado, MaeCentroPobladoServicioImpl.class, "listarCentroPobladoPorIdUbigeo");
		
	}

}

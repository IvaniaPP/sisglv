package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;


import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;

@Mapper
public interface MaeAmbitoElectoralMapper {
	void listarAmbitoPorTipo(MaeAmbitoElectoral param) throws Exception;
	void crearMaeAmbitoElectoral(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	void obtenerMaeAmbitoElectoralPorId(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	void obtenerMaeAmbitoElectoralPorNombre(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	void listarAmbitoElectoralPorProcesoElectoral(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	void listarAmbitoElectoralPorProcesoTipo(MaeAmbitoElectoral maeAmbitoElectoral) throws Exception;
	

}

package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.beans.*;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ReporteMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ReporteService;
import pe.gob.onpe.sisglvbackend.transversal.properties.ReporteProperties;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.reporte.ValidarClaveReporteInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte.*;


@Service
public class ReporteServiceImpl implements ReporteService{
	
	@Autowired
	private ReporteMapper reporteMapper;

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	ReporteProperties reporteProperties;

	
	@Override
	public void reporteRegistrosOrc(ReporteRegistrosAmbitoElectoral reporte) throws Exception {
		reporteMapper.reporteRegistrosOrc(reporte);
		Funciones.validarOperacionConBaseDatos(reporte, ReporteRegistrosAmbitoElectoral.class, "reporteRegistrosOrc");
	}


	@Override
	public void reporteRegistrosRegistrador(ReporteRegistrosPorRegistrador reporte) throws Exception {
		reporteMapper.reporteRegistrosRegistrador(reporte);
		Funciones.validarOperacionConBaseDatos(reporte, ReporteRegistrosPorRegistrador.class, "reporteRegistrosRegistrador");
		
	}


	@Override
	public void reporteRegistrosPorSeccion(ReporteSeccionesSisglv reporte) throws Exception {
		reporteMapper.reporteRegistrosPorSeccion(reporte);
		Funciones.validarOperacionConBaseDatos(reporte, ReporteSeccionesSisglv.class, "reporteRegistrosPorSeccion");
		
	}


	@Override
	public ListaReporteSeccionABCDEOutputDto reporteSeccionesABCDE() throws Exception {
		ReporteSeccionAB reporteSeccionAB = new ReporteSeccionAB();
		ReporteSeccionC reporteSeccionC = new ReporteSeccionC();
		ReporteSeccionD reporteSeccionD = new ReporteSeccionD();
		ReporteSeccionE reporteSeccionE = new ReporteSeccionE();

		reporteMapper.reporteSeccionAB(reporteSeccionAB);
		Funciones.validarOperacionConBaseDatos(reporteSeccionAB, ReporteSeccionesSisglv.class, "reporteSeccionesABCDE");

		reporteMapper.reporteSeccionC(reporteSeccionC);
		Funciones.validarOperacionConBaseDatos(reporteSeccionC, ReporteSeccionesSisglv.class, "reporteSeccionesABCDE");

		reporteMapper.reporteSeccionD(reporteSeccionD);
		Funciones.validarOperacionConBaseDatos(reporteSeccionD, ReporteSeccionesSisglv.class, "reporteSeccionesABCDE");

		reporteMapper.reporteSeccionE(reporteSeccionE);
		Funciones.validarOperacionConBaseDatos(reporteSeccionE, ReporteSeccionesSisglv.class, "reporteSeccionesABCDE");

		return ListaReporteSeccionABCDEOutputDto.builder()
				.seccionAB(Funciones.mapAll(reporteSeccionAB.getLista(), ReporteSeccionABOutputDto.class))
				.seccionC(Funciones.mapAll(reporteSeccionC.getLista(), ReporteSeccionCOutputDto.class))
				.seccionD(Funciones.mapAll(reporteSeccionD.getLista(), ReporteSeccionDOutputDto.class))
				.seccionE(Funciones.mapAll(reporteSeccionE.getLista(), ReporteSeccionEOutputDto.class))
				.build();
	}

	@Override
	public ValidarClaveReporteOutputDto validarClaveReporte(ValidarClaveReporteInputDto validarClaveReporteInputDto) {
		if(reporteProperties.getClave() != null ) {
			if(reporteProperties.getClave().trim().equals(validarClaveReporteInputDto.getClave().trim())){
				return ValidarClaveReporteOutputDto.builder().claveValidado(true).mensajeValidacion("Clave correcta").build();
			} else {
				return ValidarClaveReporteOutputDto.builder().claveValidado(false).mensajeValidacion("Clave no autorizada").build();
			}
		} else {
			return ValidarClaveReporteOutputDto.builder().claveValidado(false).mensajeValidacion("La clave del reporte no está configurada.").build();
		}
	}
}

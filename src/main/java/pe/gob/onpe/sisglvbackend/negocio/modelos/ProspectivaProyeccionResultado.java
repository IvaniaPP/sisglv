package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProspectivaProyeccionResultado extends ModeloBase {
    Integer idPadronProyeccion;
    Integer tipoAmbito;
    Integer idPadronParametro;

    String ubigeo;
    String departamento;
    String provincia;
    String distrito;
    Double proyeccion;
    Double proyeccionDiscapacitado;

    List<ProspectivaProyeccionResultado> lista;
}

package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;


import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;

public interface MaeUbigeoServicio {
	
	
	public void listarDepartamento(MaeUbigeo maeUbigeo) throws Exception;
	public void listarProvincia(MaeUbigeo maeUbigeo) throws Exception;
	public void listarDistrito(MaeUbigeo param)throws Exception;
	
	public void listarDepartamentoPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception;
	
	public void listarProvinciaPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception;
	
	public void listarDistritoPorProcesoAmbito(MaeUbigeo maeUbigeo) throws Exception;
}

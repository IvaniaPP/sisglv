package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaGenerica;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.FichaGenericaMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.FichaGenericaServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

/**
 * @author glennlq
 * @created 9/27/21
 */
@Service
public class FichaGenericaServicioImpl implements FichaGenericaServicio {

    @Autowired
    private FichaGenericaMapper fichaGenericaMapper;

    @Override
    public FichaGenerica copiarFichasABCDE(final FichaGenerica fichaGenerica) throws Exception {
        //fichaGenericaMapper.copiarFichasABCDE(fichaGenerica);
        fichaGenericaMapper.copiarFichasABCDEPorLocalVotacion(fichaGenerica);
        Funciones.validarOperacionConBaseDatos(fichaGenerica, FichaGenericaServicioImpl.class, "copiarFichasABCDE");
        return fichaGenerica;
    }

}

package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetFichaSeccionD extends ModeloBase {

    private Integer idDetFichaSeccionD;
    private CabFichaSeccionD cabFichaSeccionD;
    private Integer tipoArea;
    private String descripcionTipoArea;
    private Integer largo;
    private Integer ancho;
    private String referencia;
    private Integer tieneTecho;

    private Integer tienePuerta;
    private Integer cantidadPuerta;
    private Integer tieneAccesoLuz;

    private List<DetFichaSeccionD> lista;
}

package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionD;

public interface RegistroSeccionDServicio {
	
	
	public void registrarSeccionD(DetRegistroSeccionD param) throws Exception;
	
	public void listarSeccionD(DetRegistroSeccionD param) throws Exception;
	
	public void actualizarSeccionD(DetRegistroSeccionD param ) throws Exception;
	
	public void listarCabeceraPorVerificacion(CabRegistroSeccionD param) throws Exception;
	
	public void eliminarArea(DetRegistroSeccionD param) throws Exception;

	public void validarSeccioD(CabRegistroSeccionD param);
	
	public void actualizarEstadoTerminado(CabRegistroSeccionD param);

}

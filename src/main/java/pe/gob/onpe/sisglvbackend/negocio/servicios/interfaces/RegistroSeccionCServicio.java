package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionC;

public interface RegistroSeccionCServicio {
	
	
	public void registrarSeccionC(DetRegistroSeccionC datosAula) throws Exception;
	
	public void listarSeccionC(DetRegistroSeccionC datosAula) throws Exception;
	
	public void actualizarSeccionC(DetRegistroSeccionC datosAula ) throws Exception;
	
	public void listarCabeceraRegistroSeccionC(CabRegistroSeccionC cabecera) throws Exception;

	public void eliminarAula(DetRegistroSeccionC param)throws Exception;

	public void validarSeccionC(CabRegistroSeccionC param);

	public void actualizarEstadoTerminado(CabRegistroSeccionC param);
}

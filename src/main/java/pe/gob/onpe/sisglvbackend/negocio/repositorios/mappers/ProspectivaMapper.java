package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.*;

@Mapper
public interface ProspectivaMapper {

	public void listarPeriodos(Periodo periodo);
	void proyeccionParte1(ProspectivaProyeccionParte1 proyeccion);
	void proyeccionParte2(ProspectivaProyeccionParte2 proyeccion);
	void generarProyeccionParte1(GenerarProyeccionParte1 proyeccion);
	void generarProyeccionParte2(GenerarProyeccionParte2 proyeccion);
	void obtenerEstadisticaResultado(ProspectivaEstadisticaResultado proyeccion);
	void obtenerProyeccionResultado(ProspectivaProyeccionResultado proyeccion);

	public void guardarSeleccion(ProspectivaSeleccion prospectivaSeleccion);
	public void ejecutarProspectiva(ProspectivaEjecucion prospectivaEjecucion);
	public void ejecutarResultadoNacional(ProspectivaResultadoNacional prospectivaResultado);
	public void ejecutarResultadoExtranjero(ProspectivaResultadoExtranjero prospectivaResultado);
	
}

package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionA;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.RegistroSeccionAMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionAServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class RegistroSeccionAServicioImpl implements RegistroSeccionAServicio {

    @Autowired
    RegistroSeccionAMapper registroSeccionAMapper;

    @Override
    public void registrarDatosGeneralesPrimerPaso(RegistroSeccionA param) {
        // TODO Auto-generated method stub
        registroSeccionAMapper.registrarDatosGeneralesPrimerPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "registrarDatosGeneralesPrimerPaso");
    }

    @Override
    public void registrarDatosGeneralesSegundoPaso(RegistroSeccionA param) {
        // TODO Auto-generated method stub
        registroSeccionAMapper.registrarDatosGeneralesSegundoPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "registrarDatosGeneralesSegundoPaso");
    }

    @Override
    public void registrarDatosGeneralesTercerPaso(RegistroSeccionA param) {
        // TODO Auto-generated method stub
    	System.out.println("=========>"+param.getDistanciaCentroDistrito());
        registroSeccionAMapper.registrarDatosGeneralesTercerPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "registrarDatosGeneralesTercerPaso");
    }

    @Override
    public void registrarDatosGeneralesCuartoPaso(RegistroSeccionA param) {
        // TODO Auto-generated method stub
        registroSeccionAMapper.registrarDatosGeneralesCuartoPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "registrarDatosGeneralesCuartoPaso");
    }

    @Override
    public RegistroSeccionA obtenerDatosGeneralesPrimerPaso(RegistroSeccionA param) {

        registroSeccionAMapper.obtenerDatosGeneralesPrimerPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "obtenerDatosGeneralesPrimerPaso");

        return param.getRegistrosSeccionA().size() == 0 ? null : param.getRegistrosSeccionA().get(0);
    }

    @Override
    public RegistroSeccionA obtenerDatosGeneralesSegundoPaso(RegistroSeccionA param) {

        registroSeccionAMapper.obtenerDatosGeneralesSegundoPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "obtenerDatosGeneralesSegundoPaso");
        return param.getRegistrosSeccionA().size() == 0 ? null : param.getRegistrosSeccionA().get(0);
    }

    @Override
    public RegistroSeccionA obtenerDatosGeneralesTercerPaso(RegistroSeccionA param) {

        registroSeccionAMapper.obtenerDatosGeneralesTercerPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "obtenerDatosGeneralesTercerPaso");
        return param.getRegistrosSeccionA().size() == 0 ? null : param.getRegistrosSeccionA().get(0);
    }

    @Override
    public RegistroSeccionA obtenerDatosGeneralesCuartoPaso(RegistroSeccionA param) {

        registroSeccionAMapper.obtenerDatosGeneralesCuartoPaso(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "obtenerDatosGeneralesCuartoPaso");
        return param.getRegistrosSeccionA().size() == 0 ? null : param.getRegistrosSeccionA().get(0);
    }

    @Override
    public void ListarLVActivos(RegistroSeccionA param) {
        registroSeccionAMapper.listarLVActivos(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "listarLVActivos");
    }

    @Override
    public void ListarFiltroLVActivos(RegistroSeccionA param) {
        registroSeccionAMapper.listarLVActivos(param);
        Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "listarFiltroLVActivos");
    }

	@Override
	public RegistroSeccionA obtenerDatosGenerales(RegistroSeccionA param) {
		  registroSeccionAMapper.obtenerDatosGenerales(param);
	    Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "obtenerDatosGenerales");
	     return param.getRegistrosSeccionA().size() == 0 ? null : param.getRegistrosSeccionA().get(0);

	}

	@Override
	public void validarSeccionA(RegistroSeccionA param) {
	 registroSeccionAMapper.validarSeccionA(param);
	 Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "validarSeccionA");
		
	}

	@Override
	public void actualizarDatosGeneralesPrimerPaso(RegistroSeccionA param) {
		// TODO Auto-generated method stub
		 registroSeccionAMapper.actualizarDatosGeneralesPrimerPaso(param);
		 Funciones.validarOperacionConBaseDatos(param, RegistroSeccionAServicioImpl.class, "actualizarDatosGeneralesPrimerPaso");
		
	}

}

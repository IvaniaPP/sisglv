package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FiltroRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.CabRegistroSeccionEMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CabRegistroSeccionEServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.SFTPService;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class CabRegistroSeccionEServicioImpl implements CabRegistroSeccionEServicio {

	@Autowired
	private SFTPService ftpService;
	
	@Autowired
	private CabRegistroSeccionEMapper cabRegistroSeccionEMapper;

	
	@Override
	@Transactional(readOnly = false)
	public void listarArchivos(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.listarArchivos(cabRegistroSeccionE);
	} // end

	@Override
	@Transactional(readOnly = false)
	public void obtenerArchivoPorId(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.obtenerArchivoPorId(cabRegistroSeccionE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void desactivarArchivo(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.desactivarArchivo(cabRegistroSeccionE);
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void agregarArchivo(CabRegistroSeccionE cabRegistroSeccionE) {
		ftpService.uploadFileToFTP(
				cabRegistroSeccionE.getArchivoAgregado().getArchivoStream(), 
				cabRegistroSeccionE.getArchivoAgregado().getPath(), 
				cabRegistroSeccionE.getArchivoAgregado().getFilenameOriginal());
		cabRegistroSeccionEMapper.agregarArchivo(cabRegistroSeccionE);
	}

	@Override
	public void validarRegistroSeccionE(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.validarRegistroSeccionE(cabRegistroSeccionE);
	}

	@Override
	@Transactional(readOnly = false)
	public void listarPorId(FiltroRegistroSeccionE filtroRegistroSeccionE) {
		cabRegistroSeccionEMapper.listarPorId(filtroRegistroSeccionE);
	}

	@Override
	@Transactional
	public void actualizarObservacionReg(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.actualizarObservacionReg(cabRegistroSeccionE);
	}

	@Override
	@Transactional
	public void actualizarObservacionLv(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.actualizarObservacionLv(cabRegistroSeccionE);
	}

	@Override
	public void listarCategoriaArchivoSeccionE(CabRegistroSeccionE cabRegistroSeccionE) {
		cabRegistroSeccionEMapper.listarCategoriaArchivoSeccionE(cabRegistroSeccionE);
		Funciones.validarOperacionConBaseDatos(cabRegistroSeccionE, CabRegistroSeccionEServicioImpl.class, "listarCategoriaArchivoSeccionE");
		
	}

}

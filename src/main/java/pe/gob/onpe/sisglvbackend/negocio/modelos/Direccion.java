package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Direccion {
	private Integer idTipoVia;
	private String nombreTipoVia;
	private String nombreVia;
	private Integer numero;
	private Float kilometro;
	private String interior;
	private String departamento;
	private String manzana;
	private String lote;
	private Integer piso;
	
	private Integer idTipoZona;
	private String nombreTipoZona;
	private String nombreZona;
	private String referencia;
}

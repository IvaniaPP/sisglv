package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;

@Mapper
public interface CabRegistroSeccionCMapper {
	
	 void listarPorVerificacion(CabRegistroSeccionC cabRegistroSeccionC) throws Exception;
	 void validarSeccionC(CabRegistroSeccionC param);
	 void actualizarEstadoTerminado(CabRegistroSeccionC cabRegistroSeccionC);

}

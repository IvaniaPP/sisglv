package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MaeLocalVotacion extends ModeloBasePaginacion {
	private Integer idLocalVotacion;
	private MaeUbigeo maeUbigeo;
	
	//Codigo comentado, se encuentra en su propia seccion
	private Integer idTipoLocal;
	private String  nombreLocal;
	private String  numeroLocal;
	private Integer idAmbitoElectoral;
	private Integer idTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String  otroCategoriaInstitucion;
	private Integer idNivelInstitucion;
	private String nombre;
	private Direccion direccion;
	
	private Integer idEstado;
	private List<MaeLocalVotacion> localesVotaciones;
	private FichaSeccionA fichaSeccionA;
	private FichaSeccionB fichaSeccionB;
	private CabFichaSeccionC cabFichaSeccionC;
	private CabFichaSeccionD cabFichaSeccionD;
	private CabFichaSeccionE cabFichaSeccionE;
}

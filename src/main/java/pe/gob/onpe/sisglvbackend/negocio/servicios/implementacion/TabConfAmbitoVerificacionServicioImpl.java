package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.TabConfAmbitoVerificacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfAmbitoVerificacionServicio;

@Service
public class TabConfAmbitoVerificacionServicioImpl implements TabConfAmbitoVerificacionServicio {

	@Autowired
	private TabConfAmbitoVerificacionMapper tabConfAmbitoVerificacionMapper;
	
	@Override
	public TabConfAmbitoVerificacion crearTabConfAmbitoVerificacion(TabConfAmbitoVerificacion tabConfAmbitoVerificacion)
			throws Exception {
		tabConfAmbitoVerificacionMapper.crearTabConfAmbitoVerificacion(tabConfAmbitoVerificacion);
		return tabConfAmbitoVerificacion;
	}

}

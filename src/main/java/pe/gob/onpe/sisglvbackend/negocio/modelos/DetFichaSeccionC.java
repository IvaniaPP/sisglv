package pe.gob.onpe.sisglvbackend.negocio.modelos;


import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetFichaSeccionC extends ModeloBase {

    private Integer idDetFichaSeccionC;
    private CabFichaSeccionC cabFichaSeccionC;

    private String aula;
    private String pabellon;
    private Integer piso;
    private Integer requiereToldo;
    private Integer usoAula;
    private Integer cantidadMesa;

    private Integer tipoAula;
    private String descripcionTipoAula;

    private Integer tieneLuminaria;
    private Integer cantidadLuminariaBuenEstado;
    private Integer cantidadLuminariaMalEstado;

    private Integer tieneTomaCorriente;
    private Integer cantidadTomacorrienteBuenEstado;
    private Integer cantidadTomacorrienteMalEstado;

    private Integer tieneInterruptor;
    private Integer cantidadInterruptorBuenEstado;
    private Integer cantidadInterruptorMalEstado;

    private Integer tienePuerta;
    private Integer cantidadPuertaBuenEstado;
    private Integer cantidadPuertaMalEstado;

    private Integer tieneVentana;
    private Integer cantidadVentanaBuenEstado;
    private Integer cantidadVentanaMalEstado;

    private Integer tienePuntoInternet;
    private Integer tieneAccesoWifi;
    
    private List<DetFichaSeccionC> lista;
}

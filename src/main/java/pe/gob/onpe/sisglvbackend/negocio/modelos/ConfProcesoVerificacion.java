package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConfProcesoVerificacion extends ModeloBase {
	private Integer idConfProcesoVerificacion;
	private MaeProcesoVerificacion maeProcesoVerificacion;
	private MaeAmbitoElectoral maeAmbitoElectoral;
	private Integer notificacion;
}

package pe.gob.onpe.sisglvbackend.negocio.beans;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ModeloBase;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReporteRegistrosAmbitoElectoral extends ModeloBase{
	
	private String ubigeo;
	private String orc;
	private String departamento;
	private String provincia;
	private String distrito;
	private String tipoRegistro;
	private Integer totalRegistros;
	private Integer esNuevo;
	
	 private List<ReporteRegistrosAmbitoElectoral> reporte;

}

package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCargaArchivo;

/**
 * @author glennlq
 * @created 9/23/21
 */

@Mapper
public interface CabCargaArchivoMapper {
    CabCargaArchivo obtenerCabCargaArchivoPorId(CabCargaArchivo param) throws Exception;
    CabCargaArchivo crearCabCargaArchivo(CabCargaArchivo param) throws Exception;
}

package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import java.util.HashMap;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Usuario;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotAuthorizedException;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.LoginInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.RefrescarTokenOutputDto;

public interface UsuarioServicio {
	
	
  Usuario obtenerInfoUsuarioPorUsuarioAplicacion (Integer idUsuario, Integer idAplicacion);

  
  void validarSesionActiva(Usuario usuario);
  
  
  Usuario obtenerUsuarioPorNumeroDocumento(String numeroDocumento);
  
  
  LoginDatosOutputDto accederSistema(LoginInputDto input) throws Exception;

  CargarAccesoDatosOutputDto cargarAccesos(CargarAccesosInputDto input, String token ) throws Exception;
  
  BaseOutputDto asignarPersonaAUsuarioGenerardo (AsignarPersonaUsuarioGeneradoInputDto input, String token) throws Exception;
  
  HashMap<String, Object> actualizarNuevaClave(ActualizarNuevaClaveInputDto input, String token ) throws Exception;
  
  RefrescarTokenOutputDto refrescarToken(String token) throws Exception;
  
}

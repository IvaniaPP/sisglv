package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabCatalogo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.DetCatalogoEstructuraMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CatalogoServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteCatalogo;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarCatalogoBasicoOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCatalogoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarCatalogoBasicoOuputDto;

import java.util.List;

@Service
public class CatalogoServicioImpl implements CatalogoServicio{
    
    @Autowired
    DetCatalogoEstructuraMapper catalogoMapper;

    @Override
    public void listarCatalogosActivos(DetCatalogoEstructura param) throws Exception {
        catalogoMapper.listarCatalogosActivos(param);
        Funciones.validarOperacionConBaseDatos(param, CatalogoServicioImpl.class, "listarCatalogosActivos");
    }

    @Override
    public ResponseListarCatalogoBasicoOuputDto listarCatalogosActivosPorMaestroYColumna(String maestro, String columna) throws Exception {
        DetCatalogoEstructura param = buildDetCatalogoEstructura(maestro, columna);
        this.listarCatalogosActivos(param);
        return buildResponseListarCatalogoBasicoOuputDto(param);
    }

    private DetCatalogoEstructura buildDetCatalogoEstructura(String maestro, String columna) throws Exception {
        DetCatalogoEstructura param = new DetCatalogoEstructura();
        CabCatalogo catalogo = new CabCatalogo();
        catalogo.setMaestro(maestro);
        param.setCabCatalogo(catalogo);
        param.setColumna(columna);
        return param;
    }

    private ResponseListarCatalogoBasicoOuputDto buildResponseListarCatalogoBasicoOuputDto(DetCatalogoEstructura param) throws Exception {
        List<ListarCatalogoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarCatalogoBasicoOutputDto.class);
        DatosListarCatalogoBasicoOuputDto datos = new DatosListarCatalogoBasicoOuputDto();
        datos.setCatalogos(outputDto);
        ResponseListarCatalogoBasicoOuputDto response = new ResponseListarCatalogoBasicoOuputDto();
        response.setResultado(param.getResultado());
        response.setMensaje( param.getMensaje());
        response.setDatos(datos);
        return response;
    }
}

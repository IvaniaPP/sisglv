package pe.gob.onpe.sisglvbackend.negocio.modelos;


import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CabCargaArchivo extends ModeloBase {
	private Integer idCargarArchivo;
	private MaeProcesoElectoral maeProcesoElectoral;
	private MaeProcesoVerificacion maeProcesoVerificacion;
	private Integer tipoProceso;	// 1 = proceso electoral,   x = proceso verificacion
	private CabFormatoPlantilla cabFormatoPlantilla;
	private TabArchivo tabArchivo;
	private String contenidoJson;
	private String observacionGeneral;
	private String observacionDetalle;
	private List<CabCargaArchivo> lista;
}

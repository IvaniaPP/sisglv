package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ConfAmbitoVerificacion extends ModeloBase {
	private Integer idConfAmbitoVerificacion;
	private ConfProcesoVerificacion confProcesoVerificacion;
	private MaeUbigeo maeUbigeo;
}

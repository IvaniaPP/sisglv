package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionC;

@Mapper
public interface DetRegistroSeccionCMapper {
	
	void registrar(DetRegistroSeccionC detFichaSeccionC) throws Exception;
	void listar(DetRegistroSeccionC deteRegistroSeccionC) throws Exception;
	void actualizar(DetRegistroSeccionC detRegistroSeccionC) throws Exception;
	void eliminarAula(DetRegistroSeccionC param)throws Exception;
	
}

package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FichaSeccionD extends ModeloBase {

    private MaeLocalVotacion maeLocalVotacion;
    private String observacionRegistro;
    private Integer tipoArea;
    private Integer largo;
    private Integer ancho;
    private String referencia;
    private Integer tieneTecho;
    private Integer tienePuerta;
    private Integer cantidadPuerta;
    private Integer tieneAccesoLuz;
    private Integer activo;
    
    private List<FichaSeccionC> lista = new ArrayList<>();

}

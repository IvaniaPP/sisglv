package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArchivoLVCarga extends ModeloBase {

	private List<ArchivoLV> archivos;
	private String validado;
}

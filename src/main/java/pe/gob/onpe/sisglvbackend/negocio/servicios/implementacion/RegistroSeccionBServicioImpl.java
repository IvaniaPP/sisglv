package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionB;

import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.RegistroSeccionBMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionBServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service 
public class RegistroSeccionBServicioImpl implements RegistroSeccionBServicio{
	@Autowired
	RegistroSeccionBMapper registroSeccionBMapper;
	
	@Override
	public void registrarCaracteristicasPrimerPaso(RegistroSeccionB param) {
		// TODO Auto-generated method stub
		registroSeccionBMapper.registrarCaracteristicaPrimerPaso(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "registrarCaracteristicasPrimerPaso");
	}

	@Override
	public void registrarCaracteristicasSegundoPaso(RegistroSeccionB param) {
		// TODO Auto-generated method stub
		registroSeccionBMapper.registrarCaracteristicaSegundoPaso(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "registrarCaracteristicasSegundoPaso");
	}

	@Override
	public RegistroSeccionB obtenerCaracteristicasPrimerPaso(RegistroSeccionB param) {
		// TODO Auto-generated method stub
		registroSeccionBMapper.obtenerCaracteristicaPrimerPaso(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "obtenerCaracteristicasPrimerPaso");
		return param.getRegistrosSeccionB().size()==0 ? null : param.getRegistrosSeccionB().get(0);
	}


	@Override
	public RegistroSeccionB obtenerCaracteristicasSegundoPaso(RegistroSeccionB param) {
		// TODO Auto-generated method stub
		registroSeccionBMapper.obtenerCaracteristicaSegundoPaso(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "obtenerCaracteristicasSegundoPaso");
		return param.getRegistrosSeccionB().size()==0 ? null : param.getRegistrosSeccionB().get(0);
	}

	@Override
	public RegistroSeccionB obtenerCaracteristicas(RegistroSeccionB param) {
		registroSeccionBMapper.obtenerCaracteristicas(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "obtenerCaracteristicas");
		return param.getRegistrosSeccionB().size()==0 ? null : param.getRegistrosSeccionB().get(0);

	}

	@Override
	public void validarSeccionB(RegistroSeccionB param) {
		registroSeccionBMapper.validarSeccionB(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "validarSeccionB");

		
	}

	@Override
	public void actualizarCaracteristicaPrimerPaso(RegistroSeccionB param) {
		// TODO Auto-generated method stub
		registroSeccionBMapper.actualizarCaracteristicaPrimerPaso(param);
		Funciones.validarOperacionConBaseDatos(param, RegistroSeccionBServicioImpl.class, "actualizarCaracteristicaPrimerPaso");
	}

}

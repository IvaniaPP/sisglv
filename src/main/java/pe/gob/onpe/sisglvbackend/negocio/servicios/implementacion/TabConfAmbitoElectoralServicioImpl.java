package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.TabConfAmbitoElectoralMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.TabConfAmbitoElectoralServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class TabConfAmbitoElectoralServicioImpl implements TabConfAmbitoElectoralServicio {

    @Autowired
    private TabConfAmbitoElectoralMapper tabConfAmbitoElectoralMapper;

    @Override
    public TabConfAmbitoElectoral crearTabConfAmbitoElectoral(TabConfAmbitoElectoral tabConfAmbitoElectoral) throws Exception {
        tabConfAmbitoElectoralMapper.crearTabConfAmbitoElectoral(tabConfAmbitoElectoral);
        Funciones.validarOperacionConBaseDatos(tabConfAmbitoElectoral, TabConfAmbitoElectoralServicioImpl.class, "crearTabConfAmbitoElectoral");
        return tabConfAmbitoElectoral;
    }

}

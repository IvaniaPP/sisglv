package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoElectoral;

public interface TabConfAmbitoElectoralServicio {
    TabConfAmbitoElectoral crearTabConfAmbitoElectoral(TabConfAmbitoElectoral tabConfAmbitoElectoral) throws Exception;
}

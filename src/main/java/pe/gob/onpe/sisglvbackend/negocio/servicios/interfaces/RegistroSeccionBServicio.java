package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionB;

public interface RegistroSeccionBServicio {

	public void registrarCaracteristicasPrimerPaso(RegistroSeccionB param);
	public void registrarCaracteristicasSegundoPaso(RegistroSeccionB param);
	public RegistroSeccionB obtenerCaracteristicasPrimerPaso(RegistroSeccionB param);
	public RegistroSeccionB obtenerCaracteristicasSegundoPaso(RegistroSeccionB param);
	public RegistroSeccionB obtenerCaracteristicas(RegistroSeccionB param);
	public void validarSeccionB(RegistroSeccionB param);

	public void actualizarCaracteristicaPrimerPaso(RegistroSeccionB param);
}

package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionD;

@Mapper
public interface DetFichaSeccionDMapper {
    
    void obtenerLocalVotacionHistDetSecD(DetFichaSeccionD param) throws Exception;
    
}

package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;

import lombok.Setter;

@Getter
@Setter
public class HistorialLocalVotacionETLV extends ModeloBase {
	
	private Integer id;
	private VerificacionLV verificacionLV;
	private Integer iteracion;

}

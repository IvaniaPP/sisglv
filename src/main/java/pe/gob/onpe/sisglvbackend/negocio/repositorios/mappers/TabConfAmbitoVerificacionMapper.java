package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.TabConfAmbitoVerificacion;

@Mapper
public interface TabConfAmbitoVerificacionMapper {
    void crearTabConfAmbitoVerificacion(TabConfAmbitoVerificacion tabConfAmbitoVerificacion) throws Exception;
}

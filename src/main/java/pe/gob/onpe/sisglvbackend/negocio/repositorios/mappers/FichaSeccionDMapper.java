package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionD;

@Mapper
public interface FichaSeccionDMapper {
    
    void listarFichaSeccionD(FichaSeccionD param) throws Exception;
    
}

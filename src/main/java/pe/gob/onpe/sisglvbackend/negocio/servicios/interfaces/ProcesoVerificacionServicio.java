package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.AsignarOrcAVerificarProcesoDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarOrcAVerficacionProcesoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ProcesarArchivoOrcOutputDto;

public interface ProcesoVerificacionServicio {
	List<MaeProcesoVerificacion> listarActivos(MaeProcesoVerificacion param);
	
	public void registrarProcesoVerificacion(MaeProcesoVerificacion param);
	
	public ProcesarArchivoOrcOutputDto cargarArchivoOrcsAFtp(Integer idProcesoElectoral, MultipartFile multipartFile, String numeroDocumento) throws Exception;
	
	public MaeProcesoVerificacion getProcesoVerificacionById(Integer idProcesoVerificacion) throws Exception;

	
	public MaeProcesoVerificacion getProcesoVerificacionHabilitado() throws Exception;
	
	
	AsignarOrcAVerficacionProcesoOutputDto asignarOrcsAVerificarProceso(AsignarOrcAVerificarProcesoDto asignarOdpeAProcesoElectoralDto, String numeroDocumento) throws Exception;
}

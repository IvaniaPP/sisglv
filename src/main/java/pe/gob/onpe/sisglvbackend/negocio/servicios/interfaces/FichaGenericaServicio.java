package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaGenerica;

/**
 * @author glennlq
 * @created 9/27/21
 */
public interface FichaGenericaServicio {
    FichaGenerica copiarFichasABCDE(FichaGenerica fichaGenerica) throws Exception;
}

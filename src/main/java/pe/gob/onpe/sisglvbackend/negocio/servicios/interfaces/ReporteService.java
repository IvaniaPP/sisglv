package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteRegistrosAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteRegistrosPorRegistrador;
import pe.gob.onpe.sisglvbackend.negocio.beans.ReporteSeccionesSisglv;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.reporte.ValidarClaveReporteInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte.ListaReporteSeccionABCDEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte.ValidarClaveReporteOutputDto;

public interface ReporteService {
	
	void reporteRegistrosOrc (ReporteRegistrosAmbitoElectoral repote) throws Exception;
	
	public void reporteRegistrosRegistrador(ReporteRegistrosPorRegistrador reporte) throws Exception;
	
	public void reporteRegistrosPorSeccion (ReporteSeccionesSisglv reporte) throws Exception;

	ListaReporteSeccionABCDEOutputDto reporteSeccionesABCDE() throws Exception;

	ValidarClaveReporteOutputDto validarClaveReporte(ValidarClaveReporteInputDto validarClaveReporteInputDto) throws Exception;

}

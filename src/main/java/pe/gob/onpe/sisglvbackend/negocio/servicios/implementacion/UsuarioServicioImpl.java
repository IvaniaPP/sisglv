package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.Usuario;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeAmbitoElectoralServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoElectoralServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoVerificacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.UsuarioServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotAuthorizedException;
import pe.gob.onpe.sisglvbackend.transversal.properties.SasaProperties;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.LoginInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginPerfilesOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginUsuarioOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.RefrescarTokenOutputDto;
import springfox.documentation.spring.web.json.Json;


@Service
public class UsuarioServicioImpl implements UsuarioServicio {
	
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private SasaProperties sasaProperties;
	
	@Autowired
	private ProcesoElectoralServicio procesoElectoralServicio;
	
	@Autowired
	private ProcesoVerificacionServicio procesoVerificacionServicio;
	
	@Autowired
	private MaeAmbitoElectoralServicio ambitoElectoralServicio;
	
	@Override
	public Usuario obtenerInfoUsuarioPorUsuarioAplicacion(Integer idUsuario, Integer idAplicacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void validarSesionActiva(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario obtenerUsuarioPorNumeroDocumento(String numeroDocumento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoginDatosOutputDto accederSistema(LoginInputDto input) throws Exception {
		LoginDatosOutputDto datos = null;
		
				input.setCodigo(sasaProperties.getAplicacion());
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("Content-Type", "application/json");
				HttpEntity<LoginInputDto> requestEntity = new HttpEntity<LoginInputDto>(input,
						headers);
				
				try {
						ResponseEntity<LoginDatosOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/loginsc",
						HttpMethod.POST, requestEntity, LoginDatosOutputDto.class);
						datos = responseLogin.getBody();
						
				}catch(Exception e) {
					e.printStackTrace();
					Usuario usuario = new Usuario();
					usuario.setResultado(-2);
					usuario.setMensaje("Usuario y/o contraseña inválido.");
					Funciones.validarOperacionConBaseDatos(usuario, UsuarioServicioImpl.class, "accederSistema");
					
				}
				
				
				
				 
				if(!StringUtils.isEmpty(datos.getDatos().getUsuario().getAcronimoProceso())) {
					MaeProcesoElectoral procesoElectoral = procesoElectoralServicio.getProcesoElectoralPorAcronimoYNombre(datos.getDatos().getUsuario().getAcronimoProceso(), datos.getDatos().getUsuario().getNombreProceso());
					if(procesoElectoral == null) {
						MaeProcesoVerificacion procesoVerificacion = procesoVerificacionServicio.getProcesoVerificacionHabilitado();
						if(procesoVerificacion == null) {
							Usuario usuario = new Usuario();
							usuario.setResultado(-2);
							usuario.setMensaje("Proceso Electoral/Verificación no se encuentra activo.");
							Funciones.validarOperacionConBaseDatos(usuario, UsuarioServicioImpl.class, "accederSistema");
						} else {
							
							datos.getDatos().getUsuario().setIdProcesoElectoral(procesoVerificacion.getIdProcesoVerificacion());
							datos.getDatos().getUsuario().setTipoProceso(2);
						}
						
					} else {
						datos.getDatos().getUsuario().setIdProcesoElectoral(procesoElectoral.getIdProcesoElectoral());
						datos.getDatos().getUsuario().setTipoProceso(1);
					}
				}
				
				if(!StringUtils.isEmpty(datos.getDatos().getUsuario().getNombreAmbito() ) ) {
					 MaeAmbitoElectoral ambito = ambitoElectoralServicio.obtenerMaeAmbitoElectoralPorNombre(datos.getDatos().getUsuario().getNombreAmbito(), datos.getDatos().getUsuario().getTipoAmbito());
					 
					 if(ambito == null) {
						 Usuario usuario = new Usuario();
							usuario.setResultado(-2);
							usuario.setMensaje("Ambito Electoral no se encuentra activo.");
							Funciones.validarOperacionConBaseDatos(usuario, UsuarioServicioImpl.class, "accederSistema");
						
					 } else {
						 datos.getDatos().getUsuario().setIdAmbitoElectoral(ambito.getIdAmbitoElectoral());
						 
					 }
				}
		
		
			return datos;
		
	}
		
	@Override
	public CargarAccesoDatosOutputDto cargarAccesos(CargarAccesosInputDto input,String token) throws Exception {
	try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<CargarAccesosInputDto> requestEntity = new HttpEntity<CargarAccesosInputDto>(input,headers);

			// ResponseEntity<ConsultaPadronOutputDto> responsePadron =
			// restTemplate.postForEntity(padronProperties.getUrl(), peticion,
			// ConsultaPadronOutputDto.class);

			ResponseEntity<CargarAccesoDatosOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/cargar-accesos",
					HttpMethod.POST, requestEntity, CargarAccesoDatosOutputDto.class);

			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public HashMap<String, Object> actualizarNuevaClave(ActualizarNuevaClaveInputDto input, String token)
			throws Exception {
	try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<ActualizarNuevaClaveInputDto> requestEntity = new HttpEntity<ActualizarNuevaClaveInputDto>(input,headers);

			
			ResponseEntity<HashMap> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/actualizar-nueva-clave",
					HttpMethod.POST, requestEntity, HashMap.class);

			//Map<String, Object> respuesta = (HashMap<String, Object> responseLogin.getBody());
		//	LoginOutputDto respuesta = new LoginOutputDto();
			
					
			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public BaseOutputDto asignarPersonaAUsuarioGenerardo(AsignarPersonaUsuarioGeneradoInputDto input, String token)
			throws Exception {
		try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<AsignarPersonaUsuarioGeneradoInputDto> requestEntity = new HttpEntity<AsignarPersonaUsuarioGeneradoInputDto>(input,headers);

			
			ResponseEntity<BaseOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/asignar-persona-usuario-autogenerado",
					HttpMethod.POST, requestEntity, BaseOutputDto.class);

			//Map<String, Object> respuesta = (HashMap<String, Object> responseLogin.getBody());
		//	LoginOutputDto respuesta = new LoginOutputDto();
			
					
			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

	@Override
	public RefrescarTokenOutputDto refrescarToken(String token) throws Exception {
	try {
			
			
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			headers.set("Authorization",  token);
			
			HttpEntity<RefrescarTokenOutputDto> requestEntity = new HttpEntity<RefrescarTokenOutputDto>(headers);

			
			ResponseEntity<RefrescarTokenOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/usuario/refreshtoken",
					HttpMethod.GET, requestEntity, RefrescarTokenOutputDto.class);

			//Map<String, Object> respuesta = (HashMap<String, Object> responseLogin.getBody());
		//	LoginOutputDto respuesta = new LoginOutputDto();
			
					
			return  responseLogin.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		
		}
	}

}

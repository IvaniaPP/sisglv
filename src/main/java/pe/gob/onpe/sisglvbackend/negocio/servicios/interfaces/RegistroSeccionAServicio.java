package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionA;


public interface RegistroSeccionAServicio {
	public void registrarDatosGeneralesPrimerPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesSegundoPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesTercerPaso(RegistroSeccionA param);
	public void registrarDatosGeneralesCuartoPaso(RegistroSeccionA param);
	

	public RegistroSeccionA obtenerDatosGeneralesPrimerPaso(RegistroSeccionA param);
	public RegistroSeccionA obtenerDatosGeneralesSegundoPaso(RegistroSeccionA param);
	public RegistroSeccionA obtenerDatosGeneralesTercerPaso(RegistroSeccionA param);
	public RegistroSeccionA obtenerDatosGeneralesCuartoPaso(RegistroSeccionA param);
	public RegistroSeccionA obtenerDatosGenerales(RegistroSeccionA param);
        
    public void ListarLVActivos(RegistroSeccionA param);
    public void ListarFiltroLVActivos(RegistroSeccionA param);
	public void validarSeccionA(RegistroSeccionA registroSeccionA);
	
	 public void actualizarDatosGeneralesPrimerPaso(RegistroSeccionA param);
}

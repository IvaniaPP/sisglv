package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;
import java.util.List;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaeProcesoElectoral extends ModeloBase {
	private Integer idProcesoElectoral;
	private Integer idProcesoElectoralPadre;
	private String idProceso;
	private String nombre;
	private String acronimo;
	private Date fechaConvocatoria;
	private Integer tipoAmbitoElectoral;
	private Integer tipoAmbitoGeografico;
	private Integer base;
	private String correlativo;
	private Integer etapa;
	private Integer estado2;
	private Integer estadoConfig;
	private List<MaeProcesoElectoral> procesosElectorales;

	public static Integer PE_CREADO = 0;
	public static Integer PE_ARCHIVO_XLSX_CARGADO = 1;
	public static Integer PE_ARCHIVO_PROCESADO = 2;
	public static Integer PE_CONFIG_FINALIZADO = 3;

}

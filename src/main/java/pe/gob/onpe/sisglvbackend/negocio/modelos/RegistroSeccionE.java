package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistroSeccionE extends ModeloBase {
	
	private Integer idRegistroSeccionD;
	private VerificacionLV verificacionLV;
	private String seccion;
	private String observacionLV;
	private String fotoFrontis;
	private String fotoCentroAcopio;
	private String fotoPuntoTransmision;
	private String fotoCercoPerimetrico;
	private String fotoAulaMesa;
	private String fotoPatioPrincipal;
	private String fotoEscalera;
	private String croquisVerificacionLV;
	private String croquisJEL;//jel:Jornada Electoral
	private String documentoSolicitudLV;
	private String actaCompromisoUso;
	private String observacionRegistro;
	private String usuarioObservacion;
	private Date fechaObservacion;
	private String usuarioAprobacion;
	private Date fechaAprobacion;
	
	
}

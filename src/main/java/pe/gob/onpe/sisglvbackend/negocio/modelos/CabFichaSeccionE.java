package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabFichaSeccionE extends ModeloBase {

    private Integer idFichaSeccionE;
    private MaeLocalVotacion maeLocalVotacion;
    private String seccion;
    private String observacionLV;
    private String observacionRegistro;

}

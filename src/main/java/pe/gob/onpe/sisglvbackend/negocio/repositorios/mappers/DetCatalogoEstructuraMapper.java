package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.Catalogo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCatalogoEstructura;

@Mapper
public interface DetCatalogoEstructuraMapper {
    
    void listarCatalogosActivos(DetCatalogoEstructura param) throws Exception;
    
}

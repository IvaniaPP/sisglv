package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import pe.gob.onpe.sisglvbackend.ftp.ArchivoFTP;
import pe.gob.onpe.sisglvbackend.ftp.ConfArchivoFTP;
import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeProcesoElectoralMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.*;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteFTP;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.properties.FtpProperties;
import pe.gob.onpe.sisglvbackend.transversal.properties.SasaProperties;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.utils.StringUtils;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.AsignarOdpeAProcesoElectoralDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.OdpeInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.PerfilInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.ProcesarArchivoOdpesDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.RegistrarGeneracionUsuarioPorProcesoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.RegistrarProcesoElectoralInicioInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.RegistrarProcesoElectoralInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.AsignarOdpeAProcesoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.ProcesarArchivoOdpesOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral.RegistrarProcesoElectoralInicioOutput;


@Service
public class ProcesoElectoralServicioImpl implements ProcesoElectoralServicio {

//	@Value("${FTP_DIRHOME}")
//	protected String FTP_DIRHOME;

	@Autowired
	private MaeProcesoElectoralMapper procesoElectoralMapper;

	@Autowired
	private TabArchivoServicio tabArchivoServicio;

	@Autowired
	private CabCargaArchivoServicio cabCargaArchivoServicio;

	@Autowired
	private DetCargaArchivoServicio detCargaArchivoServicio;

	@Autowired
	private MaeAmbitoElectoralServicio maeAmbitoElectoralServicio;

	@Autowired
	private TabConfProcesoElectoralServicio tabConfProcesoElectoralServicio;

	@Autowired
	private TabConfAmbitoElectoralServicio tabConfAmbitoElectoralServicio;

	@Autowired
	private FichaGenericaServicio fichaGenericaServicio;

	@Autowired
	private UbigeoServicio ubigeoServicio;

	@Autowired
	private SFTPService ftpService;

	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private SasaProperties sasaProperties;

	private final Integer COL_TIPO_PROCESO = 1;
	private final Integer COL_TIPO_AMBITO = 1;
	private final Integer COL_ACTIVO = 1;
	private final boolean FTP_LOCAL = false;
	private final String FTP_LOCAL_RUTA = "/home/glennlq/dev/SISGLV_FTP/";
//	private final String USUARIO_CREACION = "444444";
	private static final Logger logger = Logger.getLogger(ProcesoElectoralServicioImpl.class.getCanonicalName());

	@Override
	public List<MaeProcesoElectoral> listarActivos(MaeProcesoElectoral param) {
		// TODO Auto-generated method stub
		procesoElectoralMapper.listarActivos(param);
		Funciones.validarOperacionConBaseDatos(param, ProcesoElectoralServicioImpl.class, "listarActivos");
		return param.getProcesosElectorales();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public MaeProcesoElectoral crearProcesoElectoral(MaeProcesoElectoral maeProcesoElectoral, String numeroDocumento) {
			maeProcesoElectoral.setIdProceso(String.valueOf(maeProcesoElectoral.getIdProcesoElectoral()));
			maeProcesoElectoral.setAcronimo(maeProcesoElectoral.getAcronimo());
			maeProcesoElectoral.setFechaConvocatoria(new Date());
			maeProcesoElectoral.setBase(1);
			maeProcesoElectoral.setCorrelativo(String.valueOf(maeProcesoElectoral.getIdProcesoElectoral()));
			maeProcesoElectoral.setEtapa(1);
			maeProcesoElectoral.setEstado2(1);
			maeProcesoElectoral.setEstadoConfig(MaeProcesoElectoral.PE_CREADO);
			maeProcesoElectoral.setActivo(COL_ACTIVO);
			maeProcesoElectoral.setUsuarioCreacion(numeroDocumento);
			maeProcesoElectoral.setUsuarioModificacion("00000");
			procesoElectoralMapper.crearProcesoElectoral(maeProcesoElectoral);
			Funciones.validarOperacionConBaseDatos(maeProcesoElectoral, AplicacionServicioImpl.class, "registrarAplicacion");
			return maeProcesoElectoral;
	}

	@Override
	public MaeProcesoElectoral getProcesoElectoralById(Integer idProcesoElectoral) throws Exception {
		MaeProcesoElectoral maeProcesoElectoral = MaeProcesoElectoral.builder().idProcesoElectoral(idProcesoElectoral).build();
		procesoElectoralMapper.getProcesoElectoralById(maeProcesoElectoral);
		Funciones.validarOperacionConBaseDatos(maeProcesoElectoral, TabArchivoServicioImpl.class, "getProcesoElectoralById");
		if(maeProcesoElectoral.getProcesosElectorales().size() == 0)
			throw new NotFoundRegisterInDb("MAE_PROCESO_ELECTORAL", "N_PROCESO_ELECTORAL_PK", idProcesoElectoral);
		return maeProcesoElectoral.getProcesosElectorales().get(0);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ProcesarArchivoOdpesOutputDto cargarArchivoOdpesAFtp(Integer idProcesoElectoral, MultipartFile multipartFile, String numeroDocumento) throws Exception {
		MaeProcesoElectoral maeProcesoElectoral = getProcesoElectoralById(idProcesoElectoral);
		TabArchivo tabArchivo = buildTabArchivo(multipartFile, numeroDocumento);
		guardarArchivoEnFtp(multipartFile, tabArchivo);
		tabArchivo = tabArchivoServicio.crearTabArchivo(tabArchivo);
		ProcesarArchivoOdpesOutputDto procesarArchivoOdpesOutputDto = ___procesarArchivoOdpes(maeProcesoElectoral, tabArchivo, numeroDocumento);
		maeProcesoElectoral.setEstadoConfig(MaeProcesoElectoral.PE_ARCHIVO_PROCESADO);
		updateEstadoConfigMaeProcesoElectoral(maeProcesoElectoral);
		return procesarArchivoOdpesOutputDto;
	}

	private TabArchivo buildTabArchivo(MultipartFile multipartFile, String numeroDocumento) throws Exception {
		ArchivoFTP archivoFTP = new ArchivoFTP(multipartFile, getConfArchivoFTP());
		archivoFTP.validateAll();
		TabArchivo tabArchivo = TabArchivo.builder()
				.guidArchivo(archivoFTP.getUUID())
				.rutaArchivo(ConstanteFTP.CARPETA_ODPS)
				.nombreArchivo(archivoFTP.getFileName())
				.formatoArchivo(archivoFTP.getFileExtension())
				.pesoArchivo(archivoFTP.getSize().toString())
				.nombreOriginalArchivo(archivoFTP.getFileNameGenerated())
				.build();
		tabArchivo.setActivo(COL_ACTIVO);
		tabArchivo.setUsuarioCreacion(numeroDocumento);
		return tabArchivo;
	}

	private void guardarArchivoEnFtp(MultipartFile multipartFile, TabArchivo tabArchivo) throws Exception {
		if(FTP_LOCAL) {
			File file = new File(FTP_LOCAL_RUTA, tabArchivo.getRutaArchivo());
			File excel = new File(file, tabArchivo.getNombreOriginalArchivo());
			FileUtils.writeByteArrayToFile(excel, multipartFile.getBytes());
		} else {
			ftpService.uploadFileToFTP(multipartFile.getInputStream(), tabArchivo.getRutaArchivo(), tabArchivo.getNombreOriginalArchivo());
		}
	}

	private ConfArchivoFTP getConfArchivoFTP() {
		ConfArchivoFTP conf = new ConfArchivoFTP();
		conf.setMaxCharacters(50L);
		conf.setValidatedExtensions(new String[] { "xls", "xlsx"});
		conf.setMaxSize(1048576L*6L); // 6MB
		return conf;
	}

	public ProcesarArchivoOdpesOutputDto ___procesarArchivoOdpes(MaeProcesoElectoral maeProcesoElectoral, TabArchivo tabArchivo, String numeroDocumento) throws Exception {
		InputStream inputStreamFileExcel =  getInputStreamFileExcel(tabArchivo);
		// TODO implementar aquí la tabla CAB_FORMATO_PLANTILLA
		CabFormatoPlantilla cabFormatoPlantilla = new CabFormatoPlantilla();
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.crearCabCargaArchivo(buildCabCargaArchivo(maeProcesoElectoral, tabArchivo, cabFormatoPlantilla, numeroDocumento));
		List<DetCargaArchivo> detCargaArchivos = guardarDetCargaArchivo(leerArchivoOdpes(tabArchivo, cabCargaArchivo, inputStreamFileExcel, numeroDocumento));
		return modelMapper.map(cabCargaArchivo, ProcesarArchivoOdpesOutputDto.class);
	}

	public void updateEstadoConfigMaeProcesoElectoral(MaeProcesoElectoral maeProcesoElectoral) throws Exception {
		procesoElectoralMapper.actualizarEstadoProcesoElectoral(maeProcesoElectoral);
		Funciones.validarOperacionConBaseDatos(maeProcesoElectoral, TabArchivoServicioImpl.class, "updateEstadoConfigMaeProcesoElectoral");
	}

	@Override
	public ProcesarArchivoOdpesOutputDto procesarArchivoOdpes(ProcesarArchivoOdpesDto procesarArchivoOdpesDto, String numeroDocumento) throws Exception {
		MaeProcesoElectoral maeProcesoElectoral = getProcesoElectoralById(procesarArchivoOdpesDto.getIdProcesoElectoral());
		TabArchivo tabArchivo = tabArchivoServicio.obtenerTabArchivoPorId(procesarArchivoOdpesDto.getIdArchivo());
		InputStream inputStreamFileExcel =  getInputStreamFileExcel(tabArchivo);
		// TODO implementar aquí la tabla CAB_FORMATO_PLANTILLA
		CabFormatoPlantilla cabFormatoPlantilla = new CabFormatoPlantilla();
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.crearCabCargaArchivo(buildCabCargaArchivo(maeProcesoElectoral, tabArchivo, cabFormatoPlantilla, numeroDocumento));
		List<DetCargaArchivo> detCargaArchivos = guardarDetCargaArchivo(leerArchivoOdpes(tabArchivo, cabCargaArchivo, inputStreamFileExcel, numeroDocumento));
		return modelMapper.map(cabCargaArchivo, ProcesarArchivoOdpesOutputDto.class);
	}

	private InputStream getInputStreamFileExcel(TabArchivo tabArchivo) throws Exception {
		if(FTP_LOCAL) {
			File path = new File(FTP_LOCAL_RUTA, tabArchivo.getRutaArchivo());
			File fileExcel = new File(path, tabArchivo.getNombreOriginalArchivo());
			return new FileInputStream(fileExcel);
		} else {
			byte[] data = ftpService.downloadFileFromFTP(tabArchivo.getRutaArchivo(), tabArchivo.getNombreOriginalArchivo());
			return new ByteArrayInputStream(data);
		}
	}

	private CabCargaArchivo buildCabCargaArchivo(MaeProcesoElectoral maeProcesoElectoral, TabArchivo tabArchivo,
												 CabFormatoPlantilla cabFormatoPlantilla, String numeroDocumento) throws Exception {
		CabCargaArchivo cabCargaArchivo = CabCargaArchivo.builder()
				.maeProcesoElectoral(maeProcesoElectoral)
				.maeProcesoVerificacion(new MaeProcesoVerificacion())
				.tipoProceso(COL_TIPO_PROCESO)
				.cabFormatoPlantilla(cabFormatoPlantilla)
				.tabArchivo(tabArchivo)
				.build();
		cabCargaArchivo.setActivo(COL_ACTIVO);
		cabCargaArchivo.setUsuarioCreacion(numeroDocumento);
		return cabCargaArchivo;
	}

	private List<DetCargaArchivo> leerArchivoOdpes(TabArchivo tabArchivo, CabCargaArchivo cabCargaArchivo,
												   InputStream inputStreamFile, String numeroDocumento) throws Exception {
		Workbook workbook = new XSSFWorkbook(inputStreamFile);
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		String[] heads = getHeadExcel(sheet, dataFormatter);
		List<DetCargaArchivo> detCargaArchivos = new ArrayList<>();
		int fila = 0;
		for (Row row : sheet) {
			if(fila++ == 0) continue;
			boolean rowEmpty = getIfEmptyRow(row, heads, dataFormatter);
			if(!rowEmpty) {
				for (int i = 0; i < heads.length; i++) {
					detCargaArchivos.add(buildDetCargaArchivo(cabCargaArchivo, fila, heads[i], dataFormatter.formatCellValue(row.getCell(i)), numeroDocumento));
				}
			}
		}
		return detCargaArchivos;
	}

	private boolean getIfEmptyRow(Row row, String[] heads, DataFormatter dataFormatter) throws Exception {
		boolean rowEmpty = false;
		for(int i = 0; i < heads.length; i++) {
			String tmp = dataFormatter.formatCellValue(row.getCell(i));
			if(tmp != null && !tmp.trim().equals("")){
			} else {
				rowEmpty = true;
			}
		}
		return rowEmpty;
	}

	private String[] getHeadExcel(Sheet sheet, DataFormatter dataFormatter) throws Exception {
		List<String> heads = new ArrayList<>();
		Row row = sheet.getRow(0);
		for(int i = 0 ; i < 30 ; i++) {
			String value = dataFormatter.formatCellValue(row.getCell(i));
			if(value != null && !value.equals("")) {
				heads.add(value);
			}
		}
		return heads.toArray(new String[0]);
	}

	private DetCargaArchivo buildDetCargaArchivo(CabCargaArchivo cabCargaArchivo, int fila, String clave, String valor, String numeroDocumento) throws Exception {
		DetCargaArchivo detCargaArchivo = DetCargaArchivo.builder()
				.cabCargaArchivo(cabCargaArchivo)
				.fila(fila)
				.clave(clave)
				.valor(valor).build();
		detCargaArchivo.setUsuarioCreacion(numeroDocumento);
		return detCargaArchivo;
	}

	private List<DetCargaArchivo> guardarDetCargaArchivo(List<DetCargaArchivo> detCargaArchivos) throws Exception {
		Iterator<DetCargaArchivo> iter = detCargaArchivos.iterator();
		while(iter.hasNext()) {
			DetCargaArchivo tmp = iter.next();
			detCargaArchivoServicio.crearDetCargaArchivo(tmp);
		}
		return detCargaArchivos;
	}

	@Override
	public BaseOutputDto registrarProcesoAsignacionOdpeEnSasa(AsignarOdpeAProcesoElectoralDto asignarOdpeAProcesoElectoralDto, String token) throws Exception{
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.getCabCargaArchivoPorId(asignarOdpeAProcesoElectoralDto.getIdCargarArchivo());
		MaeProcesoElectoral maeProcesoElectoral = this.getProcesoElectoralById(cabCargaArchivo.getMaeProcesoElectoral().getIdProcesoElectoral());
		List<MaeAmbitoElectoral> listaAmbitos = maeAmbitoElectoralServicio.listarAmbitoElectoralPorProceso(maeProcesoElectoral.getIdProcesoElectoral());
		
		RegistrarProcesoElectoralInputDto proceso = Funciones.map(maeProcesoElectoral, RegistrarProcesoElectoralInputDto.class);
		proceso.setProcesoBase(0);
		
		List<OdpeInputDto> listaOdpe = Funciones.mapAll(listaAmbitos, OdpeInputDto.class);
		
		RegistrarProcesoElectoralInicioInputDto paramIn = new RegistrarProcesoElectoralInicioInputDto();
		paramIn.setListaOdpes(listaOdpe);
		paramIn.setProceso(proceso);
		
		//Se llama al servicio del sasa
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization",  token);
		
		HttpEntity<RegistrarProcesoElectoralInicioInputDto> requestEntity = new HttpEntity<RegistrarProcesoElectoralInicioInputDto>(paramIn,headers);

	
		ResponseEntity<RegistrarProcesoElectoralInicioOutput> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/odpeProcesoElectoral/registrar-proceso-electoral-inicio",
				HttpMethod.POST, requestEntity, RegistrarProcesoElectoralInicioOutput.class);
		
		RegistrarProcesoElectoralInicioOutput out = responseLogin.getBody();
		
		BaseOutputDto baseOutputDto = generarUsuarioAutomaticoEnSasa(out,token);
		return baseOutputDto;
	}

	private BaseOutputDto generarUsuarioAutomaticoEnSasa(RegistrarProcesoElectoralInicioOutput out, String token) {
		RegistrarGeneracionUsuarioPorProcesoInputDto inputGeneracion = new RegistrarGeneracionUsuarioPorProcesoInputDto();
		inputGeneracion.setIdProcesoElectoral(out.getIdProceso());
		inputGeneracion.setIdTipoAmbito(1);
		inputGeneracion.setIdUnidadOrganica(50);
		
		List<PerfilInputDto> listaPerfiles = new ArrayList<PerfilInputDto>();
		
		PerfilInputDto perfil1 = new PerfilInputDto();
		perfil1.setAbreviatura("OPE_ODPE");
		perfil1.setNombre("PERFIL OPERADOR ODPE");
		
		PerfilInputDto perfil2 = new PerfilInputDto();
		perfil2.setAbreviatura("JODPE");
		perfil2.setNombre("PERFIL JEFE ODPE");
		
		
		listaPerfiles.add(perfil1);
		listaPerfiles.add(perfil2);
		
		inputGeneracion.setPerfiles(listaPerfiles);
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization",  token);
		
		HttpEntity<RegistrarGeneracionUsuarioPorProcesoInputDto> requestEntity = new HttpEntity<RegistrarGeneracionUsuarioPorProcesoInputDto>(inputGeneracion,headers);

	
		ResponseEntity<BaseOutputDto> responseLogin = restTemplate.exchange(sasaProperties.getUrl() +"/generacion-usuario/registrar-generacion-usuario-por-proceso",
				HttpMethod.POST, requestEntity, BaseOutputDto.class);
		
		return responseLogin.getBody();
	}
	

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Async("threadPoolTaskExecutor")
	public AsignarOdpeAProcesoElectoralOutputDto asignarOdpesAProcesoElectoral(AsignarOdpeAProcesoElectoralDto asignarOdpeAProcesoElectoralDto,
																			   String token, String numeroDocumento) throws Exception {
		CabCargaArchivo cabCargaArchivo = cabCargaArchivoServicio.getCabCargaArchivoPorId(asignarOdpeAProcesoElectoralDto.getIdCargarArchivo());
		MaeProcesoElectoral maeProcesoElectoral = this.getProcesoElectoralById(cabCargaArchivo.getMaeProcesoElectoral().getIdProcesoElectoral());
		List<DetCargaArchivo> detCargaArchivos = detCargaArchivoServicio.listaDetCargaArchivoPorIdCargarArchivo(cabCargaArchivo.getIdCargarArchivo());
		List<Map<String, DetCargaArchivo>> ordenados = ordenarPorFilaDetCargaArchivo(detCargaArchivos);
		registrarMaeAmbitoElectoral(ordenados, maeProcesoElectoral, numeroDocumento);
		maeProcesoElectoral.setEstadoConfig(MaeProcesoElectoral.PE_CONFIG_FINALIZADO);
		updateEstadoConfigMaeProcesoElectoral(maeProcesoElectoral);
		//Aqui se enviar la informacion generada al SASA
		this.registrarProcesoAsignacionOdpeEnSasa(asignarOdpeAProcesoElectoralDto, token);
		return AsignarOdpeAProcesoElectoralOutputDto.builder().exito(true).mensaje("Proceso exitoso").build();
	}

	private List<Map<String, DetCargaArchivo>> ordenarPorFilaDetCargaArchivo(List<DetCargaArchivo> detCargaArchivos) throws Exception {
		int filaTmp = -1;
		List<Map<String, DetCargaArchivo>> lista = new ArrayList<>();
		for(DetCargaArchivo detCargaArchivo : detCargaArchivos) {
			if(detCargaArchivo.getFila() != filaTmp) {
				Map<String, DetCargaArchivo> tmp = new HashMap<>();
				tmp.put(detCargaArchivo.getClave().trim(), detCargaArchivo);
				lista.add(tmp);
				filaTmp = detCargaArchivo.getFila();
			} else {
				Map<String, DetCargaArchivo> tmp2 = lista.get(lista.size()-1);
				tmp2.put(detCargaArchivo.getClave().trim(), detCargaArchivo);
				lista.set(lista.size()-1, tmp2);
			}
		}
		return lista;
	}

	private void registrarMaeAmbitoElectoral(List<Map<String, DetCargaArchivo>> lista, MaeProcesoElectoral maeProcesoElectoral, String numeroDocumento) throws Exception {
		for(Map<String, DetCargaArchivo> mapRow : lista) {
			MaeAmbitoElectoral maeAmbitoElectoral = maeAmbitoElectoralServicio.obtenerMaeAmbitoElectoralPorNombre(mapRow.get("NOMBRE_ODP").getValor(), COL_TIPO_AMBITO);
			if(maeAmbitoElectoral == null) {
				maeAmbitoElectoral =maeAmbitoElectoralServicio.crearMaeAmbitoElectoral(buildMaeAmbitoElectoralFromMap(mapRow, numeroDocumento));
			}
			// TAB_CONF_PROCESO_ELECTORAL
			registrarTabConfProcesoElectoral(maeAmbitoElectoral, maeProcesoElectoral, mapRow, numeroDocumento);
		}
	}

	private MaeAmbitoElectoral buildMaeAmbitoElectoralFromMap(Map<String, DetCargaArchivo> mapRow, String numeroDocumento) {
		MaeAmbitoElectoral ambitoElectoral = MaeAmbitoElectoral.builder()
				.idAmbitoElectoralPadre(null)
				.nombre(mapRow.get("NOMBRE_ODP").getValor())
				.abreviatura(StringUtils.toAscii(StringUtils.quitarEspacios(mapRow.get("NOMBRE_ODP").getValor())))
				.tipoAmbitoElectoral(COL_TIPO_AMBITO)
				.build();
		ambitoElectoral.setActivo(COL_ACTIVO);
		ambitoElectoral.setUsuarioCreacion(numeroDocumento);
		return ambitoElectoral;
	}

	private void registrarTabConfProcesoElectoral(MaeAmbitoElectoral maeAmbitoElectoral, MaeProcesoElectoral maeProcesoElectoral,
												  Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		TabConfProcesoElectoral tabConfProcesoElectoral = buildTabConfProcesoElectoral(maeProcesoElectoral, maeAmbitoElectoral, mapRow, numeroDocumento);
		tabConfProcesoElectoral = tabConfProcesoElectoralServicio.crearTabConfProcesoElectoral(tabConfProcesoElectoral);
		// TAB_CONF_AMBITO_ELECTORAL
		registrarTabConfAmbitoElectoral(maeProcesoElectoral, maeAmbitoElectoral, tabConfProcesoElectoral, mapRow, numeroDocumento);
	}

	private TabConfProcesoElectoral buildTabConfProcesoElectoral(MaeProcesoElectoral maeProcesoElectoral, MaeAmbitoElectoral ambitoElectoral,
																 Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		TabConfProcesoElectoral tabConfProcesoElectoral = TabConfProcesoElectoral.builder()
				.maeProcesoElectoral(maeProcesoElectoral)
				.maeAmbitoElectoral(ambitoElectoral)
				.sede(mapRow.get("SEDE_DE_OD").getValor())
				.correlativo(Integer.parseInt(mapRow.get("NRO").getValor()))
				.notificacion(0)
				.build();
		tabConfProcesoElectoral.setActivo(COL_ACTIVO);
		tabConfProcesoElectoral.setUsuarioCreacion(numeroDocumento);
		return tabConfProcesoElectoral;
	}

	private void registrarTabConfAmbitoElectoral(MaeProcesoElectoral maeProcesoElectoral, MaeAmbitoElectoral maeAmbitoElectoral,
												 TabConfProcesoElectoral tabConfProcesoElectoral, Map<String, DetCargaArchivo> mapRow,
												 String numeroDocumento) throws Exception {
		TabConfAmbitoElectoral tabConfAmbitoElectoral = buildTabConfAmbitoElectoral(tabConfProcesoElectoral, mapRow, numeroDocumento);
		tabConfAmbitoElectoralServicio.crearTabConfAmbitoElectoral(tabConfAmbitoElectoral);
		copiarFichasABCDE(maeProcesoElectoral, maeAmbitoElectoral, tabConfAmbitoElectoral);
	}

	private TabConfAmbitoElectoral buildTabConfAmbitoElectoral(TabConfProcesoElectoral tabConfProcesoElectoral, Map<String, DetCargaArchivo> mapRow, String numeroDocumento) throws Exception {
		MaeUbigeo maeUbigeo = ubigeoServicio.obtenerMaeUbigeoPorUbigeo(mapRow.get("N_DIST_PK").getValor());
		TabConfAmbitoElectoral tabConfAmbitoElectoral = TabConfAmbitoElectoral.builder()
				.tabConfProcesoElectoral(tabConfProcesoElectoral)
				.maeUbigeo(maeUbigeo)
				.capital(mapRow.get("C_CAPITAL").getValor()).build();
		tabConfAmbitoElectoral.setActivo(COL_ACTIVO);
		tabConfAmbitoElectoral.setUsuarioCreacion(numeroDocumento);
		return tabConfAmbitoElectoral;
	}

	private void copiarFichasABCDE(MaeProcesoElectoral maeProcesoElectoral, MaeAmbitoElectoral maeAmbitoElectoral,
								   TabConfAmbitoElectoral tabConfAmbitoElectoral) throws Exception {
		//System.err.println(tabConfAmbitoElectoral.getMaeUbigeo().getIdUbigeo() + " -> "+ tabConfAmbitoElectoral.getMaeUbigeo().getUbigeo());
		logger.warning("Proceso elect:"+maeProcesoElectoral.getIdProcesoElectoral()+",  "
				+ "ODPE :"+maeAmbitoElectoral.getIdAmbitoElectoral()+" - "+maeAmbitoElectoral.getNombre()
				+ "UBIGEO: "+tabConfAmbitoElectoral.getMaeUbigeo().getIdUbigeo() + " -> "+ tabConfAmbitoElectoral.getMaeUbigeo().getUbigeo()
				+ "TIPO DE AMBITO: "+ maeAmbitoElectoral.getTipoAmbitoElectoral());
		FichaGenerica fichaGenerica = FichaGenerica.builder()
				.maeProcesoElectoral(maeProcesoElectoral)
				.maeAmbitoElectoral(maeAmbitoElectoral)
				.maeUbigeo(tabConfAmbitoElectoral.getMaeUbigeo())
				.build();
		fichaGenerica.setUsuarioCreacion(tabConfAmbitoElectoral.getUsuarioCreacion());
		logger.warning(new Gson().toJson(fichaGenerica));
		fichaGenerica = fichaGenericaServicio.copiarFichasABCDE(fichaGenerica);
	}

	@Override
	public MaeProcesoElectoral getProcesoElectoralPorAcronimoYNombre(String acronimo, String nombre) throws Exception {
		MaeProcesoElectoral procesoElectoral = MaeProcesoElectoral.builder()
				.acronimo(acronimo)
				.nombre(nombre)
				.build();
				procesoElectoralMapper.getProcesoElectoralByAcronimoNombre(procesoElectoral);
		MaeProcesoElectoral encontrado = (procesoElectoral.getProcesosElectorales() != null && procesoElectoral.getProcesosElectorales().size() > 0 ) ? procesoElectoral.getProcesosElectorales().get(0): null;
		return encontrado;
	}



}

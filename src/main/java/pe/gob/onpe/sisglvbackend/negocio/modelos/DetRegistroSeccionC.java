package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetRegistroSeccionC extends ModeloBase{
	private Integer idDetRegistroSeccionC;
	private CabRegistroSeccionC cabRegistroSeccionC;
	
	private String aula;
	private String pabellon;
	private Integer piso;
	private Integer requiereToldo;
	private Integer usoAula;
	
	private Integer cantidadMesa;
	private Integer tipoAula;
	private String  descripcionTipoAula;
	
	private Integer tieneLuminaria;
	private Integer cantidadLuminariaBuenEstado;
	private Integer cantidadLuminariaMalEstado;
	
	private Integer tieneTomaCorriente;
	private Integer cantidadTomacorrienteBuenEstado;
	private Integer cantidadTomacorrienteMalEstado;
	
	
	private Integer tieneInterruptor;
	private Integer cantidadInterruptorBuenEstado;
	private Integer cantidadInterruptorMalEstado;
	
	private Integer tieneVentana;
	private Integer cantidadVentanaBuenEstado;
	private Integer cantidadVentanaMalEstado;
	
	
	private Integer tienePuerta;
	private Integer cantidadPuertaBuenEstado;
	private Integer cantidadPuertaMalEstado;
	
	
	
	private Integer tienePuntoInternet;
	private Integer tienePuntoWifi;
	
	private String usuarioObservacion;
	private Date fechaObservacion;
	
	private String usuarioAprobacion;
	private Date fechaAprobacion;
	private Integer terminado;
	
	
	// lista de aulas
	private List<DetRegistroSeccionC> registrosC = new ArrayList<>();
	
	
}

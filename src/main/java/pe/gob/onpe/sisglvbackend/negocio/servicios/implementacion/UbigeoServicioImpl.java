package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.MaeUbigeoMapper;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.UbigeoMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.UbigeoServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotFoundRegisterInDb;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

@Service
public class UbigeoServicioImpl implements UbigeoServicio {

    @Autowired
    UbigeoMapper ubigeoMapper;

    @Autowired
    MaeUbigeoMapper maeUbigeoMapper;

    @Override
    public void listarDepartamentosActivos(MaeUbigeo param) throws Exception {
        ubigeoMapper.listarDepartamentosActivos(param);
        Funciones.validarOperacionConBaseDatos(param, UbigeoServicioImpl.class, "listarDepartamentosActivos");
    }

    @Override
    public void listarProvinciasActivos(MaeUbigeo param) throws Exception {
        ubigeoMapper.listarProvinciasActivos(param);
        Funciones.validarOperacionConBaseDatos(param, UbigeoServicioImpl.class, "listarProvinciasActivos");
    }

    @Override
    public void listarDistritosActivos(MaeUbigeo param) throws Exception {
        ubigeoMapper.listarDistritosActivos(param);
        Funciones.validarOperacionConBaseDatos(param, UbigeoServicioImpl.class, "listarDistritosActivos");
    }

    @Override
    public MaeUbigeo obtenerMaeUbigeoPorUbigeo(String ubigeo) throws Exception {
        MaeUbigeo maeUbigeo = MaeUbigeo.builder().ubigeo(ubigeo).build();
        maeUbigeoMapper.obtenerMaeUbigeoPorUbigeo(maeUbigeo);
        if(maeUbigeo.getLista().size() == 0){
            throw new NotFoundRegisterInDb("MAE_UBIGEO", "C_UBIGEO", ubigeo);
        }
        return maeUbigeo.getLista().get(0);
    }
}

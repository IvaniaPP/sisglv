package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiOdpeInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiBasicOutputDto;

/**
 * @author glennlq
 * @created 10/21/21
 */
public interface ApiOdpeServicio {
    ApiBasicOutputDto getOdpePorProcesoElectoral(ApiToken apiToken, ApiOdpeInputDto apiOdpeInputDto) throws Exception;
}

package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabFichaSeccionD extends ModeloBase {

    private Integer idFichaSeccionD;
    private MaeLocalVotacion maeLocalVotacion;
    private String seccion;
    private String observacionRegistro;
    private String usuarioObservacion;
    private String usuarioAprobacion;
    private Date fechaObservacion;
    private Date fechaAprobacion;

}

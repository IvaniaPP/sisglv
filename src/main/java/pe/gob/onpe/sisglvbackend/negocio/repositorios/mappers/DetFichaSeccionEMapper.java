
package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionE;

@Mapper
public interface DetFichaSeccionEMapper {
    
    void obtenerLocalVotacionHistDetSecE(DetFichaSeccionE param) throws Exception;
    
}

package pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers;

import org.apache.ibatis.annotations.Mapper;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetCargaArchivo;

/**
 * @author glennlq
 * @created 9/23/21
 */
@Mapper
public interface DetCargaArchivoMapper {
    DetCargaArchivo crearDetCargaArchivo(DetCargaArchivo detCargaArchivo) throws Exception;
    DetCargaArchivo obtenerDetCargaArchivoPorId(DetCargaArchivo detCargaArchivo) throws Exception;
    DetCargaArchivo listaDetCargaArchivoPorIdCargarArchivo(DetCargaArchivo detCargaArchivo) throws Exception;
}

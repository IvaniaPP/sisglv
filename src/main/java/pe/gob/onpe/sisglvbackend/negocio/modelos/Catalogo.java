package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Catalogo extends ModeloBase {
	private Integer idCatalogo;
	private Integer idCatalogoPadre;
	private String tabla;
	private String columna;
	private String nombre;
	private Integer codigoN;
	private String codigoC;
	private Integer orden;
	private String tipo;
	private String informacionAdicional;
        
   private List<Catalogo> catalogos;
}

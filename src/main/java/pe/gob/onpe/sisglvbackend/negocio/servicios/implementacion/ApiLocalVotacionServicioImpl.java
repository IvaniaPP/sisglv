package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.beans.ApiLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ApiLocalVotacionMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiBitacoraServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiLocalVotacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiTokenServicio;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.api.ApiLocalVotacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiBasicOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.api.ApiLocalVotacionOutputDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Service
public class ApiLocalVotacionServicioImpl implements ApiLocalVotacionServicio {

    @Autowired
    ApiLocalVotacionMapper apiLocalVotacionMapper;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ApiTokenServicio apiTokenServicio;

    @Autowired
    ApiBitacoraServicio apiBitacoraServicio;

    @Override
    public ApiBasicOutputDto getLocalVotacionPorProcesoElectoral(ApiToken apiToken, ApiLocalVotacionInputDto apiLocalVotacionInputDto) throws Exception {
        try {
            ApiLocalVotacion apiLocalVotacion = ApiLocalVotacion.builder().acronimo(apiLocalVotacionInputDto.getAcronimo()).build();
            apiLocalVotacionMapper.getLocalVotacionGenericoPorPE(apiLocalVotacion);
            Funciones.validarOperacionConBaseDatos(apiLocalVotacion, ApiLocalVotacionServicioImpl.class, "getLocalVotacionPorProcesoElectoral");
            apiBitacoraServicio.insertarApiBitacora(apiToken, 1, "OK");
            List<ApiLocalVotacionOutputDto> lista = buildListLocalVotacionOutputDto(apiLocalVotacion);
            return ApiBasicOutputDto.builder()
                    .estado(true)
                    .codigoError(0)
                    .mensaje("")
                    .cantidad(lista.size())
                    .resultado(lista)
                    .build();
        } catch (Exception e) {
            apiBitacoraServicio.insertarApiBitacora(apiToken, 0, e.getMessage().substring(0,255));
            return ApiBasicOutputDto.builder()
                    .estado(false)
                    .codigoError(1)
                    .mensaje(e.getMessage())
                    .resultado(null)
                    .build();
        }
    }

    private List<ApiLocalVotacionOutputDto> buildListLocalVotacionOutputDto(ApiLocalVotacion apiLocalVotacion) throws Exception {
        List<ApiLocalVotacionOutputDto> listaResultado = new ArrayList<>();
        for(ApiLocalVotacion localTmp : apiLocalVotacion.getLista()) {
            listaResultado.add(modelMapper.map(localTmp, ApiLocalVotacionOutputDto.class));
        }
        return listaResultado;
    }
}

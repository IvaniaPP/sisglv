package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabConfProcesoVerificacion extends ModeloBase {
	private Integer idConfProcesoVerificacion;
	private MaeProcesoVerificacion maeProcesoVerificacion;
	private MaeAmbitoElectoral maeAmbitoElectoral;
	private Integer notificacion;
	private Integer activo;
}

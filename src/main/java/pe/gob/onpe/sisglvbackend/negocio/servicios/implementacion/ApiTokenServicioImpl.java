package pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;
import pe.gob.onpe.sisglvbackend.negocio.repositorios.mappers.ApiTokenMapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ApiTokenServicio;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.TokenApiException;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;

/**
 * @author glennlq
 * @created 10/25/21
 */
@Service
public class ApiTokenServicioImpl implements ApiTokenServicio {

    @Autowired
    private ApiTokenMapper apiTokenMapper;

    @Override
    public ApiToken getApiTokenByToken(String token) throws TokenApiException {
        try {
            ApiToken apiToken = ApiToken.builder().token(token).build();
            apiTokenMapper.getToken(apiToken);
            Funciones.validarOperacionConBaseDatos(apiToken, ApiTokenServicioImpl.class, "getApiTokenByToken");
            if (apiToken.getLista().size() == 0) {
                throw new TokenApiException("Acceso restringido", HttpStatus.FORBIDDEN);
            }
            return apiToken.getLista().get(0);
        } catch (Exception e) {
            e.printStackTrace();
            throw new TokenApiException("Acceso restringido", HttpStatus.FORBIDDEN);
        }
    }

}

package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CabRegistroSeccionD extends ModeloBase{
	private Integer idRegistroSeccionD;
	private VerificacionLV verificacionLV;
	private String seccion;
	private String observacionRegistro;
	private String usuarioObservacion;
	private String usuarioAprobacion;
	private Date fechaObservacion;
	private Date fechaAprobacion;
	private Integer idEstado;
	private Integer terminado;
	private Integer estadoVerificacion;

	private List<CabRegistroSeccionD> seccionD = new ArrayList<>();
}

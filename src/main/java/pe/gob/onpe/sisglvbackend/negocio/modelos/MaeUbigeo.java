package pe.gob.onpe.sisglvbackend.negocio.modelos;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaeUbigeo extends ModeloBase {

	private Integer idUbigeo;
	private Integer idUbigeoPadre;
	private String ubigeo;
	private String nombre;
	private Integer tipoAmbitoGeografico;
	
	private Integer tipoProceso;
	private Integer idProceso;
	private Integer idAmbitoElectoral;
	
	private List<MaeUbigeo> lista = new ArrayList<>();

}

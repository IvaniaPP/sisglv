package pe.gob.onpe.sisglvbackend.negocio.modelos;


import java.util.ArrayList;

import java.util.List;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MaeAmbitoElectoral extends ModeloBase {
	private Integer idAmbitoElectoral;
	private Integer idAmbitoElectoralPadre;
	private String nombre;
	private String abreviatura;
	private Integer tipoAmbitoElectoral;
	private Integer correlativo;
	private Integer idProcesoElectoral;
	private Integer tipoProceso;
	
	private List<MaeAmbitoElectoral> lista = new ArrayList<>();

}

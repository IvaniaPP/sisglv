package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiBitacora;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ApiToken;

/**
 * @author glennlq
 * @created 10/26/21
 */
public interface ApiBitacoraServicio {
    void insertarApiBitacora(ApiBitacora apiBitacora) throws Exception;
    void insertarApiBitacora(ApiToken apiToken, Integer estado, String descripcion) throws Exception;
}

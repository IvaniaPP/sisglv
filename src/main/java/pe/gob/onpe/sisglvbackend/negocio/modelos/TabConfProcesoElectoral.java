package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabConfProcesoElectoral extends ModeloBase {
	private Integer idConfProcesoElectoral;
	private MaeProcesoElectoral maeProcesoElectoral;
	private MaeAmbitoElectoral maeAmbitoElectoral;
	private String sede;
	private Integer correlativo;
	private Integer notificacion;
}

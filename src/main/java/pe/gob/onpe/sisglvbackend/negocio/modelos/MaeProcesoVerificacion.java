package pe.gob.onpe.sisglvbackend.negocio.modelos;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MaeProcesoVerificacion extends ModeloBase {
	private Integer idProcesoVerificacion;
	private String nombre;
	private Integer tipoAmbitoElectoral;
	private Integer etapa;
	private String acronimo;
	private List<MaeProcesoVerificacion> procesosVerificaciones;
}

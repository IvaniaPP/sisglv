package pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces;

import pe.gob.onpe.sisglvbackend.negocio.beans.Persona;

public interface PadronService {
	
	public void consultarPadron(Persona persona);

}

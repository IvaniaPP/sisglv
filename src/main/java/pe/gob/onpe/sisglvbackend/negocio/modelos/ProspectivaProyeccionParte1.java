package pe.gob.onpe.sisglvbackend.negocio.modelos;

import lombok.*;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProspectivaProyeccionParte1  extends ModeloBase {
    String nombreProyeccion;
    Integer idProcesoElectoral;
    MaeProcesoElectoral procesoElectoral;
    Integer padronInicio;
    Integer padronFin;
    Integer padronReferencia;
    Date fechaInicio;
    Date fechaFin;
    Integer idPadronProyeccion;
    Integer idPadronReferencia;
}

package pe.gob.onpe.sisglvbackend.vista.wrapper;

import java.util.ArrayList;
import java.util.List;

import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarCategoriaArchivoSeccionEOutputDto;

public class RegistroSeccionEWrapper {
	
	
	public static final List<ListarCategoriaArchivoSeccionEOutputDto> obtenerListarCategoriaArchivoSeccionEOutputDto(List<DetRegistroSeccionE> origen) {
		if(origen==null) {
			return null;
		}
		
		List<ListarCategoriaArchivoSeccionEOutputDto> lista = new ArrayList<>();
		ListarCategoriaArchivoSeccionEOutputDto destino ;
		for(DetRegistroSeccionE seccionE : origen) {
			destino = new ListarCategoriaArchivoSeccionEOutputDto();
			destino.setCodigo(seccionE.getArchivo().getIdCategoria());
			destino.setNombre(seccionE.getArchivo().getCategoria());
			destino.setOrden(seccionE.getArchivo().getOrden());
			destino.setObligatorio(seccionE.getArchivo().getObligatorio());
			destino.setActivo(seccionE.getArchivo().getActivo());
			destino.setIdDetRegistroSeccionE(seccionE.getIdDetRegistroSeccionE());
			lista.add(destino);
		}
		
		
		return lista;
	}

}

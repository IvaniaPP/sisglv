package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseListarEditarRegistroNuevoLVOutputDto extends BaseOutputDto {
	
	private DatosListarEditarRegistroNuevoLVOutputDto datos;
	private Integer totalRegistros;

}

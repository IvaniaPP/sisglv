package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;



import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarUbigeoBasicoOutputDto;

@Setter
@Getter
public class ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto {

	
	private ListarUbigeoBasicoOutputDto departamento;
	private ListarUbigeoBasicoOutputDto provincia;
	private ListarUbigeoBasicoOutputDto distrito;
	private ListarCentroPobladoBasicoOutputDto centroPoblado;
	private Integer idTipoLocal;	
	private String nombreLocal;	
	private String numeroLocal;
	private Integer idCentroPoblado;
	private String observacionRegistro;
	private Integer idTipoAmbitoElectoral;
	private Integer idAmbitoElectoral;
	private Integer estadoVerificacion;
	
}

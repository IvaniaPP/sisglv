package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeCentroPoblado;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeCentroPobladoServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeUbigeoServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarUbigeoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarCentroPobladoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarUbigeoBasicoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarCentroPobladoBasicoOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarUbigeoBasicoOuputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.CentroPobladoWrapper;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/ubigeo")
@Validated
public class UbigeoController {
	@Autowired
	JWTTokenProvider jwtTokenProvider;
	
	@Autowired
	MaeUbigeoServicio maeUbigeoServicio;
	
	@Autowired
	MaeCentroPobladoServicio maeCentroPobladoServicio;
	
	
	@ApiOperation(value = "listar todos los departamentos", notes = "Permite listar todos los departamentos")
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	
	
	@GetMapping("/listar-departamento-all")
	public ResponseEntity<?> listarDepartamentoAll() throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		maeUbigeoServicio.listarDepartamento(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@ApiOperation(value = "listar departamento por ambito geografico", notes = "Permite listar los departamentos por ambito geografico"
			+ " \n1 = NACIONAL"
			+ " \n2 = INTERNACIONAL ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarUbigeoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	
	
	@GetMapping("/listar-departamento-ambito/{tipoAmbitoGeografico}")
	public ResponseEntity<?> listarDepartamentoPorAmbito(
			@PathVariable Optional<Integer> tipoAmbitoGeografico) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		if(tipoAmbitoGeografico.isPresent()) {
			param.setTipoAmbitoGeografico(tipoAmbitoGeografico.get());
		}
		
		maeUbigeoServicio.listarDepartamento(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	
	@ApiOperation(value = "listar provincia", notes = "Permite listar las provincias pro departamento")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarUbigeoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	@GetMapping("/listar-provincia/{idDepartamento}")
	public ResponseEntity<?> listarProvincia(@PathVariable(required = true) Integer idDepartamento ) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		param.setIdUbigeoPadre(idDepartamento);
		
		maeUbigeoServicio.listarProvincia(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@ApiOperation(value = "listar distrito", notes = "Permite listar los distritos por provincia")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarUbigeoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	@GetMapping("/listar-distrito/{idProvincia}")
	public ResponseEntity<?> listarDistrito(			@PathVariable Integer idProvincia ) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		param.setIdUbigeoPadre(idProvincia);
		
		maeUbigeoServicio.listarDistrito(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	
	@ApiOperation(value = "listar centro poblado", notes = "Permite listar los centros poblados por distrito")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarCentroPobladoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	@GetMapping("/listar-centro-poblado/{ubigeoDistrito}")
	public ResponseEntity<?> listarCentroPobladoPorUbigeo(@PathVariable String ubigeoDistrito ) throws Exception {

		MaeCentroPoblado param = new MaeCentroPoblado();
		param.setUbigeo(ubigeoDistrito);
		
		maeCentroPobladoServicio.listarCentroPobladoPorUbigeo(param);
		
		List<ListarCentroPobladoBasicoOutputDto> outputDto = CentroPobladoWrapper.ListarCentroPobladoBasicoOutputDtoModelToDtoWrapper(param.getLista());

		DatosListarCentroPobladoBasicoOutputDto datos = new DatosListarCentroPobladoBasicoOutputDto();
		datos.setCentrosPoblados(outputDto);
		
		ResponseListarCentroPobladoBasicoOuputDto response = new ResponseListarCentroPobladoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@ApiOperation(value = "listar centro poblado por id distrito", notes = "Permite listar los centros poblados por distrito")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarCentroPobladoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	@GetMapping("/listar-centro-poblado-por-distrito/{ubigeoIdDistrito}")
	public ResponseEntity<?> listarCentroPobladoPorIdUbigeo(@PathVariable Long ubigeoIdDistrito) throws Exception {

		MaeCentroPoblado param = new MaeCentroPoblado();
		param.setIdUbigeo(ubigeoIdDistrito);
		
		maeCentroPobladoServicio.listarCentroPobladoPorIdUbigeo(param);
		
		List<ListarCentroPobladoBasicoOutputDto> outputDto = CentroPobladoWrapper.ListarCentroPobladoBasicoOutputDtoModelToDtoWrapper(param.getLista());

		DatosListarCentroPobladoBasicoOutputDto datos = new DatosListarCentroPobladoBasicoOutputDto();
		datos.setCentrosPoblados(outputDto);
		
		ResponseListarCentroPobladoBasicoOuputDto response = new ResponseListarCentroPobladoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@ApiOperation(value = "listar departamento por proceso electoral y odpe", notes = "listar departamento por proceso electoral y odpe ")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarUbigeoBasicoOuputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
		  @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")			  
	)
	
	
	@GetMapping("/listar-departamento-proceso-ambito/{tipoProceso}/{idProceso}/{idAmbitoElectoral}")
	public ResponseEntity<?> listarDepartamentoPorProcesoAmbito(
			@PathVariable Optional<Integer> tipoProceso,@PathVariable Optional<Integer> idProceso,@PathVariable Optional<Integer> idAmbitoElectoral) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		if(tipoProceso.isPresent()) {
			param.setTipoProceso(tipoProceso.get());
		}
		
		if(idProceso.isPresent()) {
			param.setIdProceso(idProceso.get());
		}
		
		if(idAmbitoElectoral.isPresent()) {
			param.setIdAmbitoElectoral(idAmbitoElectoral.get());
		}

		maeUbigeoServicio.listarDepartamentoPorProcesoAmbito(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@GetMapping("/listar-provincia-proceso-ambito/{idUbigeoPadre}/{tipoProceso}/{idProceso}/{idAmbitoElectoral}")
	public ResponseEntity<?> listarProvinciaPorProcesoAmbito(@PathVariable Optional<Integer> idUbigeoPadre,
			@PathVariable Optional<Integer> tipoProceso,@PathVariable Optional<Integer> idProceso,@PathVariable Optional<Integer> idAmbitoElectoral) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		if(tipoProceso.isPresent()) {
			param.setTipoProceso(tipoProceso.get());
		}
		
		if(idProceso.isPresent()) {
			param.setIdProceso(idProceso.get());
		}
		
		if(idAmbitoElectoral.isPresent()) {
			param.setIdAmbitoElectoral(idAmbitoElectoral.get());
		}
		
		
		if(idUbigeoPadre.isPresent()) {
			param.setIdUbigeoPadre(idUbigeoPadre.get());
		}
		
		
		
		maeUbigeoServicio.listarProvinciaPorProcesoAmbito(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@GetMapping("/listar-distrito-proceso-ambito/{idUbigeoPadre}/{tipoProceso}/{idProceso}/{idAmbitoElectoral}")
	public ResponseEntity<?> listarDistritoPorProcesoAmbito(@PathVariable Optional<Integer> idUbigeoPadre,
			@PathVariable Optional<Integer> tipoProceso,@PathVariable Optional<Integer> idProceso,@PathVariable Optional<Integer> idAmbitoElectoral) throws Exception {

		MaeUbigeo param = new MaeUbigeo();
		if(tipoProceso.isPresent()) {
			param.setTipoProceso(tipoProceso.get());
		}
		
		if(idProceso.isPresent()) {
			param.setIdProceso(idProceso.get());
		}
		
		if(idAmbitoElectoral.isPresent()) {
			param.setIdAmbitoElectoral(idAmbitoElectoral.get());
		}
		
		if(idUbigeoPadre.isPresent()) {
			param.setIdUbigeoPadre(idUbigeoPadre.get());
		}
		
		maeUbigeoServicio.listarDistritoPorProcesoAmbito(param);
		
		List<ListarUbigeoBasicoOutputDto> outputDto = Funciones.mapAll(param.getLista(),ListarUbigeoBasicoOutputDto.class);

		DatosListarUbigeoBasicoOutputDto datos = new DatosListarUbigeoBasicoOutputDto();
		datos.setUbigeos(outputDto);
		
		ResponseListarUbigeoBasicoOuputDto response = new ResponseListarUbigeoBasicoOuputDto();
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());
		response.setDatos(datos);
		
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	
	
	
}

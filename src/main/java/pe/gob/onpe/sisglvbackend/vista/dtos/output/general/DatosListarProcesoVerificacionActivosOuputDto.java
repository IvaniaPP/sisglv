package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosListarProcesoVerificacionActivosOuputDto {
	List<ListarProcesoVerificacionActivosOuputDto> procesosVerificaciones;
}

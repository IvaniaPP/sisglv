package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatoObtenerAulaOutputDto {
	
	ListarDetRegistroSeccionCOutputDto aula;

}

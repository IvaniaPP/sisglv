package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeProcesoVerificacion;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeLocalVotacionServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProcesoVerificacionServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.AsignarOrcAVerificarProcesoDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral.ListaLocalVotacionDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.habilitarProcesoVerificacion.RegistrarProcesoVerificacionInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarProcesoVerificacionActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarProcesoVerificacionActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarProcesoVerificacionActivosOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.habilitarProcesoVerificacion.DatosRegistrarProcesoVerificacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.habilitarProcesoVerificacion.ResponseRegistrarProcesoVerificacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.MaeProcesoVerificacionWrapper;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/proceso-verificacion")
@Validated
public class ProcesoVerificacionController {

	@Autowired
    JWTTokenProvider jwtTokenProvider;
	
	@Autowired
	ProcesoVerificacionServicio procesoVerificacionServicio;
	
	@Autowired
	MaeLocalVotacionServicio maeLocalVotacionServicio;

	@ApiOperation(value = "listar procesos de verificación activos", notes = "Permite listar los procesos de verificación activos")
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
	@GetMapping("/listar-procesos-verificacion-activos")
	public ResponseEntity<?> listarProcesosVerificacionActivos() throws Exception {

		MaeProcesoVerificacion param = new MaeProcesoVerificacion();

		List<MaeProcesoVerificacion> paramOut = procesoVerificacionServicio.listarActivos(param);

		List<ListarProcesoVerificacionActivosOuputDto> paramDtoOut = MaeProcesoVerificacionWrapper
				.listarActivosModelToDtoWrapper(paramOut);

		DatosListarProcesoVerificacionActivosOuputDto objDto = new DatosListarProcesoVerificacionActivosOuputDto();
		objDto.setProcesosVerificaciones(paramDtoOut);

		ResponseListarProcesoVerificacionActivosOuputDto outDto = new ResponseListarProcesoVerificacionActivosOuputDto();
		outDto.setResultado(param.getResultado());
		outDto.setMensaje(param.getMensaje());
		outDto.setDatos(objDto);
		return new ResponseEntity<>(outDto, HttpStatus.OK);
	}

	@ApiOperation(value = "registrar proceso verificacion", notes = "Permite registrar la información de un proceso de verificación")
	@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarProcesoVerificacionOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
			@ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class) })
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))

	@PostMapping("/registrar-proceso-verificacion")
	public ResponseEntity<ResponseRegistrarProcesoVerificacionOutputDto> registrarProcesoVerificacion(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestBody @Valid final RegistrarProcesoVerificacionInputDto paramInputDto) throws Exception {

		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		MaeProcesoVerificacion param = Funciones.map(paramInputDto, MaeProcesoVerificacion.class);
		param.setUsuarioCreacion(numeroDocumento);

		procesoVerificacionServicio.registrarProcesoVerificacion(param);

		DatosRegistrarProcesoVerificacionOutputDto datos = new DatosRegistrarProcesoVerificacionOutputDto();
		datos.setIdProcesoVerificacion(param.getIdProcesoVerificacion());

		ResponseRegistrarProcesoVerificacionOutputDto response = new ResponseRegistrarProcesoVerificacionOutputDto();
		response.setDatos(datos);
		response.setResultado(param.getResultado());
		response.setMensaje(param.getMensaje());

		return new ResponseEntity<ResponseRegistrarProcesoVerificacionOutputDto>(response, HttpStatus.OK);
	}

	
	@PostMapping(value = "/cargar-archivo-orcs")
	public ResponseEntity<?> cargarArchivoOdpes(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestParam("idProcesoVerificacion") Integer idProcesoVerificacion, @RequestParam("file") MultipartFile multipartFile) throws Exception {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		return ResponseEntity.ok(procesoVerificacionServicio.cargarArchivoOrcsAFtp(idProcesoVerificacion, multipartFile, numeroDocumento));
	}
	
	@PostMapping(value = "/asignar-orcs-a-proceso-verificacion")
	public ResponseEntity<?> asignarOrcsAVerificacionProceso(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestBody AsignarOrcAVerificarProcesoDto asignarOrcAProcesoElectoralDto) throws Exception {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		return ResponseEntity.ok(procesoVerificacionServicio.asignarOrcsAVerificarProceso(asignarOrcAProcesoElectoralDto, numeroDocumento));
	}
	
	@PostMapping(value = "/lista-local-votacion")
	public ResponseEntity<?> listaLocalVotacion(@RequestBody ListaLocalVotacionDto listaLocalVotacionDto) throws Exception {
		return ResponseEntity.ok(maeLocalVotacionServicio.getListaLocalVotacionOrcGenerico(listaLocalVotacionDto));
	}

}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

import java.util.List;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaReporteSeccionABCDEOutputDto extends BaseOutputDto {
	List<ReporteSeccionABOutputDto> seccionAB;
	List<ReporteSeccionCOutputDto> seccionC;
	List<ReporteSeccionDOutputDto> seccionD;
	List<ReporteSeccionEOutputDto> seccionE;
}

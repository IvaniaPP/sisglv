package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnviarValidacionNuevoLVInputDto {
	
	@NotNull(message = "Id de verificación es requerido")
	private Integer idVerificacion;

}

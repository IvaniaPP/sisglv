package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListaLocalVotacionDto {
    private Integer idProcesoElectoral;
    private Integer idProcesoVerificacion;
}

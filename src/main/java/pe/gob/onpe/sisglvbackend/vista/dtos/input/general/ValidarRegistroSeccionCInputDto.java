package pe.gob.onpe.sisglvbackend.vista.dtos.input.general;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidarRegistroSeccionCInputDto {

	@NotNull(message = "ID del registro de la seccion A")
	private Integer idCabRegistroSeccionC;
	
	private String observacionRegistro;
	

	
}

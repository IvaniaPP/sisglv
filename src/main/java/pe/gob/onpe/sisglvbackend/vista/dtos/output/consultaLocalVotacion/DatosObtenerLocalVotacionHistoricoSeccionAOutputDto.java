package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DatosObtenerLocalVotacionHistoricoSeccionAOutputDto {
    private ObtenerLocalVotacionHistoricoSeccionAOutputDto  localVotacionHistoricoSeccionA;
}

package pe.gob.onpe.sisglvbackend.vista.wrapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarDetRegistroSeccionCInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDetRegistroSeccionCInputDto;

public  class DetRegistroSeccionCWrapper {
	
	public DetRegistroSeccionC RegistrarDetSeccionCInputDtoToModelWrapper(RegistrarDetRegistroSeccionCInputDto origen) {
		DetRegistroSeccionC destino = new DetRegistroSeccionC();
		
		if(origen==null) {
			return null;
		}
		
		destino.setCabRegistroSeccionC(new CabRegistroSeccionC());
		destino.getCabRegistroSeccionC().setVerificacionLV(new VerificacionLV());
		destino.getCabRegistroSeccionC().getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacionLV());
		destino.getCabRegistroSeccionC().setSeccion(origen.getSeccion());
		destino.setAula(origen.getAula());
		destino.setPabellon(origen.getPabellon());
		destino.setPiso(origen.getPiso());
		destino.setRequiereToldo(origen.getRequiereToldo());
		destino.setCantidadMesa(origen.getCantidadMesa());
		destino.setTipoAula(origen.getTipoAula());
		destino.setTieneLuminaria(origen.getTieneLuminaria());
		destino.setCantidadLuminariaBuenEstado(origen.getCantidadLuminariaBuenEstado());
		destino.setCantidadLuminariaMalEstado(origen.getCantidadLuminariaMalEstado());
		destino.setTieneTomaCorriente(origen.getTieneTomaCorriente());
		destino.setCantidadTomacorrienteBuenEstado(origen.getCantidadTomacorrienteBuenEstado());
		destino.setCantidadTomacorrienteMalEstado(origen.getCantidadTomacorrienteMalEstado());
		destino.setTieneInterruptor(origen.getTieneInterruptor());
		destino.setCantidadInterruptorBuenEstado(origen.getCantidadInterruptorBuenEstado());
		destino.setCantidadInterruptorMalEstado(origen.getCantidadInterruptorMalEstado());
		destino.setTieneVentana(origen.getTieneVentana());
		destino.setCantidadVentanaBuenEstado(origen.getCantidadVentanaBuenEstado());
		destino.setCantidadVentanaMalEstado(origen.getCantidadVentanaMalEstado());
		destino.setTienePuerta(origen.getTienePuerta());
		destino.setCantidadPuertaBuenEstado(origen.getCantidadPuertaBuenEstado());
		destino.setCantidadPuertaMalEstado(origen.getCantidadPuertaMalEstado());
		destino.setTienePuntoInternet(origen.getTienePuntoInternet());
		destino.setTienePuntoWifi(origen.getTienePuntoWifi());
		
		
		return destino;
		
	}	
	
	public DetRegistroSeccionC ActualizarDetRegistroSeccionCInputDtoToModelWrapper(ActualizarDetRegistroSeccionCInputDto origen) {
		DetRegistroSeccionC destino = new DetRegistroSeccionC();
		
		if(origen==null) {
			return null;
		}
		
		destino.setIdDetRegistroSeccionC(origen.getIdDetRegistroSeccionC());
		destino.setAula(origen.getAula());
		destino.setPabellon(origen.getPabellon());
		destino.setPiso(origen.getPiso());
		destino.setRequiereToldo(origen.getRequiereToldo());
		destino.setCantidadMesa(origen.getCantidadMesa());
		destino.setTipoAula(origen.getTipoAula());
		destino.setTieneLuminaria(origen.getTieneLuminaria());
		destino.setCantidadLuminariaBuenEstado(origen.getCantidadLuminariaBuenEstado());
		destino.setCantidadLuminariaMalEstado(origen.getCantidadLuminariaMalEstado());
		destino.setTieneTomaCorriente(origen.getTieneTomaCorriente());
		destino.setCantidadTomacorrienteBuenEstado(origen.getCantidadTomacorrienteBuenEstado());
		destino.setCantidadTomacorrienteMalEstado(origen.getCantidadTomacorrienteMalEstado());
		destino.setTieneInterruptor(origen.getTieneInterruptor());
		destino.setCantidadInterruptorBuenEstado(origen.getCantidadInterruptorBuenEstado());
		destino.setCantidadInterruptorMalEstado(origen.getCantidadInterruptorMalEstado());
		destino.setTieneVentana(origen.getTieneVentana());
		destino.setCantidadVentanaBuenEstado(origen.getCantidadVentanaBuenEstado());
		destino.setCantidadVentanaMalEstado(origen.getCantidadVentanaMalEstado());
		destino.setTienePuerta(origen.getTienePuerta());
		destino.setCantidadPuertaBuenEstado(origen.getCantidadPuertaBuenEstado());
		destino.setCantidadPuertaMalEstado(origen.getCantidadPuertaMalEstado());
		destino.setTienePuntoInternet(origen.getTienePuntoInternet());
		destino.setTienePuntoWifi(origen.getTienePuntoWifi());
		destino.setActivo(origen.getActivo());
		
		
		return destino;
		
	}	
}

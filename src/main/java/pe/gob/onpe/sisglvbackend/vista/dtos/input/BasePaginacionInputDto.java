package pe.gob.onpe.sisglvbackend.vista.dtos.input;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BasePaginacionInputDto {
    private Integer pagina;
    private Integer totalRegistroPorPagina;
}

package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;

import pe.gob.onpe.sisglvbackend.negocio.modelos.Aplicacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.Usuario;
import pe.gob.onpe.sisglvbackend.negocio.modelos.UsuarioSesion;
import pe.gob.onpe.sisglvbackend.negocio.servicios.implementacion.captcha.CaptchaServicioV3;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CaptchaServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.UsuarioServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteJwt;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.NotAuthorizedException;
import pe.gob.onpe.sisglvbackend.transversal.properties.ValidarCustomProperties;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.ActualizarNuevaClaveInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.AsignarPersonaUsuarioGeneradoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.CargarAccesosInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.seguridad.LoginInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.CargarAccesoDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginDatosOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad.LoginOutputDto;


//@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Validated
@RequestMapping(ConstanteApi.VERSION_API + "/api/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioServicio usuarioServicio;

	@Autowired
	private JWTTokenProvider jwtTokenProvider;

	@Autowired
	private ValidarCustomProperties validarCustomProperties;
	
	@Autowired
	private CaptchaServicio captchaServiceV3;
	
	public UsuarioController() {

	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody @Valid final LoginInputDto paramInputDto) throws Exception {
		// Implement
	
			//LoginOutputDto outputDto = usuarioServicio.accederSistema(paramInputDto);
		// Implement
		String recaptcha = paramInputDto.getRecaptcha();
		if(validarCustomProperties.getServicioCaptcha()) {
					captchaServiceV3.processResponse(recaptcha, CaptchaServicioV3.IMPORTANT_ACTION);
		}
			LoginDatosOutputDto datos = usuarioServicio.accederSistema(paramInputDto);
		
			HashMap<String, Object> resp = new HashMap<>();
			resp.put("resultado", 1);
			resp.put("mensaje", "La operación se ejecutó correctamente.");
			resp.put("datos", datos.getDatos());
			return new ResponseEntity<>(resp, HttpStatus.OK);
		
	}
	
	@PostMapping("/cargar-accesos")
	public ResponseEntity<?> cargarAccesos(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token ,@RequestBody @Valid final CargarAccesosInputDto paramInputDto) throws Exception {
		
		CargarAccesoDatosOutputDto datos =  usuarioServicio.cargarAccesos(paramInputDto,token);
		
		HashMap<String,Object> resp = new HashMap<>();
		resp.put("resultado", 1);
		resp.put("mensaje", "La operación se ejecutó correctamente.");
		resp.put("datos", datos.getDatos());
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@PostMapping("/actualizar-nueva-clave")
	public ResponseEntity<?> actualizarNuevaClave(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final ActualizarNuevaClaveInputDto paramInputDto) throws Exception {
		
		HashMap<String, Object> resp = usuarioServicio.actualizarNuevaClave(paramInputDto,token);
		resp.put("resultado", 1);
		resp.put("mensaje","La operación se ejecutó correctamente.");
	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@GetMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token)
			throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();

		token = jwtTokenProvider.obtenerSoloToken(token);
		Claims claims = Jwts.parser().setSigningKey(ConstanteJwt.SECRET).parseClaimsJws(token).getBody();

		Map<String, Object> expectedMap = Funciones.getMapFromIoJsonwebtokenClaims((DefaultClaims) claims);

		String tokenRefresh = jwtTokenProvider.generarRefreshToken(expectedMap,
				expectedMap.get("numeroDocumento").toString());

		HashMap<String, Object> response = new HashMap<String, Object>();

		response.put("resultado", 1);
		response.put("mensaje", "todo correcto");
		response.put("token", tokenRefresh);

		return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
	}
	
	@PostMapping("/asignar-persona-usuario-autogenerado")
	public ResponseEntity<?> asignarPersonaUsuarioAutogenerado(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token,@RequestBody @Valid AsignarPersonaUsuarioGeneradoInputDto usuarioInputDto)
			throws Exception {
		
		return new ResponseEntity<>(usuarioServicio.asignarPersonaAUsuarioGenerardo(usuarioInputDto, token), HttpStatus.OK);

	}
	
	
	
	
/*	@PostMapping("/actualizar-nueva-clave")
	public ResponseEntity<?> actualizarNuevaClave(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final ActualizarNuevaClaveInputDto paramInputDto) throws Exception {
		
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		Integer idUsuario = (Integer) jwtTokenProvider.obtenerValorToken(token, "idUsuario");
		
		Usuario paramUsuario = new Usuario();
		paramUsuario.setUsuarioModificacion(numeroDocumento);
		paramUsuario.setIdUsuario(idUsuario);
		paramUsuario.setClave(paramInputDto.getClave());
		
		usuarioServicio.actualizarNuevaClave(paramUsuario);
	

		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("resultado", paramUsuario.getResultado());
		resp.put("mensaje", paramUsuario.getMensaje());
	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	@PostMapping("/autenticacion-puesto-cero")
	public ResponseEntity<?> autenticacionPuestoCero( 
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final LoginPuestoCeroInputDto paramInputDto) throws Exception {
		// Implement
			
		Integer idUsuario = (Integer) jwtTokenProvider.obtenerValorToken(token, "idUsuario");
		Usuario paramUsuario = new Usuario();
		
		paramUsuario.setIdUsuario(idUsuario);
		paramUsuario.setUsuario(paramInputDto.getUsuario());
		paramUsuario.setClave(paramInputDto.getClave());
		
		Aplicacion paramAplicacion = new Aplicacion();
		paramAplicacion.setCodigo(paramInputDto.getCodigo());
		paramUsuario.setAplicacion(paramAplicacion);

		usuarioServicio.validarPuestaCero(paramUsuario);
				
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("resultado", paramUsuario.getResultado());
		resp.put("mensaje", paramUsuario.getMensaje());
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	@PostMapping("/listar-usuario-autogenerado")
	public ResponseEntity<?> listarUsuarioAutogenerado(
			@RequestBody @Valid ListarUsuarioAutogeneradoInputDto paramInputDto) throws Exception {
		// Implement		
		Usuario param = Funciones.map(paramInputDto, Usuario.class);
		param.setPagina(paramInputDto.getPagina());
		param.setTotalRegistroPorPagina(paramInputDto.getTotalRegistroPorPagina());
		
		param.setAplicacion(new Aplicacion());
		param.getAplicacion().setIdAplicacion(paramInputDto.getIdAplicacion());
		
		param.setProcesoElectoral(new ProcesoElectoral());
		param.getProcesoElectoral().setIdProceso(paramInputDto.getIdProcesoElectoral());
		
		param.setUnidadOrganica(new UnidadOrganica());
		param.getUnidadOrganica().setIdUnidadOrganica(paramInputDto.getIdUnidadOrganica());
		
		param.setPersona(new Persona());
		param.getPersona().setNumeroDocumento(paramInputDto.getNumeroDocumento());
		param.getPersona().setNombres(paramInputDto.getNombres());
		param.getPersona().setApellidoPaterno(paramInputDto.getApellidoPaterno());
		param.getPersona().setApellidoMaterno(paramInputDto.getApellidoMaterno());
		
		usuarioServicio.listarUsuariosAutogenerado(param);

		List<ListarUsuarioAutogeneradoOutputDto> usuariosOutputDto = Funciones.mapAll(param.getUsuarios(),
				ListarUsuarioAutogeneradoOutputDto.class);

		HashMap<String, Object> resultado = new HashMap<String, Object>();
		resultado.put("resultado", param.getResultado());
		resultado.put("mensaje", param.getMensaje());
		resultado.put("lista", usuariosOutputDto);
		resultado.put("totalRegistros", param.getUsuarios().size() == 0 ? 0 : param.getUsuarios().get(0).getTotalRegistros());

		return new ResponseEntity<>(resultado, HttpStatus.OK);
	}
	
	@PostMapping("/actualizar-usuario-autogenerado")
	public ResponseEntity<?> actualizarUsuarioAutogenerado(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token,@RequestBody @Valid ActualizarUsuarioInputDto usuarioInputDto)
			throws Exception {
		
		// Implement
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		
		Usuario usuario = Funciones.map(usuarioInputDto, Usuario.class);
		usuario.setUsuarioModificacion(numeroDocumento);
		
		usuarioServicio.actualizarUsuarioAutogenerado(usuario);

		HashMap<String, Object> resultado = new HashMap<String, Object>();
		resultado.put("resultado", usuario.getResultado());
		resultado.put("mensaje", usuario.getMensaje());
		resultado.put("idUsuario", usuario.getIdUsuario());

		return new ResponseEntity<>(resultado, HttpStatus.OK);

	}
	
	@PostMapping("/asignar-persona-usuario-autogenerado")
	public ResponseEntity<?> asignarPersonaUsuarioAutogenerado(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token,@RequestBody @Valid AsignarPersonaUsuarioGeneradoInputDto usuarioInputDto)
			throws Exception {
		
		// Implement
		String nombreUsuario = (String)jwtTokenProvider.obtenerValorToken(token, "nombreUsuario");
		
		Usuario usuario = Funciones.map(usuarioInputDto, Usuario.class);
		usuario.setUsuarioModificacion(nombreUsuario);
		usuarioServicio.asignarPersonaUsuarioGenerado(usuario);

		HashMap<String, Object> resultado = new HashMap<String, Object>();
		resultado.put("resultado", usuario.getResultado());
		resultado.put("mensaje", usuario.getMensaje());
		resultado.put("idUsuario", usuario.getIdUsuario());

		return new ResponseEntity<>(resultado, HttpStatus.OK);

	}
	
	
	@PostMapping("/generar-sesion-activa")
	public ResponseEntity<?> generarSesionActiva(@RequestBody @Valid final LoginInputDto paramInputDto) throws Exception {
		// Implement
		String recaptcha = paramInputDto.getRecaptcha();
		if(validarCustomProperties.getServicioCaptcha()) {
			captchaServiceV3.processResponse(recaptcha, CaptchaServicioV3.IMPORTANT_ACTION);
		}
		
		Usuario paramUsuario = Funciones.map(paramInputDto, Usuario.class);
		Aplicacion paramAplicacion = new Aplicacion();
		paramAplicacion.setCodigo(paramInputDto.getCodigo());
		paramUsuario.setAplicacion(paramAplicacion);

		usuarioServicio.accederSistemaGenerarSesion(paramUsuario);
		LoginOutputDto outputDto = new LoginOutputDto();
		
		outputDto.setPerfiles(Funciones.mapAll(paramUsuario.getUsuariosPerfiles(), LoginPerfilesOutputDto.class));
		/*	
		outputDto.setModulos(Funciones.mapAll(paramUsuario.getModulos(), LoginModulosOutputDto.class));
		outputDto.setOpciones(Funciones.mapAll(paramUsuario.getOpciones(), LoginOpcionesOutputDto.class));
		outputDto.setAcciones(Funciones.mapAll(paramUsuario.getAcciones(), LoginAccionesOutputDto.class));
		*/
	/*	outputDto.setUsuario(Funciones.map(paramUsuario, LoginUsuarioOutputDto.class));
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("resultado", 1);
		resp.put("mensaje", "La operación se ejecutó correctamente.");
		resp.put("datos", outputDto);
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}*/
	
/*	@GetMapping("/cerrar-sesion")
	public ResponseEntity<?> cerraSesion(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token) throws Exception {
		// Implement
			
		Integer idUsuario = (Integer)jwtTokenProvider.obtenerValorToken(token, "idUsuario");
		Integer idAplicacion = (Integer)jwtTokenProvider.obtenerValorToken(token, "idAplicacion");
		String idSesion = (String)jwtTokenProvider.obtenerValorToken(token, "idSesion");
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		Usuario paramUsuario = new Usuario();
		paramUsuario.setIdUsuario(idUsuario);		
		Aplicacion paramAplicacion = new Aplicacion();
		paramAplicacion.setIdAplicacion(idAplicacion);
		paramUsuario.setAplicacion(paramAplicacion);
		paramUsuario.setUsuarioSesion(new UsuarioSesion());
		paramUsuario.getUsuarioSesion().setCodigoSession(idSesion);
		paramUsuario.setUsuarioModificacion(numeroDocumento);

		usuarioServicio.cerrarSesion(paramUsuario);
			
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("resultado", paramUsuario.getResultado());
		resp.put("mensaje", paramUsuario.getMensaje());
	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@GetMapping("/validar-sesion")
	public ResponseEntity<?> validarSesion(@RequestHeader(value =
	        HttpHeaders.AUTHORIZATION) String token) throws Exception {
		// Implement
			
		Integer idUsuario = (Integer)jwtTokenProvider.obtenerValorToken(token, "idUsuario");
		Integer idAplicacion = (Integer)jwtTokenProvider.obtenerValorToken(token, "idAplicacion");
		String idSesion = (String)jwtTokenProvider.obtenerValorToken(token, "idSesion");
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		Usuario paramUsuario = new Usuario();
		paramUsuario.setIdUsuario(idUsuario);		
		Aplicacion paramAplicacion = new Aplicacion();
		paramAplicacion.setIdAplicacion(idAplicacion);
		paramUsuario.setAplicacion(paramAplicacion);
		paramUsuario.setUsuarioSesion(new UsuarioSesion());
		paramUsuario.getUsuarioSesion().setCodigoSession(idSesion);
		paramUsuario.setUsuarioModificacion(numeroDocumento);

		usuarioServicio.validarSesionActiva(paramUsuario);
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("resultado", paramUsuario.getUsuarioSesion().getEstado());
		resp.put("mensaje", paramUsuario.getMensaje());
	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}*/

}

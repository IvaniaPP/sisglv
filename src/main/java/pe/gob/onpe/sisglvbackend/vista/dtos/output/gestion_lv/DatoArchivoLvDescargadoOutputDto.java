package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatoArchivoLvDescargadoOutputDto {

	private Integer idArchivo;
	private String  guid;
	private String 	filename;
	private String  formato;
	private String  filenameOriginal;
	private String  fileBase64;
	private String  peso;
	private String  categoria;
	
}

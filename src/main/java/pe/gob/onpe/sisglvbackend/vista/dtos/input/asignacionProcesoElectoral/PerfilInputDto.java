package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
public class PerfilInputDto {
	
	
	private Integer idPerfil;
	
	@NotNull(message = "El nombre no puede ser nulo")
	private String nombre;
	
	@NotNull(message = "El abreviatura no puede ser nulo")
	private String abreviatura;
}

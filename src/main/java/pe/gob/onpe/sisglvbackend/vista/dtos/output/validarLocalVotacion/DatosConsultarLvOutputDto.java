package pe.gob.onpe.sisglvbackend.vista.dtos.output.validarLocalVotacion;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerListaFiltroLocalVotacionOutputDto;

@Setter
@Getter
public class DatosConsultarLvOutputDto {
	
	private ObtenerListaFiltroLocalVotacionOutputDto locales;
	private Integer totalRegistros;
	
	

}

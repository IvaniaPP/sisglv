package pe.gob.onpe.sisglvbackend.vista.dtos.output.validarLocalVotacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResultConsultarLvOutputDto extends BaseOutputDto {
	
	
	private DatosConsultarLvOutputDto datos;
}

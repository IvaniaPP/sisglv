package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto {
	private Integer tienePatio;
	private Integer cantidadPatios;
	
	private Integer idEstadoConservacionPuertasAulas;
	private Integer idEstadoConservacionTechosAulas;
	
	private Integer idEstadoConservacionParedesAulas;
	private Integer idEstadoConservacionVentanasAulas;
	
	private Integer idEstadoConservacionPisosAulas;
	private String suministroEnergiaElectrica;
	
	private Integer cantidadCirculoSeguridad;
	private Float areaAproximadaM2;
	private Float areaConstruidaM2;
	private Float areaSinConstruirM2;
	private String observacionRegistro;
	
}

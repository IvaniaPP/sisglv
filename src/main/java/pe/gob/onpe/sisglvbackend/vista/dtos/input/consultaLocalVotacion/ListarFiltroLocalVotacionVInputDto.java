package pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarFiltroLocalVotacionVInputDto {
    
    private Integer idAmbitoElectoral;
    private Integer tipoProceso;
    private Integer idProceso;
    private String  nombreLocal;
    private Integer estadoVerificacion;
    private Integer estadoDisponible;
    private Integer idUbigeoDepartamento;
    private Integer idUbigeoProvincia;
    private Integer idUbigeoDistrito;
    private Integer pagina;
    private Integer totalRegistrosPorPagina;
    
    
}

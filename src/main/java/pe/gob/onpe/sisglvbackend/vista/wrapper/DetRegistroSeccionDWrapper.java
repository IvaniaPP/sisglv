package pe.gob.onpe.sisglvbackend.vista.wrapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarDetRegistroSeccionDInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarDetRegistroSeccionDInputDto;

public  class DetRegistroSeccionDWrapper {
	
	public DetRegistroSeccionD RegistrarDetSeccionDInputDtoToModelWrapper(RegistrarDetRegistroSeccionDInputDto origen) {
		DetRegistroSeccionD destino = new DetRegistroSeccionD();
		
		if(origen==null) {
			return null;
		}
		
		destino.setCabRegistroSeccionD(new CabRegistroSeccionD());
		destino.getCabRegistroSeccionD().setVerificacionLV(new VerificacionLV());
		destino.getCabRegistroSeccionD().getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacionLV());
		destino.getCabRegistroSeccionD().setSeccion(origen.getSeccion());
		destino.setTipoArea(origen.getTipoArea());
		destino.setAncho(origen.getAncho());
		destino.setLargo(origen.getLargo());
		destino.setReferencia(origen.getReferencia());
		destino.setTieneTecho(origen.getTieneTecho());
		destino.setTienePuerta(origen.getTienePuerta());
		destino.setCantidadPuerta(origen.getCantidadPuerta());
		destino.setTieneAccesoLuz(origen.getTieneAccesoLuz());
		destino.setObservacion(origen.getObservacion());
		return destino;
		
	}	
	
	public DetRegistroSeccionD ActualizarDetSeccionDInputDtoToModelWrapper(ActualizarDetRegistroSeccionDInputDto origen) {
		DetRegistroSeccionD destino = new DetRegistroSeccionD();
		
		if(origen==null) {
			return null;
		}
		
		destino.setIdDetRegistroSeccionD(origen.getIdDetRegistroSeccionD());
		destino.setTipoArea(origen.getTipoArea());
		destino.setAncho(origen.getAncho());
		destino.setLargo(origen.getLargo());
		destino.setReferencia(origen.getReferencia());
		destino.setTieneTecho(origen.getTieneTecho());
		destino.setTienePuerta(origen.getTienePuerta());
		destino.setCantidadPuerta(origen.getCantidadPuerta());
		destino.setTieneAccesoLuz(origen.getTieneAccesoLuz());
		destino.setObservacion(origen.getObservacion());
		return destino;
		
	}
}

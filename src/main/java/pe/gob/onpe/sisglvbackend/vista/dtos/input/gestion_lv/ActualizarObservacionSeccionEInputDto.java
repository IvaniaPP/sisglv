package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarObservacionSeccionEInputDto {

	@NotNull(message = "La sección es obligatoria")
	private String seccion;
	
	@NotNull(message = "El número de verificación es obligatorio")
	private Integer nverificacion;
	
	private String observacionRegistro;
	
}

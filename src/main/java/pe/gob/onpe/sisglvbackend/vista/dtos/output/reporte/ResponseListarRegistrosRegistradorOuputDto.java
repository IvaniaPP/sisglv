package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Schema(name = "ResponseListarRegistrosRegistradorOuputDto", description = "The entity that represents Episode")
public class ResponseListarRegistrosRegistradorOuputDto extends BaseOutputDto{
	private DatosListarRegistrosRegistradorOuputDto datos;	
}

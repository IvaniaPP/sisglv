package pe.gob.onpe.sisglvbackend.vista.controllers;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionA;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FichaSeccionB;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeLocalVotacionHistoricoServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion.ListarFiltroLocalLocalVotacionHistoricoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.*;
import pe.gob.onpe.sisglvbackend.vista.wrapper.MaeLocalVotacionHistoricoWrapper;

import javax.validation.Valid;
import java.util.List;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabFichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabFichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabFichaSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetFichaSeccionE;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/local-votacion-historico")
@Validated
public class LocalVotacionHistoricoController {

    @Autowired
    MaeLocalVotacionHistoricoServicio maeLocalVotacionHistoricoServicio;

    @ApiOperation(value = "Lista de locales de votación historico",
            notes = "Permite listar los locales de votación historico"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @PostMapping(value = "/listar-filtro-local-votacion-historico")
    public ResponseEntity<ResponseListarFiltroLocalVotacionHistoricoOutputDto> listarFiltroLocalLocalVotacionHistorico(
            @RequestBody @Valid final ListarFiltroLocalLocalVotacionHistoricoInputDto paramDto
    ) throws Exception {

        MaeLocalVotacion param = new MaeLocalVotacion();
        MaeLocalVotacionHistoricoWrapper.ListarFiltroLocalLocalVotacionHistoricoDtoToModelWrapper(paramDto, param);

        List<MaeLocalVotacion> paramOut = maeLocalVotacionHistoricoServicio.listarLocalesVotacionHistorico(param);
        List<ListarFiltroLocalVotacionHistoricoOutputDto> paramDtoOut = MaeLocalVotacionHistoricoWrapper.ListarFiltroLocalLocalVotacionHistoricoModelToDtoWrapper(paramOut);
        ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDto paramTotalesDtoOut = MaeLocalVotacionHistoricoWrapper.ListarFiltroLocalLocalVotacionHistoricoTotalesOutputDtoModelToDtoWrapper(paramOut);

        DatosListarFiltroLocalVotacionHistoricoOutputDto objDto = new DatosListarFiltroLocalVotacionHistoricoOutputDto();
        objDto.setLocalesVotacionHistorico(paramDtoOut);
        objDto.setTotales(paramTotalesDtoOut);

        ResponseListarFiltroLocalVotacionHistoricoOutputDto outDto = new ResponseListarFiltroLocalVotacionHistoricoOutputDto();
        outDto.setResultado(param.getResultado());
        outDto.setMensaje(param.getMensaje());
        outDto.setDatos(objDto);

        return new ResponseEntity<>(outDto, HttpStatus.OK);
    }

    @ApiOperation(value = "obtiene información del local de votación histórico de la sección A",
            notes = "Permite obtener la información del local de votación histórico sección A"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @GetMapping(value = "/obtener-local-votacion-historico-seccion-a/{idFichaSeccionA}")
    public ResponseEntity<ResponseObtenerLocalVotacionHistoricoSeccionAOutputDto> obtenerLocalLocalVotacionHistoricoSeccionA(
            @PathVariable Integer idFichaSeccionA
    ) throws Exception {
        MaeLocalVotacion param = new MaeLocalVotacion();
        param.setFichaSeccionA(new FichaSeccionA());
        param.getFichaSeccionA().setIdFichaSeccionA(idFichaSeccionA);

        MaeLocalVotacion paramOut = maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoSeccionA(param);
        ObtenerLocalVotacionHistoricoSeccionAOutputDto paramDtoOut = MaeLocalVotacionHistoricoWrapper.ObtenerLocalLocalVotacionHistoricoSeccionAModelToDtoWrapper(paramOut);

        DatosObtenerLocalVotacionHistoricoSeccionAOutputDto objDto = new DatosObtenerLocalVotacionHistoricoSeccionAOutputDto();
        objDto.setLocalVotacionHistoricoSeccionA(paramDtoOut);

        ResponseObtenerLocalVotacionHistoricoSeccionAOutputDto outDto = new ResponseObtenerLocalVotacionHistoricoSeccionAOutputDto();
        outDto.setResultado(param.getResultado());
        outDto.setMensaje(param.getMensaje());
        outDto.setDatos(objDto);

        return new ResponseEntity<>(outDto, HttpStatus.OK);
    }

    @ApiOperation(value = "obtiene información del local de votación histórico de la sección B",
            notes = "Permite obtener la información del local de votación histórico sección B"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @GetMapping(value = "/obtener-local-votacion-historico-seccion-b/{idFichaSeccionB}")
    public ResponseEntity<ResponseObtenerLocalVotacionHistoricoSeccionBOutputDto> obtenerLocalLocalVotacionHistoricoSeccionB(
            @PathVariable Integer idFichaSeccionB
    ) throws Exception {

        MaeLocalVotacion param = new MaeLocalVotacion();
        param.setFichaSeccionB(new FichaSeccionB());
        param.getFichaSeccionB().setIdFichaSeccionB(idFichaSeccionB);

        MaeLocalVotacion paramOut = maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoSeccionB(param);
        ObtenerLocalVotacionHistoricoSeccionBOutputDto paramDtoOut = MaeLocalVotacionHistoricoWrapper.ObtenerLocalLocalVotacionHistoricoSeccionBModelToDtoWrapper(paramOut);

        DatosObtenerLocalVotacionHistoricoSeccionBOutputDto objDto = new DatosObtenerLocalVotacionHistoricoSeccionBOutputDto();
        objDto.setLocalVotacionHistoricoSeccionB(paramDtoOut);

        ResponseObtenerLocalVotacionHistoricoSeccionBOutputDto outDto = new ResponseObtenerLocalVotacionHistoricoSeccionBOutputDto();
        outDto.setResultado(param.getResultado());
        outDto.setMensaje(param.getMensaje());
        outDto.setDatos(objDto);
        return new ResponseEntity<>(outDto, HttpStatus.OK);
    }

    @ApiOperation(value = "obtiene información del local de votación histórico de la sección C",
            notes = "Permite obtener la información del local de votación histórico sección C"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @GetMapping(value = "/obtener-local-votacion-historico-seccion-c/{idCabFichaSeccionC}")
    public ResponseEntity<ResponseObtenerLocalVotacionHistoricoSeccionCOutputDto> obtenerLocalLocalVotacionHistoricoSeccionC(
            @PathVariable Integer idCabFichaSeccionC) throws Exception {

        MaeLocalVotacion paramMaeLocal = new MaeLocalVotacion();
        paramMaeLocal.setCabFichaSeccionC(new CabFichaSeccionC());
        paramMaeLocal.getCabFichaSeccionC().setIdFichaSeccionC(idCabFichaSeccionC);

        MaeLocalVotacion paramCabOut = maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoSeccionC(paramMaeLocal);

        DetFichaSeccionC paramDetFicha = new DetFichaSeccionC();
        paramDetFicha.setCabFichaSeccionC(new CabFichaSeccionC());
        paramDetFicha.getCabFichaSeccionC().setIdFichaSeccionC(idCabFichaSeccionC);

        maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoDetSeccionC(paramDetFicha);
        ObtenerLocalVotacionHistoricoSeccionCOutputDto paramDtoOut = MaeLocalVotacionHistoricoWrapper.ObtenerLocalLocalVotacionHistoricoSeccionCModelToDtoWrapper(paramCabOut, paramDetFicha.getLista());

        ResponseObtenerLocalVotacionHistoricoSeccionCOutputDto objDto = new ResponseObtenerLocalVotacionHistoricoSeccionCOutputDto();
        objDto.setDatos(paramDtoOut);
        objDto.setResultado(paramCabOut.getResultado());
        objDto.setMensaje(paramCabOut.getMensaje());
        return new ResponseEntity<>(objDto, HttpStatus.OK);
    }
    
    @ApiOperation(value = "obtiene información del local de votación histórico de la sección D",
            notes = "Permite obtener la información del local de votación histórico sección D"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @GetMapping(value = "/obtener-local-votacion-historico-seccion-d/{idCabFichaSeccionD}")
    public ResponseEntity<ResponseObtenerLocalVotacionHistoricoSeccionDOutputDto> obtenerLocalLocalVotacionHistoricoSeccionD(
            @PathVariable Integer idCabFichaSeccionD) throws Exception {

        MaeLocalVotacion paramMaeLocal = new MaeLocalVotacion();
        paramMaeLocal.setCabFichaSeccionD(new CabFichaSeccionD());
        paramMaeLocal.getCabFichaSeccionD().setIdFichaSeccionD(idCabFichaSeccionD);

        MaeLocalVotacion paramCabOut = maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoSeccionD(paramMaeLocal);

        DetFichaSeccionD paramDetFicha = new DetFichaSeccionD();
        paramDetFicha.setCabFichaSeccionD(new CabFichaSeccionD());
        paramDetFicha.getCabFichaSeccionD().setIdFichaSeccionD(idCabFichaSeccionD);

        maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoDetSeccionD(paramDetFicha);
        ObtenerLocalVotacionHistoricoSeccionDOutputDto paramDtoOut = MaeLocalVotacionHistoricoWrapper.ObtenerLocalLocalVotacionHistoricoSeccionDModelToDtoWrapper(paramCabOut, paramDetFicha.getLista());

        ResponseObtenerLocalVotacionHistoricoSeccionDOutputDto objDto = new ResponseObtenerLocalVotacionHistoricoSeccionDOutputDto();
        objDto.setDatos(paramDtoOut);
        objDto.setResultado(paramCabOut.getResultado());
        objDto.setMensaje(paramCabOut.getMensaje());
        return new ResponseEntity<>(objDto, HttpStatus.OK);
    }

    @ApiOperation(value = "obtiene información del local de votación histórico de la sección E",
            notes = "Permite obtener la información del local de votación histórico sección E"
    )
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @GetMapping(value = "/obtener-local-votacion-historico-seccion-e/{idCabFichaSeccionE}")
    public ResponseEntity<ResponseObtenerLocalVotacionHistoricoSeccionEOutputDto> obtenerLocalLocalVotacionHistoricoSeccionE(
            @PathVariable Integer idCabFichaSeccionE) throws Exception {

        MaeLocalVotacion paramMaeLocal = new MaeLocalVotacion();
        paramMaeLocal.setCabFichaSeccionE(new CabFichaSeccionE());
        paramMaeLocal.getCabFichaSeccionE().setIdFichaSeccionE(idCabFichaSeccionE);

        MaeLocalVotacion paramCabOut = maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoSeccionE(paramMaeLocal);

        DetFichaSeccionE paramDetFicha = new DetFichaSeccionE();
        paramDetFicha.setCabFichaSeccionE(new CabFichaSeccionE());
        paramDetFicha.getCabFichaSeccionE().setIdFichaSeccionE(idCabFichaSeccionE);

        maeLocalVotacionHistoricoServicio.obtenerLocalVotacionHistoricoDetSeccionE(paramDetFicha);
        ObtenerLocalVotacionHistoricoSeccionEOutputDto paramDtoOut = MaeLocalVotacionHistoricoWrapper.ObtenerLocalLocalVotacionHistoricoSeccionEModelToDtoWrapper(paramCabOut, paramDetFicha.getLista());

        ResponseObtenerLocalVotacionHistoricoSeccionEOutputDto objDto = new ResponseObtenerLocalVotacionHistoricoSeccionEOutputDto();
        objDto.setDatos(paramDtoOut);
        objDto.setResultado(paramCabOut.getResultado());
        objDto.setMensaje(paramCabOut.getMensaje());
        return new ResponseEntity<>(objDto, HttpStatus.OK);
    }
    
}

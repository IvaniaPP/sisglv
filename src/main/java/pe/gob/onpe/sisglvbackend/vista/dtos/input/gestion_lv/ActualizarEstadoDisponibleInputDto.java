package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;


import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarEstadoDisponibleInputDto {

	@NotNull(message = "ID de la verificacion es obligatorio")
	private Integer idVerificacion;
	@NotNull(message = "ID del estado disponible es obligatorio")
	private Integer idEstadoDisponible;
	private String  observacion;
}

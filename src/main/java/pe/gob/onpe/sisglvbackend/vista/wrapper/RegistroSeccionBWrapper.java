package pe.gob.onpe.sisglvbackend.vista.wrapper;

import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionB;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarCaracteristicasLVPrimerPasoInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarCaracteristicasPrimerPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.RegistrarCaracteristicasSegundoPasoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto;

public class RegistroSeccionBWrapper {
	public static final void RegistrarCaracteristicasPrimerPasoDtoToModelWrapper(RegistrarCaracteristicasPrimerPasoLVInputDto origen,RegistroSeccionB destino) {
		if(origen==null) {
			return;
		}
		
		destino.setVerificacionLV(new VerificacionLV());
		destino.getVerificacionLV().setIdVerificacionLV(origen.getIdVerificacion());
		destino.setTieneCercoPerimetrico(origen.getTieneCercoPerimetrico());
		destino.setEstadoCercoPerimetrico(origen.getIdEstadoCerco());
		destino.setCantidadPuertaAcceso(origen.getCantidaPuertasAcceso());
		destino.setEstadoPuestaAcceso(origen.getIdEstadoPuertasAcceso());
		destino.setCantidadSshh(origen.getCantidaSshh());
		
		destino.setEstadoSshh(origen.getIdEstadoSshh());
		
		destino.setTieneAgua(origen.getTieneServicioAgua());		
		destino.setHorarioInicioAgua(origen.getDesdeHoraServicioAgua());
		destino.setHorarioTerminoAgua(origen.getHastaHoraServicioAgua());
		
		
		destino.setTieneLuz(origen.getTieneEnergiaElectrica());
		destino.setHorarioInicioLuz(origen.getDesdeHoraEnergiaElectrica());
		destino.setHorarioTerminoLuz(origen.getHastaHoraEnergiaElectrica());
		
		destino.setTieneInternet(origen.getTieneServicioIntenet());
		destino.setProveedorInternet(origen.getProveedorInternet());
		
	}
	public static final void RegistrarCaracteristicasSegundoPasoDtoToModelWrapper(RegistrarCaracteristicasSegundoPasoLVInputDto origen,RegistroSeccionB destino) {
		if(origen==null) {
			return;
		}
		
		destino.setIdRegistroSeccionB(origen.getIdRegistroSeccionB());
		destino.setTienePatio(origen.getTienePatio());
		destino.setCantidadPatio(origen.getCantidadPatios());
		destino.setEstadoPuertaAula(origen.getIdEstadoConservacionPuertasAulas());
		destino.setEstadoTechoAula(origen.getIdEstadoConservacionTechosAulas());
		destino.setEstadoParedAula(origen.getIdEstadoConservacionParedesAulas());
		destino.setEstadoVentanaAula(origen.getIdEstadoConservacionVentanasAulas());
		destino.setEstadoPisoAula(origen.getIdEstadoConservacionPisosAulas());
		destino.setSuministroLuz(origen.getSuministroEnergiaElectrica());
		destino.setCantidadCirculoSeguridad(origen.getCantidadCirculoSeguridad());
		destino.setAreaAproximadaM2(origen.getAreaAproximadaM2());
		destino.setAreaConstruidaM2(origen.getAreaConstruidaM2());
		destino.setAreaSinConstruirM2(origen.getAreaSinConstruirM2());
		
	}
	
	public static final ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto ObtenerCaracteristicasPrimerPasoModelToDtoWrapper(RegistroSeccionB origen) {
		if(origen==null) {
			return null;
		}
		ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto destino = new ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto();
		destino.setTieneCercoPerimetrico(origen.getTieneCercoPerimetrico());
		destino.setIdEstadoCerco(origen.getEstadoCercoPerimetrico());
		destino.setCantidaPuertasAcceso(origen.getCantidadPuertaAcceso());
		destino.setIdEstadoPuertasAcceso(origen.getEstadoPuestaAcceso());
		destino.setCantidaSshh(origen.getCantidadSshh());
		destino.setIdEstadoSshh(origen.getEstadoSshh());
		destino.setTieneServicioAgua(origen.getTieneAgua());
		destino.setDesdeHoraServicioAgua(origen.getHorarioInicioAgua());
		destino.setHastaHoraServicioAgua(origen.getHorarioTerminoAgua());
		destino.setTieneEnergiaElectrica(origen.getTieneLuz());
		destino.setDesdeHoraEnergiaElectrica(origen.getHorarioInicioLuz());
		destino.setHastaHoraEnergiaElectrica(origen.getHorarioTerminoLuz());
		destino.setTieneServicioIntenet(origen.getTieneInternet());
		destino.setProveedorInternet(origen.getProveedorInternet());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		
		return destino;
	}
	public static final ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto ObtenerCaracteristicasSegundoPasoModelToDtoWrapper(RegistroSeccionB origen) {
		if(origen==null) {
			return null;
		}
		ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto destino = new ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto();
		destino.setTienePatio(origen.getTienePatio());
		destino.setCantidadPatios(origen.getCantidadPatio());
		destino.setIdEstadoConservacionPuertasAulas(origen.getEstadoPuertaAula());
		destino.setIdEstadoConservacionTechosAulas(origen.getEstadoTechoAula());
		destino.setIdEstadoConservacionParedesAulas(origen.getEstadoParedAula());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setIdEstadoConservacionVentanasAulas(origen.getEstadoVentanaAula());
		destino.setIdEstadoConservacionPisosAulas(origen.getEstadoPisoAula());
		destino.setSuministroEnergiaElectrica(origen.getSuministroLuz());
		
		destino.setCantidadCirculoSeguridad(origen.getCantidadCirculoSeguridad());
		destino.setAreaAproximadaM2(origen.getAreaAproximadaM2());
		destino.setAreaConstruidaM2(origen.getAreaConstruidaM2());
		destino.setAreaSinConstruirM2(origen.getAreaSinConstruirM2());
		return destino;
	}
	
	public static final ObtenerCaracteristicasLocalVotacionOutputDto ObtenerCaracteristicasModelToDtoWrapper(RegistroSeccionB origen) {
		if(origen==null) {
			return null;
		}
		ObtenerCaracteristicasLocalVotacionOutputDto destino = new ObtenerCaracteristicasLocalVotacionOutputDto();
		destino.setTieneCercoPerimetrico(origen.getTieneCercoPerimetrico());
		destino.setIdEstadoCercoPerimetrico(origen.getEstadoCercoPerimetrico());
		destino.setCantidadPuertaAcceso(origen.getCantidadPuertaAcceso());
		destino.setIdEstadoPuertaAcceso(origen.getEstadoPuestaAcceso());
		destino.setCantidadSshh(origen.getCantidadSshh());
		destino.setIdEstadoSshh(origen.getEstadoSshh());
		destino.setTieneServicioAgua(origen.getTieneAgua());
		destino.setDesdeServicioAgua(origen.getHorarioInicioAgua());
		destino.setHastaServicioAgua(origen.getHorarioTerminoAgua());
		destino.setTieneServicioEnergiaElectrica(origen.getTieneLuz());
		destino.setDesdeServicioEnergiaElectrica(origen.getHorarioInicioLuz());
		destino.setHastaServicioEnergiaElectrica(origen.getHorarioTerminoLuz());
		destino.setTieneServicioInternet(origen.getTieneInternet());
		destino.setProveedorInternet(origen.getProveedorInternet());
		destino.setTienePatio(origen.getTienePatio());
		destino.setCantidadPatio(origen.getCantidadPatio());
		destino.setIdEstadoConservacionPuertasAulas(origen.getEstadoPuertaAula());
		destino.setIdEstadoConservacionTechosAulas(origen.getEstadoTechoAula());
		destino.setIdEstadoConservacionParedesAulas(origen.getEstadoParedAula());
		destino.setObservacionRegistro(origen.getObservacionRegistro());
		destino.setIdEstadoConservacionVentanasAulas(origen.getEstadoVentanaAula());
		destino.setIdEstadoConservacionPisosAulas(origen.getEstadoPisoAula());
		destino.setSuministroEnergiaElectrica(origen.getSuministroLuz());
		
		destino.setCantidadCirculoSeguridad(origen.getCantidadCirculoSeguridad());
		destino.setAreaAproximadaM2(origen.getAreaAproximadaM2());
		destino.setAreaConstruidaM2(origen.getAreaConstruidaM2());
		destino.setAreaSinConstruirM2(origen.getAreaSinConstruirM2());
		destino.setEstadoVerificacion(origen.getEstadoVerificacion());
		
		return destino;
	}
	public static final void ActualizarRegistroSeccionBPrimerPasoDtoToModelWrapper(ActualizarCaracteristicasLVPrimerPasoInputDto origen,RegistroSeccionB destino) {
		
		if(origen==null) {
			return;
		}
		
		destino.setIdRegistroSeccionB(origen.getIdRegistroSeccionB());
		destino.setTieneCercoPerimetrico(origen.getTieneCercoPerimetrico());
		destino.setEstadoCercoPerimetrico(origen.getIdEstadoCerco());
		destino.setCantidadPuertaAcceso(origen.getCantidaPuertasAcceso());
		destino.setEstadoPuestaAcceso(origen.getIdEstadoPuertasAcceso());
		destino.setCantidadSshh(origen.getCantidaSshh());
		destino.setEstadoSshh(origen.getIdEstadoSshh());
		destino.setTieneAgua(origen.getTieneServicioAgua());
		destino.setHorarioInicioAgua(origen.getDesdeHoraServicioAgua());
		destino.setHorarioTerminoAgua(origen.getHastaHoraServicioAgua());
		
		destino.setTieneLuz(origen.getTieneEnergiaElectrica());
		destino.setHorarioInicioLuz(origen.getDesdeHoraEnergiaElectrica());
		destino.setHorarioTerminoLuz(origen.getHastaHoraEnergiaElectrica());
		
		destino.setTieneInternet(origen.getTieneServicioIntenet());
		destino.setProveedorInternet(origen.getProveedorInternet());
		
	}
}

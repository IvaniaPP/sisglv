package pe.gob.onpe.sisglvbackend.vista.dtos.input.asignacionProcesoElectoral;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrarProcesoElectoralInicioInputDto {
	
	private RegistrarProcesoElectoralInputDto proceso;
	private List<OdpeInputDto> listaOdpes;

}

package pe.gob.onpe.sisglvbackend.vista.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.gob.onpe.sisglvbackend.negocio.modelos.*;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.ProspectivaServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/prospectiva-electoral-download")
@Validated
public class ProspectivaElectoralDownloadController {


	@Autowired
	private ProspectivaServicio prospectivaServicio;

	@RequestMapping(value = "/obtener-excel-estadistica-resultado/{idPadronProyeccion}/{tipoAmbito}", method = RequestMethod.GET)
	public void obtenerExcelEstadisticaResultado(HttpServletRequest request, HttpServletResponse response,
												 @PathVariable("idPadronProyeccion") Integer idPadronProyeccion,
												 @PathVariable("tipoAmbito") Integer tipoAmbito ) {
		try {
			ProspectivaEstadisticaResultado resultado = ProspectivaEstadisticaResultado.builder()
				.idPadronProyeccion(idPadronProyeccion)
					.tipoAmbito(tipoAmbito).build();
			prospectivaServicio.obtenerExcelEstadisticaResultado(request, response, resultado);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/obtener-excel-proyeccion-resultado/{idPadronProyeccion}/{tipoAmbito}/{idPadronParametro}", method = RequestMethod.GET)
	public void obtenerExcelProyeccionResultado(HttpServletRequest request, HttpServletResponse response,
												@PathVariable("idPadronProyeccion") Integer idPadronProyeccion,
												@PathVariable("tipoAmbito") Integer tipoAmbito,
												@PathVariable("idPadronParametro") Integer idPadronParametro) {
		try {
			ProspectivaProyeccionResultado resultado = ProspectivaProyeccionResultado.builder()
					.idPadronProyeccion(idPadronProyeccion)
					.tipoAmbito(tipoAmbito)
					.idPadronParametro(idPadronParametro).build();
			prospectivaServicio.obtenerExcelProyeccionResultado(request, response, resultado);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

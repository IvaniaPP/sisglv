package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DatosLocalesVotacionOutputDto {
    
    private Integer procesoVerificacion;
    private Integer ubigeo;
    private Integer ubigeoNivel1;       
    private String nombreUbigeoNivel1;
    private Integer ubigeoNivel2;
    private String nombreUbigeoNivel2;
    private Integer ubigeoNivel3;
    private String nombreUbigeoNivel3;
    private Integer centroPobladoN;
    private String centroPobladoC;
    private String nombreLV;
    private String numeroLV;
    
    private Integer idTipoVia;
    private String nombretipoVia;
    private String nombreVia;
    private Integer direccionNumero;
    private Float direccionKilometro;
    private String direccionManzana;
    private String direccionInterior;
    private String direccionDepartamento;
    private String direccionLote;
    private Integer direccionPiso;
    private Integer IdTipoZona;
    private String nombreTipoZona;
    private String nombreZona;
    
    private Integer tipoLV;
    private String nombreTipoLV;
    private Integer estadoLVN;
    private String estadoLVC;
    private Integer estadoVerificacion;
    private String estadoVerificacionLV;
    private Date fechaCreacion;
    private Date fechaModificacion;
}




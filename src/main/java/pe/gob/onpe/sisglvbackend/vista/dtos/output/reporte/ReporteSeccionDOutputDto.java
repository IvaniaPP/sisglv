package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
public class ReporteSeccionDOutputDto {
	private String codigoHistoricoLV;
	private String nombreLv;
	private String tipoArea;
	private Double largo;
	private Double ancho;
	private String referencia;
	private String tieneTecho;
	private String tienePuertaSeccionD;
	private Integer cantidadPuertaD;
	private String tieneLuzD;
}

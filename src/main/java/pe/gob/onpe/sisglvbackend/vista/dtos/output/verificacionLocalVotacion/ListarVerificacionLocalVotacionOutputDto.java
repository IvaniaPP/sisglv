package pe.gob.onpe.sisglvbackend.vista.dtos.output.verificacionLocalVotacion;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class ListarVerificacionLocalVotacionOutputDto {
    private String codigoUbigeo;
    private String nombreUbigeoNivel1;
    private String nombreUbigeoNivel2;
    private String nombreUbigeoNivel3;
    private String nombreCentroPoblado;
    private String nombreLocalVotacion;
    private Integer idTipoVia;
    private String nombreVia;
    private Integer direccionNumero;
    private Float direccionKilometro;
    private String direccionManzana;
    private String direccionDepartamento;
    private String direccionLote;
    private Integer direccionPiso;
    private Integer idTipoZona;
    private String nombreZona;
    private Date fechaModificacion;
    private Integer idEstadoVerificacion;
    private Integer totalPaginas;
    private Integer totalRegistros;
}

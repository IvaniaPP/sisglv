package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosListarSeccionesSislgvOuputDto {
	
	List<ListarRegistrosSeccionesSisglvOutputDto> registros;
}

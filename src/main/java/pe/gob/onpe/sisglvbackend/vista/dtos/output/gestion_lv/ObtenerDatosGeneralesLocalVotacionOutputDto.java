package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ObtenerDatosGeneralesLocalVotacionOutputDto {

	private String dniRegistrador;
	private String nombreApellidoRegistrador;
	private Integer idDependencia;
	private Integer idAmbitoElectoral;
	private String  ambitoElecotoral;
	
	private String nombreDepartamento;
	private String nombreProvincia;
	private String nombreDistrito;
	private String codigoUbigeoNivel1;
	private String codigoUbigeoNivel2;
	private String codigoUbigeoNivel3;
	private String nombreCentroPoblado;
	private Integer idCentroPoblado;
	private String latitudDistrito;
	private String longitudDistrito;
	
	
	private Integer idTipoLocal;	
	private String nombreLocal;	
	private String numeroLocal;
	private Integer idTipoInstitucion;
	private Integer idCategoriaInstitucion;
	private String otraCategoria;
	private Integer idNivel;
	private Integer idTipoVia;
	private String nombreVia;
	private Integer numero;
	private Float kilometro;
	private String manzana;
	private String interior;
	private String departamento;
	private String lote;
	private Integer piso;
	private Integer idTipoZona;
	private String nombreZona;
	private String referencia;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private Float distanciaCentroDistrito;
	private String tiempoCentroDistrito;
	private String documentoIdentidadRepresentante;
	private String nombreCompletoRepresentante;
	private String cargoRepresentante;
	private String telefonoRepresentante;
	private String correoRepresentante;
	private String documentoIdentidadRepresentanteApafa;
	private String nombreCompletoRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	
	private Integer cantidadAula;
	private Integer cantidadMesa;
	private String codigoMinedu;
	private Integer idTipoTecnologia;
	
	private String observacionRegistro;
	

	
	

	

	
}

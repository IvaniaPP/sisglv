package pe.gob.onpe.sisglvbackend.vista.dtos.output.habilitarProcesoVerificacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseRegistrarProcesoVerificacionOutputDto extends BaseOutputDto{
	
	private DatosRegistrarProcesoVerificacionOutputDto datos;

}

package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.onpe.sisglvbackend.ftp.ArchivoFTP;
import pe.gob.onpe.sisglvbackend.ftp.ConfArchivoFTP;
import pe.gob.onpe.sisglvbackend.negocio.modelos.ArchivoLV;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionD;
import pe.gob.onpe.sisglvbackend.negocio.modelos.DetRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.FiltroRegistroSeccionE;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeLocalVotacion;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeUbigeo;
import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionA;
import pe.gob.onpe.sisglvbackend.negocio.modelos.RegistroSeccionB;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.CabRegistroSeccionEServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionCServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionDServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionAServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionBServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.SFTPService;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.VerificacionLVServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.CustomBussinessException;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.CustomException;
import pe.gob.onpe.sisglvbackend.transversal.excepciones.ExceptionResponse;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.consultaLocalVotacion.ListarFiltroLocalVotacionVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.general.ValidarRegistroSeccionCInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.*;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion.ObtenerListaFiltroLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ActualizarTerminadoSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatoArchivoLvDescargadoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosArchivoLvListadoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosArchivoLvObtenidoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosArchivoLvSubidoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosCabSeccionEListadoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosDetSeccionEListadoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarCategoriaArchivoSeccionEOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosSeccionValidadoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatoObtenerAreaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatoObtenerAulaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosActualizarObservacionLvSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarDetRegistroSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarDetRegistroSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerCaracteristicasLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosRegistrarCaracteristicasPrimerPasoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosRegistrarDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosRegistrarDatosGeneralesPrimerPasoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosRegistrarDetRegistroSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosRegistrarDetRegistroSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarCaracteristicaAreaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarCaracteristicaAulaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarCategoriaArchivoSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarDetRegistroSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarDetRegistroSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.LocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseActualizarObservacionLvSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseActualizarObservacionSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseDescargarArchivoLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseEliminarArchivoLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerArchivoLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerCabSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerCaracteristicasLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarArchivosLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarCategoriaArchivoSeccionEOuputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarDetRegistroSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarDetRegistroSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarValidarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerAreaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerAulaOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseRegistrarCaracteristicasPrimerPasoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseRegistrarDatosGeneralesLocalVotacionOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseRegistrarDatosGeneralesPrimerPasoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseRegistrarDetRegistroSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseRegistrarDetRegistroSeccionDOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseSubirArchivoLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseValidarSeccionEOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.validarLocalVotacion.DatosConsultarLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.validarLocalVotacion.ResultConsultarLvOutputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.DetRegistroSeccionCWrapper;
import pe.gob.onpe.sisglvbackend.vista.wrapper.DetRegistroSeccionDWrapper;
import pe.gob.onpe.sisglvbackend.vista.wrapper.RegistroSeccionAWrapper;
import pe.gob.onpe.sisglvbackend.vista.wrapper.RegistroSeccionBWrapper;
import pe.gob.onpe.sisglvbackend.vista.wrapper.RegistroSeccionEWrapper;
import pe.gob.onpe.sisglvbackend.vista.wrapper.VerificacionLVWrapper;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/local-votacion")
@Validated
public class LocalVotacionController {

    @Autowired
    JWTTokenProvider jwtTokenProvider;

    @Autowired
    RegistroSeccionAServicio registroSeccionAServicio;

    @Autowired
    private RegistroSeccionCServicio detRegistroSeccionCServicio;

    @Autowired
    RegistroSeccionBServicio registroSeccionBServicio;
    
    @Autowired
    private VerificacionLVServicio verificacionLVServicio;
    
    @Autowired
	private CabRegistroSeccionEServicio cabRegistroSeccionEServicio;


    @Autowired
    private RegistroSeccionDServicio detRegistroSeccionDServicio;
    
    @Autowired
	RegistroSeccionDServicio registroSeccionDServicio;
    
    @Autowired
	private SFTPService ftpService;


    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarDatosGeneralesLocalVotacionOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-datos-generales-local-votacion")
    public ResponseEntity<?> registrarDatosGeneralesLocalVotacion(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDatosGeneralesLocalVotacionInputDto paramInputDto) throws Exception {

        // Implement
    	// recuperar el dni del token
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionA param = new RegistroSeccionA();
        RegistroSeccionAWrapper.RegistroDatosGeneralesDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioCreacion(numeroDocumento);
        registroSeccionAServicio.registrarDatosGeneralesPrimerPaso(param);

        DatosRegistrarDatosGeneralesLocalVotacionOutputDto datos = new  DatosRegistrarDatosGeneralesLocalVotacionOutputDto();
        datos.setIdRegistroSeccionA(param.getIdRegistroSeccionA());
       
        ResponseRegistrarDatosGeneralesLocalVotacionOutputDto response = new ResponseRegistrarDatosGeneralesLocalVotacionOutputDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        response.setDatos(datos);
        		
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación del primer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarDatosGeneralesPrimerPasoLVOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-datos-generales-local-votacion-primer-paso")
    public ResponseEntity<?> registrarDatosGeneralesLocalVotacionPrimerPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDatosGeneralesPrimerPasoLVInputDto paramInputDto) throws Exception {

        // Implement
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionA param = new RegistroSeccionA();
        RegistroSeccionAWrapper.RegistroDatosGeneralesPrimerPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioCreacion(numeroDocumento);
        registroSeccionAServicio.registrarDatosGeneralesPrimerPaso(param);

        HashMap<String, Object> resultDatos = new HashMap<>();
        resultDatos.put("idVerificacion", param.getVerificacionLV().getIdVerificacionLV());
        resultDatos.put("idRegistroSeccionA", param.getIdRegistroSeccionA());
        
        DatosRegistrarDatosGeneralesPrimerPasoLVOutputDto datos = new DatosRegistrarDatosGeneralesPrimerPasoLVOutputDto();
        datos.setIdRegistroSeccionA( param.getIdRegistroSeccionA());
        datos.setIdVerificacion(param.getVerificacionLV().getIdVerificacionLV());
        
        ResponseRegistrarDatosGeneralesPrimerPasoLVOutputDto response = new ResponseRegistrarDatosGeneralesPrimerPasoLVOutputDto();
        response.setResultado( param.getResultado());
        response.setMensaje(param.getMensaje());
        response.setDatos(datos);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación del segundo paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-datos-generales-local-votacion-segundo-paso")
    public ResponseEntity<?> registrarDatosGeneralesLocalVotacionSegundoPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDatosGeneralesSegundoPasoLVInputDto paramInputDto) throws Exception {

    	// recuperar el dni del token
        // Implement
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionA param = new RegistroSeccionA();
        RegistroSeccionAWrapper.RegistroDatosGeneralesSegundoPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioModificacion(numeroDocumento);
        
        registroSeccionAServicio.registrarDatosGeneralesSegundoPaso(param);

        BaseOutputDto response = new BaseOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
      
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación del tercer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-datos-generales-local-votacion-tercer-paso")
    public ResponseEntity<?> registrarDatosGeneralesLocalVotacionTercerPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDatosGeneralesTercerPasoLVInputDto paramInputDto) throws Exception {

        // Implement
    	// recuperar el dni del token
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionA param = new RegistroSeccionA();
        RegistroSeccionAWrapper.RegistroDatosGeneralesTercerPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioModificacion(numeroDocumento);
        registroSeccionAServicio.registrarDatosGeneralesTercerPaso(param);

        BaseOutputDto response = new BaseOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
      
        return new ResponseEntity<>(response, HttpStatus.OK);

       
    }

    @ApiOperation(value = "registro de datos generales local votación", notes = "Permite registrar los datos generales del local de votación del cuarto paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-datos-generales-local-votacion-cuarto-paso")
    public ResponseEntity<?> registrarDatosGeneralesLocalVotacionCuartoPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDatosGeneralesCuartoPasoLVInputDto paramInputDto) throws Exception {

        // Implement
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionA param = new RegistroSeccionA();
        RegistroSeccionAWrapper.RegistroDatosGeneralesCuartoPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioModificacion(numeroDocumento);
        registroSeccionAServicio.registrarDatosGeneralesCuartoPaso(param);

        BaseOutputDto response = new BaseOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
      
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "obtener datos generales del local votación", notes = "Permite obtener los datos generales del local de votación del segundo paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-datos-generales-local-votacion-primer-paso/{idRegistroSeccionA}")
    public ResponseEntity<?> obtenerDatosGeneralesLocalVotacionPrimerPaso(
            @PathVariable Integer idRegistroSeccionA) throws Exception {

        RegistroSeccionA param = new RegistroSeccionA();
        param.setIdRegistroSeccionA(idRegistroSeccionA);

        RegistroSeccionA paramOut = registroSeccionAServicio.obtenerDatosGeneralesPrimerPaso(param);

        ObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto paramDtoOut = RegistroSeccionAWrapper.ObtenerDatosGeneralesPrimerPasoModelToDtoWrapper(paramOut);

        DatosObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto datos = new DatosObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto();
        datos.setRegistroSeccionA(paramDtoOut);
        
        ResponseObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto response = new ResponseObtenerDatosGeneralesLocalVotacionPrimerPasoOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        response.setDatos(datos);
        
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "obtener datos generales del local votación", notes = "Permite obtener los datos generales del local de votación del segundo paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-datos-generales-local-votacion-segundo-paso/{idRegistroSeccionA}")
    public ResponseEntity<?> obtenerDatosGeneralesLocalVotacionSegundoPaso(
            @PathVariable Integer idRegistroSeccionA) throws Exception {

        RegistroSeccionA param = new RegistroSeccionA();
        param.setIdRegistroSeccionA(idRegistroSeccionA);

        RegistroSeccionA paramOut = registroSeccionAServicio.obtenerDatosGeneralesSegundoPaso(param);

        ObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto paramDtoOut = RegistroSeccionAWrapper.ObtenerDatosGeneralesSegundoPasoModelToDtoWrapper(paramOut);

        DatosObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto datos = new DatosObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto();
        datos.setRegistroSeccionA(paramDtoOut);
        
        ResponseObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto response = new ResponseObtenerDatosGeneralesLocalVotacionSegundoPasoOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        response.setDatos(datos);
        
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "obtener datos generales del local votación", notes = "Permite obtener los datos generales del local de votación del tercer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-datos-generales-local-votacion-tercer-paso/{idRegistroSeccionA}")
    public ResponseEntity<?> obtenerDatosGeneralesLocalVotacionTercerPaso(
            @PathVariable Integer idRegistroSeccionA) throws Exception {

        RegistroSeccionA param = new RegistroSeccionA();
        param.setIdRegistroSeccionA(idRegistroSeccionA);

        RegistroSeccionA paramOut = registroSeccionAServicio.obtenerDatosGeneralesTercerPaso(param);

        ObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto paramDtoOut = RegistroSeccionAWrapper.ObtenerDatosGeneralesTerceroPasoModelToDtoWrapper(paramOut);

        DatosObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto datos = new DatosObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto();
        datos.setRegistroSeccionA(paramDtoOut);
        
        ResponseObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto response = new ResponseObtenerDatosGeneralesLocalVotacionTercerPasoOutputDto();
        
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        response.setDatos(datos);
       

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "obtener datos generales del local votación", notes = "Permite obtener los datos generales del local de votación del cuarto paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-datos-generales-local-votacion-cuarto-paso/{idRegistroSeccionA}")
    public ResponseEntity<?> obtenerDatosGeneralesLocalVotacionCuartoPaso(
            @PathVariable Integer idRegistroSeccionA) throws Exception {

        RegistroSeccionA param = new RegistroSeccionA();
        param.setIdRegistroSeccionA(idRegistroSeccionA);

        RegistroSeccionA paramOut = registroSeccionAServicio.obtenerDatosGeneralesCuartoPaso(param);

        ObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto paramDtoOut = RegistroSeccionAWrapper.ObtenerDatosGeneralesCuartoPasoModelToDtoWrapper(paramOut);

        DatosObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto datos = new DatosObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto();
        datos.setRegistroSeccionA(paramDtoOut);
        
        ResponseObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto response = new ResponseObtenerDatosGeneralesLocalVotacionCuartoPasoOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
        

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registro de caracteristicas del local votación", notes = "Permite registrar las caracteristicas del local de votación del primer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarCaracteristicasPrimerPasoLVOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-caracteristicas-local-votacion-primer-paso")
    public ResponseEntity<?> registrarCaracteristicasPrimariasLocalVotacionPrimerPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarCaracteristicasPrimerPasoLVInputDto paramInputDto) throws Exception {

        // Implement
    	// recuperar el dni del token
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionB param = new RegistroSeccionB();
        RegistroSeccionBWrapper.RegistrarCaracteristicasPrimerPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioCreacion(numeroDocumento);
        registroSeccionBServicio.registrarCaracteristicasPrimerPaso(param);

        
        DatosRegistrarCaracteristicasPrimerPasoLVOutputDto datos = new DatosRegistrarCaracteristicasPrimerPasoLVOutputDto();
        datos.setIdRegistroSeccionB(param.getIdRegistroSeccionB());
        
        
        ResponseRegistrarCaracteristicasPrimerPasoLVOutputDto response = new ResponseRegistrarCaracteristicasPrimerPasoLVOutputDto();
        response.setDatos(datos);
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registro de caracteristicas del local votacion", notes = "Permite registrar las caracteristicas del local de votación del segundo paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/registrar-caracteristicas-local-votacion-segundo-paso")
    public ResponseEntity<?> registrarCaracteristicasLocalVotacionSegundoPaso(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarCaracteristicasSegundoPasoLVInputDto paramInputDto) throws Exception {

        // Implement
        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        RegistroSeccionB param = new RegistroSeccionB();
        RegistroSeccionBWrapper.RegistrarCaracteristicasSegundoPasoDtoToModelWrapper(paramInputDto, param);
        param.setUsuarioModificacion(numeroDocumento);
        param.setUsuarioCreacion(numeroDocumento);
        registroSeccionBServicio.registrarCaracteristicasSegundoPaso(param);

        BaseOutputDto response = new BaseOutputDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
       

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "obtener caracteristicas del local votación", notes = "Permite obtener las caracteristicas del local de votación del primer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-caracteristicas-local-votacion-primer-paso/{idRegistroSeccionB}")
    public ResponseEntity<?> obtenerCaracteristicasLocalVotacionPrimerPaso(
            @PathVariable Integer idRegistroSeccionB) throws Exception {

        RegistroSeccionB param = new RegistroSeccionB();
        param.setIdRegistroSeccionB(idRegistroSeccionB);

        RegistroSeccionB paramOut = registroSeccionBServicio.obtenerCaracteristicasPrimerPaso(param);

        ObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto paramDtoOut = RegistroSeccionBWrapper.ObtenerCaracteristicasPrimerPasoModelToDtoWrapper(paramOut);

        DatosObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto datos = new DatosObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto();
        datos.setRegistroSeccionB(paramDtoOut);
    
        ResponseObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto response = new ResponseObtenerCaracteristicasLocalVotacionPrimerPasoOutputDto();
        response.setDatos(datos);
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
    
       

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @ApiOperation(value = "obtener caracteristicas del local votación", notes = "Permite obtener las caracteristicas primarias del local de votación del primer paso")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/obtener-caracteristicas-local-votacion-segundo-paso/{idRegistroSeccionB}")
    public ResponseEntity<?> obtenerCaracteristicasLocalVotacionSegundoPaso(
            @PathVariable Integer idRegistroSeccionB) throws Exception {

        RegistroSeccionB param = new RegistroSeccionB();
        param.setIdRegistroSeccionB(idRegistroSeccionB);

        RegistroSeccionB paramOut = registroSeccionBServicio.obtenerCaracteristicasSegundoPaso(param);

        ObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto paramDtoOut = RegistroSeccionBWrapper.ObtenerCaracteristicasSegundoPasoModelToDtoWrapper(paramOut);

        HashMap<String, Object> resultDatos = new HashMap<>();
        resultDatos.put("RegistroSeccionB", paramDtoOut);

        DatosObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto datos = new DatosObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto();
        datos.setRegistroSeccionB(paramDtoOut);
        
        ResponseObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto response = new ResponseObtenerCaracteristicasLocalVotacionSegundoPasoOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "registrar aula", notes = "Permite registrar la información de una aula")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarDetRegistroSeccionCOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @PostMapping("/registrar-aula")
    public ResponseEntity<?> registrarAula(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDetRegistroSeccionCInputDto paramInputDto) throws Exception {

        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        DetRegistroSeccionCWrapper wrapper = new DetRegistroSeccionCWrapper();

        DetRegistroSeccionC param = wrapper.RegistrarDetSeccionCInputDtoToModelWrapper(paramInputDto);
        param.setUsuarioCreacion(numeroDocumento);

        detRegistroSeccionCServicio.registrarSeccionC(param);

        DatosRegistrarDetRegistroSeccionCOutputDto datos = new DatosRegistrarDetRegistroSeccionCOutputDto();
        datos.setIdAula(param.getIdDetRegistroSeccionC());
        
        ResponseRegistrarDetRegistroSeccionCOutputDto response = new ResponseRegistrarDetRegistroSeccionCOutputDto();
        response.setDatos(datos);
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "listar aula", notes = "Permite listar aulas por local de votación y/o verificación")
    
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionCOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/listar-aula")
    public ResponseEntity<?> listarAulas(
            @RequestBody @Valid final ListarRegistroSeccionCInputDto paramInputDto) throws Exception {

        DetRegistroSeccionC param = new DetRegistroSeccionC();
        param.setCabRegistroSeccionC(new CabRegistroSeccionC());
        param.getCabRegistroSeccionC().setVerificacionLV(new VerificacionLV());
        param.getCabRegistroSeccionC().getVerificacionLV().setIdVerificacionLV(paramInputDto.getIdVerificacion());


        detRegistroSeccionCServicio.listarSeccionC(param);

        List<ListarDetRegistroSeccionCOutputDto> outputDto = Funciones.mapAll(param.getRegistrosC(),ListarDetRegistroSeccionCOutputDto.class);

        DatosListarDetRegistroSeccionCOutputDto datos = new DatosListarDetRegistroSeccionCOutputDto();
        datos.setAulas(outputDto);
        
        ResponseListarDetRegistroSeccionCOutputDto response = new ResponseListarDetRegistroSeccionCOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
       return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @ApiOperation(value = "actualizar aula", notes = "Permite actualizar la informacion del aula")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @PostMapping("/actualizar-aula")
    public ResponseEntity<?> actualizarAula(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final ActualizarDetRegistroSeccionCInputDto paramInputDto) throws Exception {

        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        DetRegistroSeccionCWrapper wrapper = new DetRegistroSeccionCWrapper();

        DetRegistroSeccionC param = wrapper.ActualizarDetRegistroSeccionCInputDtoToModelWrapper(paramInputDto);
        param.setUsuarioCreacion(numeroDocumento);
        param.setUsuarioModificacion(numeroDocumento);

        detRegistroSeccionCServicio.actualizarSeccionC(param);
        
        BaseOutputDto response = new BaseOutputDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());

       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "listar areas", notes = "Permite listar las áres de un local de votacion, por id de verificación")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionDOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/listar-areas")
    public ResponseEntity<?> listarAreas(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final ListarRegistroSeccionDInputDto paramInputDto) throws Exception {

        DetRegistroSeccionD param = new DetRegistroSeccionD();
        param.setCabRegistroSeccionD(new CabRegistroSeccionD());
        param.getCabRegistroSeccionD().setVerificacionLV(new VerificacionLV());
        param.getCabRegistroSeccionD().getVerificacionLV().setIdVerificacionLV(paramInputDto.getIdVerificacion());
   
        detRegistroSeccionDServicio.listarSeccionD(param);

        List<ListarDetRegistroSeccionDOutputDto> outputDto = Funciones.mapAll(param.getRegistrosD(),
                ListarDetRegistroSeccionDOutputDto.class);
        
        DatosListarDetRegistroSeccionDOutputDto datos = new DatosListarDetRegistroSeccionDOutputDto();
        datos.setAreas(outputDto);
        
        ResponseListarDetRegistroSeccionDOutputDto response = new ResponseListarDetRegistroSeccionDOutputDto();
        response.setResultado(param.getResultado());
        response.setMensaje(param.getMensaje());
        response.setDatos(datos);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @ApiOperation(value = "registrar área", notes = "Permite registrar la información de una área del local de votación")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseRegistrarDetRegistroSeccionDOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @PostMapping("/registrar-area")
    public ResponseEntity<?> registrarArea(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final RegistrarDetRegistroSeccionDInputDto paramInputDto) throws Exception {

    	String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        DetRegistroSeccionDWrapper wrapper = new DetRegistroSeccionDWrapper();

        DetRegistroSeccionD param = wrapper.RegistrarDetSeccionDInputDtoToModelWrapper(paramInputDto);
        param.setUsuarioCreacion(numeroDocumento);

        detRegistroSeccionDServicio.registrarSeccionD(param);

        
        DatosRegistrarDetRegistroSeccionDOutputDto datos = new DatosRegistrarDetRegistroSeccionDOutputDto();
        datos.setIdArea(param.getIdDetRegistroSeccionD());
        
        ResponseRegistrarDetRegistroSeccionDOutputDto response = new ResponseRegistrarDetRegistroSeccionDOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "actualizar área", notes = "Permite actualizar la información de una área del local de votación")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @PostMapping("/actualizar-area")
    public ResponseEntity<?> actualizarArea(
            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
            @RequestBody @Valid final ActualizarDetRegistroSeccionDInputDto paramInputDto) throws Exception {

        String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

        DetRegistroSeccionDWrapper wrapper = new DetRegistroSeccionDWrapper();

        DetRegistroSeccionD param = wrapper.ActualizarDetSeccionDInputDtoToModelWrapper(paramInputDto);
        param.setUsuarioModificacion(numeroDocumento);

        detRegistroSeccionDServicio.actualizarSeccionD(param);

        BaseOutputDto response = new BaseOutputDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
       
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "listar locales de votacion activos", notes = "Permite listar locale de votación activos")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseLocalVotacionOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/listar-local-votacion-activos")
    public ResponseEntity<?> listarLVActivos(@Valid @RequestBody LocalVotacionInputDto paramInputDto) throws Exception {

        RegistroSeccionA param = Funciones.map(paramInputDto, RegistroSeccionA.class);
        MaeUbigeo ubigeo = new MaeUbigeo();
        ubigeo.setIdUbigeo(paramInputDto.getIdUbigeoDistrito());
        param.setUbigeo(ubigeo);
        param.setNombreLocalLV(paramInputDto.getNombre());
        MaeLocalVotacion local = new MaeLocalVotacion();
        local.setNombreLocal(paramInputDto.getNombre());
        local.setNumeroLocal(paramInputDto.getNumero());
        param.setMaeLocalVotacion(local);

        registroSeccionAServicio.ListarLVActivos(param);

        List<LocalVotacionOutputDto> outputDto = RegistroSeccionAWrapper.listarLvActivosModelToDtoWrapper(param.getRegistrosSeccionA());

       
        DatosLocalVotacionOutputDto datos = new DatosLocalVotacionOutputDto();
        datos.setLocalesVotaciones(outputDto);
        
        ResponseLocalVotacionOutputDto response = new ResponseLocalVotacionOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @ApiOperation(value = "filtrar locales de votacion activos", notes = "Permite filtrar locales de votación activos")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseLocalVotacionOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/listar-filtro-local-votacion-activos")
    public ResponseEntity<?> listarFiltroLVActivos(@RequestBody LocalVotacionInputDto paramInputDto) throws Exception {

        RegistroSeccionA param = Funciones.map(paramInputDto, RegistroSeccionA.class);

        registroSeccionAServicio.ListarLVActivos(param);

        List<LocalVotacionOutputDto> outputDto = Funciones.mapAll(param.getRegistrosSeccionA(), LocalVotacionOutputDto.class);

        DatosLocalVotacionOutputDto datos = new DatosLocalVotacionOutputDto();
        datos.setLocalesVotaciones(outputDto);
        
        ResponseLocalVotacionOutputDto response = new ResponseLocalVotacionOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    

    @ApiOperation(value = "enviar a validacion lv", notes = "Enviar a validacion el registro de un nuevo local de votación")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)

	})	
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )

    @PostMapping("/enviar-a-validacion-lv")
    public ResponseEntity<?> enviarAValidacionLV(
    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
    		@RequestBody @Valid final EnviarValidacionNuevoLVInputDto paramInputDto) throws Exception {
    	String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
        VerificacionLV param = new VerificacionLV();
        param.setIdVerificacionLV(paramInputDto.getIdVerificacion());
        param.setUsuarioModificacion(numeroDocumento);
        verificacionLVServicio.enviarAValidacionLV(param);
        BaseOutputDto response = new BaseOutputDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
		 
	@ApiOperation(value = "Agregar un archivo para la gestión de local de votación", notes = "Permite agregar un archivo para la gestión de local de votación")
	@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseSubirArchivoLvOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
		    @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)

	})		 
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@PostMapping(value = "/agregar-archivo-lv")
	public ResponseEntity<?> subido(
			@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			@RequestParam("file") MultipartFile file, 
			@RequestParam("nVerificacion") Integer nVerificacion,
			@RequestParam("seccion") String seccion, 
			@RequestParam("idCategoria") Integer idCategoria) {

    	ResponseSubirArchivoLvOutputDto response = new ResponseSubirArchivoLvOutputDto();
    	DatosArchivoLvSubidoOutputDto archivoLvResp = new DatosArchivoLvSubidoOutputDto();

		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		ConfArchivoFTP conf = new ConfArchivoFTP();
		conf.setMaxCharacters(50L);
		conf.setValidatedExtensions(new String[] { "zip", "rar", "pdf", "png", "jpg", "jpeg" });
		conf.setMaxSize(4194304L); // 4mb
		ArchivoFTP archivoFTP = new ArchivoFTP(file, conf);

		if (!archivoFTP.validLengthFileName()) {
			throw new CustomBussinessException(
					"Nombre de archivo muy extenso. Solo puede tener hasta 50 caracteres (incluido la extensión del archivo .jpg, .jpeg, .png, .pdf, .rar, .zip).");
		}

		if (!archivoFTP.validTypeFile()) {
			throw new CustomBussinessException(
					"Solo se permiten archivos con extensión .jpg, .jpeg, .png, .pdf, .rar o .zip.");
		}

		if (!archivoFTP.validSizeFile()) {
			throw new CustomBussinessException(
					"Tamaño de archivo no permitido. El límite de tamaño de archivo es de 4 MB.");
		}

		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setNVerificacion(nVerificacion);
		cabRegistroSeccionE.setIdCategoriaSeleccionada(idCategoria);
		cabRegistroSeccionE.setSeccion(seccion);
		
		ArchivoLV archivoLV = new ArchivoLV();
		archivoLV.setGuid(archivoFTP.getUUID());
		archivoLV.setFilename(archivoFTP.getFileName());
		archivoLV.setFilenameOriginal(archivoFTP.getFileNameGenerated());
		archivoLV.setExtension(archivoFTP.getFileExtension());
		archivoLV.setPesoStr(archivoFTP.getSize().toString());
		archivoLV.setUsuarioCreacion(numeroDocumento);
		archivoLV.setPath(archivoFTP.getPath());
		
		InputStream streamArchivo = null;
		try {
			streamArchivo = archivoFTP.getInputStream();
		} catch (IOException e) {
			throw new CustomException(e.getMessage());
		}
		
		archivoLV.setArchivoStream(streamArchivo);
		cabRegistroSeccionE.setArchivoAgregado(archivoLV);
		
		cabRegistroSeccionEServicio.agregarArchivo(cabRegistroSeccionE);
		
		archivoLV = cabRegistroSeccionE.getArchivoAgregado();
		
		archivoLvResp.setIdArchivo(archivoLV.getIdArchivo());
		archivoLvResp.setFilename(archivoLV.getFilename());
		archivoLvResp.setGuid(archivoLV.getGuid());
		archivoLvResp.setFormato(archivoLV.getExtension());
		archivoLvResp.setFilenameOriginal(archivoLV.getFilenameOriginal());
		archivoLvResp.setPeso(archivoLV.getPesoStr());
		archivoLvResp.setIdCabRegistroSeccionE(cabRegistroSeccionE.getIdCabRegistroSeccionE());
		response.setDatos(archivoLvResp);		
		response.setMensaje(cabRegistroSeccionE.getMensaje());
		response.setResultado(cabRegistroSeccionE.getResultado());		
		

		return new ResponseEntity<>(response, HttpStatus.OK);

	}
    
    @ApiOperation(value = "Listar los archivos cargados en gestión de local de votación por el número de verificación y seccion", notes = "Permite listar los archivos cargados en gestión de local de votación por el número de verificación y seccion")
    @ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarArchivosLvOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
    })
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/listar-archivos-lv/{seccion}/{nVerificacion}")
	public ResponseEntity<?> listarArchivoLv(@PathVariable("seccion") String seccion, @PathVariable("nVerificacion") Integer nVerificacion) {
	
    	CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setNVerificacion(nVerificacion);
		cabRegistroSeccionE.setSeccion(seccion);
    	
		cabRegistroSeccionEServicio.listarArchivos(cabRegistroSeccionE);
		
		ResponseListarArchivosLvOutputDto response = new ResponseListarArchivosLvOutputDto();
		response.setMensaje(cabRegistroSeccionE.getMensaje());
		response.setResultado(cabRegistroSeccionE.getResultado());
		
		List<DatosArchivoLvObtenidoOutputDto> datoArchivosLv = new ArrayList<DatosArchivoLvObtenidoOutputDto>();
		DatosArchivoLvObtenidoOutputDto datoArchivoLv = null;
		
		for(DetRegistroSeccionE registroSeccionE:cabRegistroSeccionE.getRegistrosDetSeccionE()) {
			datoArchivoLv = new DatosArchivoLvObtenidoOutputDto();
			datoArchivoLv.setIdArchivo(registroSeccionE.getArchivo().getIdArchivo());
			datoArchivoLv.setFilename(registroSeccionE.getArchivo().getFilename());
			datoArchivoLv.setFilenameOriginal(registroSeccionE.getArchivo().getFilenameOriginal());
			datoArchivoLv.setGuid(registroSeccionE.getArchivo().getGuid());
			datoArchivoLv.setFormato(registroSeccionE.getArchivo().getExtension());
			datoArchivoLv.setPeso(registroSeccionE.getArchivo().getPesoStr());
			datoArchivoLv.setCategoria(registroSeccionE.getArchivo().getCategoria());
			datoArchivosLv.add(datoArchivoLv);
		} // end-for
		
		response.setDatos(datoArchivosLv);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	/*
	@ApiOperation(value = "Listar los archivos cargados en gestión de local de votación por el número de verificación y seccion", notes = "Permite listar los archivos cargados en gestión de local de votación por el número de verificación y seccion")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseListarArchivosLvOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	    @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/listar-archivos-lv/{seccion}/{nVerificacion}")
	public ResponseEntity<?> listarArchivoLv(@PathVariable("seccion") String seccion, @PathVariable("nVerificacion") Integer nVerificacion) {
	
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setNVerificacion(nVerificacion);
		cabRegistroSeccionE.setSeccion(seccion);
		
		cabRegistroSeccionEServicio.listarArchivos(cabRegistroSeccionE);
		
		ResponseListarArchivosLvOutputDto response = new ResponseListarArchivosLvOutputDto();
		response.setMensaje(cabRegistroSeccionE.getMensaje());
		response.setResultado(cabRegistroSeccionE.getResultado());
		
		List<DatosArchivoLvObtenidoOutputDto> datoArchivosLv = new ArrayList<DatosArchivoLvObtenidoOutputDto>();
		DatosArchivoLvObtenidoOutputDto datoArchivoLv = null;
		
		for(DetRegistroSeccionE registroSeccionE:cabRegistroSeccionE.getRegistrosDetSeccionE()) {
			datoArchivoLv = new DatosArchivoLvObtenidoOutputDto();
			datoArchivoLv.setIdArchivo(registroSeccionE.getArchivo().getIdArchivo());
			datoArchivoLv.setFilename(registroSeccionE.getArchivo().getFilename());
			datoArchivoLv.setFilenameOriginal(registroSeccionE.getArchivo().getFilenameOriginal());
			datoArchivoLv.setGuid(registroSeccionE.getArchivo().getGuid());
			datoArchivoLv.setFormato(registroSeccionE.getArchivo().getExtension());
			datoArchivoLv.setPeso(registroSeccionE.getArchivo().getPesoStr());
			datoArchivoLv.setCategoria(registroSeccionE.getArchivo().getCategoria());
			datoArchivosLv.add(datoArchivoLv);
		} // end-for
		
		response.setDatos(datoArchivosLv);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	*/
	@ApiOperation(value = "Validar si se subieron todos los archivos obligatorio de una sección", notes = "Validar si se subieron todos los archivos obligatorio de una secciónn")
	@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseValidarSeccionEOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/validar-archivos-lv/{seccion}/{nVerificacion}")
	public ResponseEntity<?> validar(@PathVariable("seccion") String seccion, @PathVariable("nVerificacion") Integer nVerificacion) {
	
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setNVerificacion(nVerificacion);
		cabRegistroSeccionE.setSeccion(seccion);
		
		cabRegistroSeccionEServicio.validarRegistroSeccionE(cabRegistroSeccionE);

		ResponseValidarSeccionEOutputDto response = new ResponseValidarSeccionEOutputDto();
		DatosSeccionValidadoOutputDto datoValidado = new DatosSeccionValidadoOutputDto();
		datoValidado.setValidado(cabRegistroSeccionE.getValidado());
		response.setDatos(datoValidado);		
		response.setMensaje(cabRegistroSeccionE.getMensaje());
		response.setResultado(cabRegistroSeccionE.getResultado());
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Descargar un archivo subido en el ftp para la gestión de local de votación por el id del archivo", notes = "Permite descargar un archivo subido en el ftp para la gestión de local de votación por el id del archivo")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseDescargarArchivoLvOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/descargar-archivo-lv/{id}")
	public ResponseEntity<?> descargar(@PathVariable("id") Integer idArchivo) {
	
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setIdArchivoConsultado(idArchivo);
		cabRegistroSeccionEServicio.obtenerArchivoPorId(cabRegistroSeccionE);
		
		DatoArchivoLvDescargadoOutputDto archivoDescargado = new DatoArchivoLvDescargadoOutputDto();
		
		ArchivoLV archivoLv = null;
		if(cabRegistroSeccionE.getRegistrosDetSeccionE()!=null) {
	        	for(DetRegistroSeccionE registroSeccionE:cabRegistroSeccionE.getRegistrosDetSeccionE()) {
	            	if(registroSeccionE.getArchivo()!=null) {
	            		archivoLv = registroSeccionE.getArchivo();
	            		archivoDescargado.setIdArchivo(archivoLv.getIdArchivo());
	            		archivoDescargado.setFilename(archivoLv.getFilename());
	            		archivoDescargado.setFilenameOriginal(archivoLv.getFilenameOriginal());
	            		archivoDescargado.setGuid(archivoLv.getGuid());
	            		archivoDescargado.setFormato(archivoLv.getExtension());
	            		archivoDescargado.setFilenameOriginal(archivoLv.getFilenameOriginal());
	            		archivoDescargado.setPeso(archivoLv.getPesoStr());
	            		archivoDescargado.setCategoria(archivoLv.getCategoria());
	            	} // end-if
	    		} // end-for
	    } // end-if
		
		
		byte[] bytes = ftpService.downloadFileFromFTP(archivoLv.getPath(),archivoDescargado.getFilenameOriginal());
        String archivoBase64 = Base64.getEncoder().encodeToString(bytes);
        archivoDescargado.setFileBase64(archivoBase64);
        
        ResponseDescargarArchivoLvOutputDto response = new ResponseDescargarArchivoLvOutputDto();
        response.setMensaje(cabRegistroSeccionE.getMensaje());
        response.setResultado(cabRegistroSeccionE.getResultado());
        response.setDatos(archivoDescargado);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Obtener un archivo cargado en gestión de local de votación por el id del archivo", notes = "Permite obtener un archivo cargado en gestión de local de votación por el id del archivo")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerArchivoLvOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/obtener-archivo-lv/{id}")
	public ResponseEntity<?> obtenerArchivoPorId(@PathVariable("id") Integer idArchivo) {
	
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setIdArchivoConsultado(idArchivo);
		cabRegistroSeccionEServicio.obtenerArchivoPorId(cabRegistroSeccionE);
		
		ResponseObtenerArchivoLvOutputDto response = new ResponseObtenerArchivoLvOutputDto();
        response.setMensaje(cabRegistroSeccionE.getMensaje());
        response.setResultado(cabRegistroSeccionE.getResultado());
        
        DatosArchivoLvObtenidoOutputDto archivoObtenido = new DatosArchivoLvObtenidoOutputDto();
        
        if(cabRegistroSeccionE.getRegistrosDetSeccionE()!=null) {
        	for(DetRegistroSeccionE registroSeccionE:cabRegistroSeccionE.getRegistrosDetSeccionE()) {
            	if(registroSeccionE.getArchivo()!=null) {
            		archivoObtenido.setIdArchivo(registroSeccionE.getArchivo().getIdArchivo());
                	archivoObtenido.setFilename(registroSeccionE.getArchivo().getFilename());
                	archivoObtenido.setFilenameOriginal(registroSeccionE.getArchivo().getFilenameOriginal());
                	archivoObtenido.setGuid(registroSeccionE.getArchivo().getGuid());
                	archivoObtenido.setFormato(registroSeccionE.getArchivo().getExtension());
                	archivoObtenido.setFilenameOriginal(registroSeccionE.getArchivo().getFilenameOriginal());
                	archivoObtenido.setPeso(registroSeccionE.getArchivo().getPesoStr());
                	archivoObtenido.setCategoria(registroSeccionE.getArchivo().getCategoria());
            	} // end-if
    		} // end-for
        }

        response.setDatos(archivoObtenido);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

	@ApiOperation(value = "Actualizar observación de la cabecera de la sección E", notes = "Permite actualizar la observación de la cabecera de la sección E")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseActualizarObservacionSeccionEOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@PostMapping(value = "/validar-seccion-e")
	public ResponseEntity<?> validarSeccionE(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final ActualizarObservacionSeccionEInputDto paramInputDto) {
	
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setSeccion(paramInputDto.getSeccion());
		cabRegistroSeccionE.setNVerificacion(paramInputDto.getNverificacion());
		cabRegistroSeccionE.setObservacionRegistro(paramInputDto.getObservacionRegistro());
		cabRegistroSeccionE.setUsuarioCreacion(numeroDocumento);
		cabRegistroSeccionEServicio.actualizarObservacionReg(cabRegistroSeccionE);
		
        
		ResponseActualizarObservacionSeccionEOutputDto response = new ResponseActualizarObservacionSeccionEOutputDto();
        response.setMensaje(cabRegistroSeccionE.getMensaje());
        response.setResultado(cabRegistroSeccionE.getResultado());
        response.setObservado(cabRegistroSeccionE.getObservado());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@PostMapping(value = "/actualizar-observacion-seccion-e")
	public ResponseEntity<?> actualizarObservacionLvSeccionE(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid final ActualizarObservacionLvSeccionEInputDto paramInputDto) {
	
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		
		CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
		cabRegistroSeccionE.setSeccion(paramInputDto.getSeccion());
		cabRegistroSeccionE.setNVerificacion(paramInputDto.getNverificacion());
		cabRegistroSeccionE.setObservacionLv(paramInputDto.getObservacionLv());
		cabRegistroSeccionE.setUsuarioCreacion(numeroDocumento);
		cabRegistroSeccionEServicio.actualizarObservacionLv(cabRegistroSeccionE);
		
		DatosActualizarObservacionLvSeccionEOutputDto datos = new DatosActualizarObservacionLvSeccionEOutputDto();
		datos.setValidado(cabRegistroSeccionE.getValidado());
		datos.setIdCabRegistroSeccionE(cabRegistroSeccionE.getIdCabRegistroSeccionE());
		
		ResponseActualizarObservacionLvSeccionEOutputDto response = new ResponseActualizarObservacionLvSeccionEOutputDto();
        response.setMensaje(cabRegistroSeccionE.getMensaje());
        response.setResultado(cabRegistroSeccionE.getResultado());
        response.setDatos(datos);
        

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Obtener seccion E", notes = "Permite obtener los datos de la sección E")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ok", response = ResponseObtenerCabSeccionEOutputDto.class),
		@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
		@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
	})
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@GetMapping(value = "/obtener-seccion-e/{id}")
	public ResponseEntity<?> obtenerSeccionE(@PathVariable("id") Long idSeccion) {
	
		FiltroRegistroSeccionE filtro = new FiltroRegistroSeccionE();
		filtro.setIdCabRegistroSeccionE(idSeccion);
		cabRegistroSeccionEServicio.listarPorId(filtro);
		
		DatosCabSeccionEListadoOutputDto cabOut = null;
		List<DatosDetSeccionEListadoOutputDto> ldetOut = new ArrayList<DatosDetSeccionEListadoOutputDto>();
		DatosDetSeccionEListadoOutputDto detOut = null;
		DatosArchivoLvListadoOutputDto archOut = null;
		
		CabRegistroSeccionE cab = null;
		if(filtro.getRegistrosSeccionE()!=null && filtro.getRegistrosSeccionE().size()>0) {
			cabOut = new DatosCabSeccionEListadoOutputDto();
			cab = filtro.getRegistrosSeccionE().get(0);
			if(cab!=null) {
				cabOut.setIdCabRegistroSeccionE(cab.getIdCabRegistroSeccionE());
				cabOut.setSeccion(cab.getSeccion());
				cabOut.setNVerificacion(cab.getNVerificacion());
				cabOut.setObservacionLv(cab.getObservacionLv());
				cabOut.setObservacionRegistro(cab.getObservacionRegistro());
				cabOut.setEstadoVerificacion(cab.getEstadoVerificacion());
				cabOut.setActivo(cab.getActivo());
				if(cab.getRegistrosDetSeccionE()!=null) {
					for(DetRegistroSeccionE det: cab.getRegistrosDetSeccionE()) {
						detOut = new DatosDetSeccionEListadoOutputDto();
						detOut.setIdDetRegistroSeccionE(det.getIdDetRegistroSeccionE());
						if(det.getArchivo()!=null) {
							archOut = new DatosArchivoLvListadoOutputDto();
							archOut.setIdCategoria(det.getArchivo().getIdCategoria());
							archOut.setCategoria(det.getArchivo().getCategoria());
							archOut.setFormato(det.getArchivo().getExtension());
							archOut.setGuid(det.getArchivo().getGuid());
							archOut.setNombre(det.getArchivo().getFilename());
							archOut.setNombreOriginal(det.getArchivo().getFilenameOriginal());
							archOut.setPeso(det.getArchivo().getPesoStr());
						} // end-if
						detOut.setArchivo(archOut);
						ldetOut.add(detOut);
					} // end-for
				} // end-if
				cabOut.setDetalleSeccionE(ldetOut);
			}
		} // end-if
		
		ResponseObtenerCabSeccionEOutputDto response = new ResponseObtenerCabSeccionEOutputDto();
        response.setMensaje(filtro.getMensaje());
        response.setResultado(filtro.getResultado());
        response.setDatos(cabOut);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	   @ApiOperation(value = "listar registros de locales nuevos para editar", notes = "listar registros de locales nuevos para editar")
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/listar-editar-registro-nuevo-lv")
	    public ResponseEntity<?> listarEditarRegistroNuevoLV(@Valid @RequestBody ListarEditarRegistroNuevoLVInputDto paramInputDto) throws Exception {

	       
		   	VerificacionLV param = new VerificacionLV();
	       	VerificacionLVWrapper.ListarEditarRegistroNuevoLVDtoToModelWrapper(paramInputDto, param);
		   
	       	verificacionLVServicio.listarEditarNuevoLocalVotacion(param);

	        List<ListarEditarRegistroNuevoLVOutputDto> outputDto = VerificacionLVWrapper.ListarEditarRegistroNuevoLVModelToDtoWrapper(param.getVerificaciones());

	        
	        DatosListarEditarRegistroNuevoLVOutputDto datos = new DatosListarEditarRegistroNuevoLVOutputDto();
	        datos.setLocales(outputDto);
	        datos.setTotalRegistros(param.getVerificaciones().size()==0?0:param.getVerificaciones().get(0).getTotalRegistros());
	        
	        ResponseListarEditarRegistroNuevoLVOutputDto response = new ResponseListarEditarRegistroNuevoLVOutputDto();
	        response.setDatos(datos);
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());
	        
	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }
	   
	   
	   
	    @ApiOperation(value = "obtener caracteristicas de aula", notes = "Permite Obtener loas caracteristicas de aula")
	    @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionCOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/listar-caracteristicas-aula")
	    public ResponseEntity<?> listarCaracteristicasAula(
	            @RequestBody @Valid final ListarRegistroSeccionCInputDto paramInputDto) throws Exception {

	        DetRegistroSeccionC param = new DetRegistroSeccionC();
	        param.setCabRegistroSeccionC(new CabRegistroSeccionC());
	        param.getCabRegistroSeccionC().setVerificacionLV(new VerificacionLV());
	        param.getCabRegistroSeccionC().getVerificacionLV().setIdVerificacionLV(paramInputDto.getIdVerificacion());
	      
	        
	        detRegistroSeccionCServicio.listarCabeceraRegistroSeccionC(param.getCabRegistroSeccionC());
	        detRegistroSeccionCServicio.listarSeccionC(param);
	
	      
	       
	        List<ListarCaracteristicaAulaOutputDto> cabecera = Funciones.mapAll(param.getCabRegistroSeccionC().getSeccionC(), ListarCaracteristicaAulaOutputDto.class);
	        List<ListarDetRegistroSeccionCOutputDto> outputDto = Funciones.mapAll(param.getRegistrosC(),ListarDetRegistroSeccionCOutputDto.class);

	        DatosListarDetRegistroSeccionCOutputDto datos = new DatosListarDetRegistroSeccionCOutputDto();
	        if(cabecera != null && cabecera.size()>0) {
	        	datos.setCabecera(cabecera.get(0));
	        }
	        
	        datos.setAulas(outputDto);
	        
	        
	        ResponseListarDetRegistroSeccionCOutputDto response = new ResponseListarDetRegistroSeccionCOutputDto();
	        response.setDatos(datos);
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());
	        
	       return new ResponseEntity<>(response, HttpStatus.OK);

	    }
	    
	    @ApiOperation(value = "obtener caracteristicas de area", notes = "Permite Obtener loas caracteristicas de area")
	    @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionDOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @GetMapping("/obtener-caracteristicas-area/{idRegistroSeccionD}")
	    public ResponseEntity<?> obtenerCaracteristicasArea(@PathVariable("idRegistroSeccionD") Integer idRegistroSeccionD) throws Exception {

			DetRegistroSeccionD param = new DetRegistroSeccionD();
			param.setIdDetRegistroSeccionD(idRegistroSeccionD);
			
			CabRegistroSeccionD cb = new CabRegistroSeccionD();
			cb.setIdRegistroSeccionD(idRegistroSeccionD);
			param.setCabRegistroSeccionD(cb);

			detRegistroSeccionDServicio.listarCabeceraPorVerificacion(param.getCabRegistroSeccionD());
			detRegistroSeccionDServicio.listarSeccionD(param);

			List<ListarCaracteristicaAreaOutputDto> cabecera = Funciones.mapAll(param.getCabRegistroSeccionD().getSeccionD(), ListarCaracteristicaAreaOutputDto.class);
	        
			List<ListarDetRegistroSeccionDOutputDto> outputDto = Funciones.mapAll(param.getRegistrosD(),
					ListarDetRegistroSeccionDOutputDto.class);

			DatosListarDetRegistroSeccionDOutputDto datos = new DatosListarDetRegistroSeccionDOutputDto();
			 if(cabecera != null && cabecera.size()>0) {
		        	datos.setCabecera(cabecera.get(0));
		        }
			datos.setAreas(outputDto);

			ResponseListarDetRegistroSeccionDOutputDto response = new ResponseListarDetRegistroSeccionDOutputDto();
			response.setResultado(param.getResultado());
			response.setMensaje(param.getMensaje());
			response.setDatos(datos);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
  
	    
   @ApiOperation(value = "obtener caracteristicas de aula", notes = "Permite Obtener loas caracteristicas de aula")
	    
	    @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionCOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
   			@GetMapping("/obtener-caracteristicas-aula/{idRegistroSeccionC}")
	    public ResponseEntity<?> obtenerCaracteristicasAula(@PathVariable("idRegistroSeccionC")Integer idRegistroSeccionC) throws Exception {

	        DetRegistroSeccionC param = new DetRegistroSeccionC();
	        CabRegistroSeccionC cb = new CabRegistroSeccionC();
	        cb.setIdRegistroSeccionC(idRegistroSeccionC);
	        param.setCabRegistroSeccionC(cb);
	        
	        detRegistroSeccionCServicio.listarCabeceraRegistroSeccionC(param.getCabRegistroSeccionC());
	        detRegistroSeccionCServicio.listarSeccionC(param);

	        List<ListarCaracteristicaAulaOutputDto> cabecera = Funciones.mapAll(param.getCabRegistroSeccionC().getSeccionC(), ListarCaracteristicaAulaOutputDto.class);
	        List<ListarDetRegistroSeccionCOutputDto> outputDto = Funciones.mapAll(param.getRegistrosC(),ListarDetRegistroSeccionCOutputDto.class);

	        DatosListarDetRegistroSeccionCOutputDto datos = new DatosListarDetRegistroSeccionCOutputDto();
	        if(cabecera != null && cabecera.size()>0) {
	        	datos.setCabecera(cabecera.get(0));
	        }
	        
	        datos.setAulas(outputDto);
	        
	        
	        ResponseListarDetRegistroSeccionCOutputDto response = new ResponseListarDetRegistroSeccionCOutputDto();
	        response.setDatos(datos);
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());
	        
	       return new ResponseEntity<>(response, HttpStatus.OK);

	    }
	    
	    @ApiOperation(value = "obtener caracteristicas de area", notes = "Permite Obtener loas caracteristicas de area")
	    @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseListarDetRegistroSeccionDOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/listar-caracteristicas-area")
	    public ResponseEntity<?> listarCaracteristicasArea(
	            @RequestBody @Valid final ListarRegistroSeccionDInputDto paramInputDto) throws Exception {

			DetRegistroSeccionD param = new DetRegistroSeccionD();
			param.setCabRegistroSeccionD(new CabRegistroSeccionD());
			param.getCabRegistroSeccionD().setVerificacionLV(new VerificacionLV());
			param.getCabRegistroSeccionD().getVerificacionLV().setIdVerificacionLV(paramInputDto.getIdVerificacion());
			

			detRegistroSeccionDServicio.listarCabeceraPorVerificacion(param.getCabRegistroSeccionD());
			detRegistroSeccionDServicio.listarSeccionD(param);

			List<ListarCaracteristicaAreaOutputDto> cabecera = Funciones.mapAll(param.getCabRegistroSeccionD().getSeccionD(), ListarCaracteristicaAreaOutputDto.class);
	        
			List<ListarDetRegistroSeccionDOutputDto> outputDto = Funciones.mapAll(param.getRegistrosD(),
					ListarDetRegistroSeccionDOutputDto.class);

			DatosListarDetRegistroSeccionDOutputDto datos = new DatosListarDetRegistroSeccionDOutputDto();
			 if(cabecera != null && cabecera.size()>0) {
		        	datos.setCabecera(cabecera.get(0));
		        }
			datos.setAreas(outputDto);

			ResponseListarDetRegistroSeccionDOutputDto response = new ResponseListarDetRegistroSeccionDOutputDto();
			response.setResultado(param.getResultado());
			response.setMensaje(param.getMensaje());
			response.setDatos(datos);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	    
	    
	    
	    
	    
	    
  
	  @ApiOperation(value = "Eliminar aula", notes = "Permite eliminar un aula de un local de votacion por id ")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @DeleteMapping("/eliminar-aula/{idDetRegistroSeccionC}")
	  public ResponseEntity<?> eliminarAula(
	          @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			  @PathVariable("idDetRegistroSeccionC") Integer idDetRegistroSeccionC) throws Exception {

          String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		  DetRegistroSeccionC param = new DetRegistroSeccionC();
		  param.setIdDetRegistroSeccionC(idDetRegistroSeccionC);
          param.setUsuarioCreacion(numeroDocumento);
          param.setUsuarioModificacion(numeroDocumento);
	      detRegistroSeccionCServicio.eliminarAula(param);
	
	      BaseOutputDto response = new BaseOutputDto();
	      response.setResultado(param.getResultado());
	      response.setMensaje(param.getMensaje());
	     
	      return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
  
  
	  @ApiOperation(value = "Eliminar área", notes = "Permite eliminar una área de un local de votacion por id ")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @DeleteMapping("/eliminar-area/{idDetRegistroSeccionD}")
	  public ResponseEntity<?> eliminarArea(
			  @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			  @PathVariable("idDetRegistroSeccionD") Integer idDetRegistroSeccionD) throws Exception {

          String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		  DetRegistroSeccionD param = new DetRegistroSeccionD();
		  param.setIdDetRegistroSeccionD(idDetRegistroSeccionD);
          param.setUsuarioModificacion(numeroDocumento);
	      detRegistroSeccionDServicio.eliminarArea(param);
	
	      BaseOutputDto response = new BaseOutputDto();
	      response.setResultado(param.getResultado());
	      response.setMensaje(param.getMensaje());
	     
	      return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
  
	  @ApiOperation(value = "obtener aula", notes = "permite obtener aula")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseObtenerAulaOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @GetMapping("/obtener-aula/{idDetRegistroSeccionC}")
	  public ResponseEntity<?> obtenerAula ( @PathVariable("idDetRegistroSeccionC") Integer idAula) throws Exception {
	
	      DetRegistroSeccionC param = new DetRegistroSeccionC();
	      param.setIdDetRegistroSeccionC(idAula);
	
	      detRegistroSeccionCServicio.listarSeccionC(param);
	
	      List<ListarDetRegistroSeccionCOutputDto> outputDto = Funciones.mapAll(param.getRegistrosC(),ListarDetRegistroSeccionCOutputDto.class);
	
	      DatoObtenerAulaOutputDto datos = new DatoObtenerAulaOutputDto();
	      datos.setAula(outputDto != null && outputDto.size()>0 ? outputDto.get(0):null);
	      
	      ResponseObtenerAulaOutputDto response = new ResponseObtenerAulaOutputDto();
	      response.setDatos(datos);
	      response.setMensaje(param.getMensaje());
	      response.setResultado(param.getResultado());
	      
	     return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
  

	  @ApiOperation(value = "obtener area", notes = "Permite obtener area")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseObtenerAreaOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @GetMapping("/obtener-area/{idDetRegistroSeccionD}")
	  public ResponseEntity<?> obtenerArea(@PathVariable("idDetRegistroSeccionD") Integer idArea) throws Exception {
	
	      DetRegistroSeccionD param = new DetRegistroSeccionD();
	      param.setIdDetRegistroSeccionD(idArea);
	      
	      detRegistroSeccionDServicio.listarSeccionD(param);
	
	      List<ListarDetRegistroSeccionDOutputDto> outputDto = Funciones.mapAll(param.getRegistrosD(),
	              ListarDetRegistroSeccionDOutputDto.class);
	      
	      DatoObtenerAreaOutputDto datos = new DatoObtenerAreaOutputDto();
	      datos.setArea(outputDto != null && outputDto.size()>0 ? outputDto.get(0):null);
	      
	      ResponseObtenerAreaOutputDto response = new ResponseObtenerAreaOutputDto();
	      response.setResultado(param.getResultado());
	      response.setMensaje(param.getMensaje());
	      response.setDatos(datos);
	      return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
	  
	  
	  @ApiOperation(value = "listar registros de locales nuevos para validar", notes = "listar registros de locales nuevos para validar")
	    @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseListarEditarRegistroNuevoLVOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/listar-validar-registro-nuevo-lv")
	    public ResponseEntity<?> listarValidarRegistroNuevoLV(@RequestBody @Valid ListarEditarRegistroNuevoLVInputDto paramInputDto) throws Exception {

	       
		   VerificacionLV param = new VerificacionLV();
	       VerificacionLVWrapper.ListarEditarRegistroNuevoLVDtoToModelWrapper(paramInputDto, param);
		   
		   verificacionLVServicio.listarValidarNuevoLocalVotacion(param);

	        List<ListarValidarRegistroNuevoLVOutputDto> outputDto = VerificacionLVWrapper.ListarValidarRegistroNuevoLVModelToDtoWrapper(param.getVerificaciones());

	        
	        DatosListarValidarRegistroNuevoLVOutputDto datos = new DatosListarValidarRegistroNuevoLVOutputDto();
	        datos.setLocales(outputDto);
	        datos.setTotalRegistros(param.getVerificaciones().size()==0?0:param.getVerificaciones().get(0).getTotalRegistros());
	                
	            
	        ResponseListarValidarRegistroNuevoLVOutputDto response = new ResponseListarValidarRegistroNuevoLVOutputDto();
	        response.setDatos(datos);
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());
	        
	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }
	  
	  
	  @ApiOperation(value = "obtener datos generales de local votacion", notes = "permite obtener los datos generales del local de votación, para su respectiva validación")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseObtenerDatosGeneralesLocalVotacionOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @GetMapping("/obtener-datos-generales-lv/{idRegistroSeccionA}")
	  public ResponseEntity<?> obtenerDatosGeneralesLV ( @PathVariable("idRegistroSeccionA") Integer idRegistroSeccionA) throws Exception {
	
	      RegistroSeccionA param = new RegistroSeccionA();
	      param.setIdRegistroSeccionA(idRegistroSeccionA);
	
	     RegistroSeccionA obtenido =   registroSeccionAServicio.obtenerDatosGenerales(param);
	
	      ObtenerDatosGeneralesLocalVotacionOutputDto outputDto  = RegistroSeccionAWrapper.ObtenerDatosGeneralesModelToDtoWrapper(obtenido);
	
	      DatosObtenerDatosGeneralesLocalVotacionOutputDto datos = new DatosObtenerDatosGeneralesLocalVotacionOutputDto();
	      datos.setRegistroSeccionA(outputDto);
	      
	      ResponseObtenerDatosGeneralesLocalVotacionOutputDto response = new ResponseObtenerDatosGeneralesLocalVotacionOutputDto();
	      response.setDatos(datos);
	      response.setMensaje(param.getMensaje());
	      response.setResultado(param.getResultado());
	      
	     return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
	  
	  @ApiOperation(value = "obtener características de local votacion", notes = "permite obtener las características del local de votación, para su respectiva validación")
	  @ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseObtenerCaracteristicasLocalVotacionOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	      @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})	
	  @ApiImplicitParams(
	          @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	  )
	  @GetMapping("/obtener-caracteristicas-lv/{idRegistroSeccionB}")
	  public ResponseEntity<?> obtenerCaracteristicasLV ( @PathVariable("idRegistroSeccionB") Integer idRegistroSeccionB) throws Exception {
	
	      RegistroSeccionB param = new RegistroSeccionB();
	      param.setIdRegistroSeccionB(idRegistroSeccionB);
	
	     RegistroSeccionB registroObtenido = registroSeccionBServicio.obtenerCaracteristicas(param);
	      
	     ObtenerCaracteristicasLocalVotacionOutputDto outputDto  = RegistroSeccionBWrapper.ObtenerCaracteristicasModelToDtoWrapper(registroObtenido);
	
	      DatosObtenerCaracteristicasLocalVotacionOutputDto datos = new DatosObtenerCaracteristicasLocalVotacionOutputDto();
	      datos.setRegistroSeccionB(outputDto);
	      
	      ResponseObtenerCaracteristicasLocalVotacionOutputDto  response = new ResponseObtenerCaracteristicasLocalVotacionOutputDto();
	      response.setDatos(datos);
	      response.setMensaje(param.getMensaje());
	      response.setResultado(param.getResultado());
	      
	     return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }
	  
	  
	  
		@ApiOperation(value = "Validar registro sección A", notes = "Permite actualizar la observacion del registro de la sección A")
		@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/validar-seccion-a")
		public ResponseEntity<?> validarSeccionA(
				@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
				@RequestBody @Valid final ValidarRegistroSeccionAInputDto paramInputDto) {
		
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			
			RegistroSeccionA param = new RegistroSeccionA();
			param.setIdRegistroSeccionA(paramInputDto.getIdRegistroSeccionA());
			param.setObservacionRegistro(paramInputDto.getObservacionRegistro());
			param.setUsuarioModificacion(numeroDocumento);
			
			VerificacionLV verificacion = new VerificacionLV();
			verificacion.setIdVerificacionLV(paramInputDto.getIdVerificacionLv());
			param.setVerificacionLV(verificacion);
			
			registroSeccionAServicio.validarSeccionA(param);
			
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	  
	  @ApiOperation(value = "validar registro sección B", notes = "Permite actualizar la observacion del registro de la sección B")
		@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/validar-seccion-b")
		public ResponseEntity<?> validarSeccionB(
				@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
				@RequestBody @Valid final ValidarRegistroSeccionBInputDto paramInputDto) {
		
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			RegistroSeccionB param = new RegistroSeccionB();
			param.setIdRegistroSeccionB(paramInputDto.getIdRegistroSeccionB());
			param.setObservacionRegistro(paramInputDto.getObservacionRegistro());
			param.setUsuarioModificacion(numeroDocumento);
			
			VerificacionLV verificacion = new VerificacionLV();
			verificacion.setIdVerificacionLV(paramInputDto.getIdVerificacionLv());
			param.setVerificacionLV(verificacion);
			
			registroSeccionBServicio.validarSeccionB(param);
			
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	  
	  @ApiOperation(value = "validar registro sección C", notes = "Permite actualizar la observacion del registro de la sección C")
		@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = BaseOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/validar-seccion-c")
		public ResponseEntity<?> validarSeccionC(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,@RequestBody @Valid final ValidarRegistroSeccionCInputDto paramInputDto) {
		
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			
			CabRegistroSeccionC param = new CabRegistroSeccionC();
			param.setIdRegistroSeccionC(paramInputDto.getIdCabRegistroSeccionC());
			param.setObservacionRegistro(paramInputDto.getObservacionRegistro());
			param.setUsuarioModificacion(numeroDocumento);
			
			detRegistroSeccionCServicio.validarSeccionC(param);
			
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	  
	   @ApiOperation(value = "validar registro sección D", notes = "Permite actualizar la observacion del registro de la sección D")
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/validar-seccion-D")
		public ResponseEntity<BaseOutputDto> validarSeccionD(
				@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
				@RequestBody @Valid final ValidarRegistroSeccionDInputDto paramInputDto) {
		
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			CabRegistroSeccionD param = new CabRegistroSeccionD();
			param.setIdRegistroSeccionD(paramInputDto.getIdCabRegistroSeccionD());
			VerificacionLV verificacionLV = new VerificacionLV();
			verificacionLV.setIdVerificacionLV(paramInputDto.getIdVerificacionLv());
			param.setVerificacionLV(verificacionLV);
			param.setObservacionRegistro(paramInputDto.getObservacionRegistro());
			param.setUsuarioModificacion(numeroDocumento);
			
			detRegistroSeccionDServicio.validarSeccioD(param);
			
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	  
	 
	    @ApiOperation(value = "Actualizar características del local de votación del primer paso", notes = "Permite actualizar las características del local de votación del primer paso")
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/actualizar-caracteristicas-local-votacion-primer-paso")
		public ResponseEntity<BaseOutputDto> actualizarCaracteristicasLocalVotacionPrimerPaso(
				@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
				@RequestBody @Valid final ActualizarCaracteristicasLVPrimerPasoInputDto paramInputDto) {
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			RegistroSeccionB param = new RegistroSeccionB();
			RegistroSeccionBWrapper.ActualizarRegistroSeccionBPrimerPasoDtoToModelWrapper(paramInputDto,param);
            param.setUsuarioModificacion(numeroDocumento);
			registroSeccionBServicio.actualizarCaracteristicaPrimerPaso(param);
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	   
	    @ApiOperation(value = "Actualiza caracteristicas del local votacion del segundo paso", notes = "Permite actualizar las caracteristicas del local de votación del segundo paso")
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/actualizar-caracteristicas-local-votacion-segundo-paso")
	    public ResponseEntity<?> actualizarCaracteristicasLocalVotacionSegundoPaso(
	    		@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
	            @RequestBody @Valid final RegistrarCaracteristicasSegundoPasoLVInputDto paramInputDto)  {

	        // Implement
	        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
	        RegistroSeccionB param = new RegistroSeccionB();
	        RegistroSeccionBWrapper.RegistrarCaracteristicasSegundoPasoDtoToModelWrapper(paramInputDto, param);
	        param.setUsuarioModificacion(numeroDocumento);
	        registroSeccionBServicio.registrarCaracteristicasSegundoPaso(param);

	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());
	       

	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }

	    @ApiOperation(value = "Actualiza de datos generales local votación del primer paso", notes = "Permite actualizar los datos generales del local de votación del primer paso")	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PostMapping("/actualizar-datos-generales-local-votacion-primer-paso")
	    public ResponseEntity<BaseOutputDto> actualizarDatosGeneralesLocalVotacionPrimerPaso(
	            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
	            @RequestBody @Valid final ActualizarDatosGeneralesPrimerPasoLVInputDto paramInputDto) throws Exception {

	        // Implement
	        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");

	        RegistroSeccionA param = new RegistroSeccionA();
	        RegistroSeccionAWrapper.ActualizarDatosGeneralesPrimerPasoDtoToModelWrapper(paramInputDto, param);
	        
	        param.setUsuarioModificacion(numeroDocumento);
	        registroSeccionAServicio.actualizarDatosGeneralesPrimerPaso(param);

	        BaseOutputDto response = new BaseOutputDto();
	        response.setResultado(param.getResultado());
	        response.setMensaje(param.getMensaje());
	      

	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }
		
		@ApiOperation(value = "Actualiza de datos generales local votación del segundo paso", notes = "Permite actualizar los datos generales del local de votación del segundo paso")
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
	    @PutMapping("/actualizar-datos-generales-local-votacion-segundo-paso")
	    public ResponseEntity<BaseOutputDto> actualizarDatosGeneralesLocalVotacionSegundoPaso(
	            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
	            @RequestBody @Valid final RegistrarDatosGeneralesSegundoPasoLVInputDto paramInputDto) throws Exception {

	        // Implement
	        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
	        RegistroSeccionA param = new RegistroSeccionA();
	        RegistroSeccionAWrapper.RegistroDatosGeneralesSegundoPasoDtoToModelWrapper(paramInputDto, param);
	        param.setUsuarioModificacion(numeroDocumento);
	        registroSeccionAServicio.registrarDatosGeneralesSegundoPaso(param);

	        BaseOutputDto response = new BaseOutputDto();
	        response.setResultado(param.getResultado());
	        response.setMensaje(param.getMensaje());
	      
	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }	
		
		@ApiOperation(value = "Actualiza de datos generales local votación del tercer paso", notes = "Permite actualizar los datos generales del local de votación del tercer paso")	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
		@PutMapping("/actualizar-datos-generales-local-votacion-tercer-paso")
	    public ResponseEntity<BaseOutputDto> actualizarDatosGeneralesLocalVotacionTercerPaso(
	            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
	            @RequestBody @Valid final RegistrarDatosGeneralesTercerPasoLVInputDto paramInputDto) throws Exception {

	        // Implement
	        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
	        RegistroSeccionA param = new RegistroSeccionA();
	        RegistroSeccionAWrapper.RegistroDatosGeneralesTercerPasoDtoToModelWrapper(paramInputDto, param);
	        param.setUsuarioModificacion(numeroDocumento);
	        registroSeccionAServicio.registrarDatosGeneralesTercerPaso(param);

	        BaseOutputDto response = new BaseOutputDto();
	        response.setResultado(param.getResultado());
	        response.setMensaje(param.getMensaje());
	      
	        return new ResponseEntity<>(response, HttpStatus.OK);

	       
	    }	
		
		
		@ApiOperation(value = "Actualiza de datos generales local votación del cuarto paso", notes = "Permite actualizar de datos generales local votación del primer paso los datos generales del local de votación del cuarto paso")	
	    @ApiImplicitParams(
	            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	    )
		@PutMapping("/actualizar-datos-generales-local-votacion-cuarto-paso")
	    public ResponseEntity<BaseOutputDto> actualizarDatosGeneralesLocalVotacionCuartoPaso(
	            @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
	            @RequestBody @Valid final RegistrarDatosGeneralesCuartoPasoLVInputDto paramInputDto) throws Exception {

	        // Implement
	        String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
	        RegistroSeccionA param = new RegistroSeccionA();
	        RegistroSeccionAWrapper.RegistroDatosGeneralesCuartoPasoDtoToModelWrapper(paramInputDto, param);
	        param.setUsuarioModificacion(numeroDocumento);
	        registroSeccionAServicio.registrarDatosGeneralesCuartoPaso(param);

	        BaseOutputDto response = new BaseOutputDto();
	        response.setResultado(param.getResultado());
	        response.setMensaje(param.getMensaje());
	      
	        return new ResponseEntity<>(response, HttpStatus.OK);
	    }

		
		@ApiOperation(value = "Eliminar un archivo lv", notes = "Permite actualizar el estado de activado a desactivado de un archivo del FTP")
		@ApiResponses({
			@ApiResponse(code = 200, message = "ok", response = ResponseEliminarArchivoLvOutputDto.class),
			@ApiResponse(code = 401, message = "Unauthorized", response = ExceptionResponse.class),
			@ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
			@ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
	        @ApiResponse(code = 500, message = "Internal server error", response = ExceptionResponse.class)
		})
		@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
		@GetMapping(value = "/eliminar-archivo-lv/{id}")
		public ResponseEntity<?> eliminar(
                @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
                @PathVariable("id") Integer idArchivo) {

            String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			CabRegistroSeccionE cabRegistroSeccionE = new CabRegistroSeccionE();
			cabRegistroSeccionE.setIdArchivoConsultado(idArchivo);
            cabRegistroSeccionE.setUsuarioModificacion(numeroDocumento);
			cabRegistroSeccionEServicio.desactivarArchivo(cabRegistroSeccionE);
			
			ResponseEliminarArchivoLvOutputDto response = new ResponseEliminarArchivoLvOutputDto();
			response.setResultado(cabRegistroSeccionE.getResultado());
			response.setMensaje(cabRegistroSeccionE.getMensaje());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

		
		@ApiOperation(value = "listar categoria de archivos", notes = "Permite listar las categorias de archivos de la seccion E")
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@GetMapping(value = { "/listar-categoria-archivo-seccion-e", "/listar-categoria-archivo-seccion-e/{idVerificacionLV}" })
		public ResponseEntity<?> listarCategoriaArchivoSeccionE(@PathVariable(name = "idVerificacionLV", required = false) Integer idCabRegistroSeccionE) {
		
			CabRegistroSeccionE cabecera = new CabRegistroSeccionE();
			VerificacionLV verificacion = new VerificacionLV();
			verificacion.setIdVerificacionLV(idCabRegistroSeccionE);
			cabecera.setVerificacionLv(verificacion);
			
			cabRegistroSeccionEServicio.listarCategoriaArchivoSeccionE(cabecera);
			
			List<ListarCategoriaArchivoSeccionEOutputDto> lista = RegistroSeccionEWrapper.obtenerListarCategoriaArchivoSeccionEOutputDto(cabecera.getRegistrosDetSeccionE());
			
			DatosListarCategoriaArchivoSeccionEOuputDto datos = new DatosListarCategoriaArchivoSeccionEOuputDto();
			datos.setCatalogos(lista);
			
			ResponseListarCategoriaArchivoSeccionEOuputDto response = new ResponseListarCategoriaArchivoSeccionEOuputDto();
			response.setDatos(datos);
			response.setMensaje(cabecera.getMensaje());
		    response.setResultado(cabecera.getResultado());
		        
		

			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		
		 
		@ApiOperation(value = "Validar registro local de votacion", notes = "Permite validar el registro de un nuevo del local de votación ")
		@ApiImplicitParams(
				@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
		)
		@PostMapping(value = "/validar-registro-nuevo-lv")
		public ResponseEntity<BaseOutputDto> validarRegistroNuevoLV(
				@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
				@RequestBody @Valid final ValidarRegistroAulaInputDto paramInputDto) throws Exception{
		
			String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
			VerificacionLV param = new VerificacionLV();
			param.setIdVerificacionLV(paramInputDto.getIdVerificacionLv());
			param.setUsuarioModificacion(numeroDocumento);
			
			
			verificacionLVServicio.validarNuevoRegistroLV(param);
			
	        BaseOutputDto response = new BaseOutputDto();
	        response.setMensaje(param.getMensaje());
	        response.setResultado(param.getResultado());

			return new ResponseEntity<BaseOutputDto>(response, HttpStatus.OK);
		}
	

		 
	@ApiOperation(value = "Validar verificacion de  local de votacion", notes = "Permite validar la verificacion de un local de votación ")
	@ApiImplicitParams(
			@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	)
	@PostMapping(value = "/validar-verificacion-lv")
	public ResponseEntity<BaseOutputDto> validarVerificacionLv( @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,@RequestBody @Valid final ValidarRegistroAulaInputDto paramInputDto) throws Exception{
			
		String numeroDocumento = (String)jwtTokenProvider.obtenerValorToken(token, "numeroDocumento");
		VerificacionLV param = new VerificacionLV();
		param.setIdVerificacionLV(paramInputDto.getIdVerificacionLv());
		param.setUsuarioModificacion(numeroDocumento);

		verificacionLVServicio.validarVerificacionLV(param);
				
		BaseOutputDto response = new BaseOutputDto();
		response.setMensaje(param.getMensaje());
		response.setResultado(param.getResultado());

		return new ResponseEntity<BaseOutputDto>(response, HttpStatus.OK);
	}
		
		
		
    @ApiOperation(value = "Lista de locales de votación historico para su verificacion",
        notes = "Permite listar los locales de votación historico para su verificacion"
    ) 
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @PostMapping(value = "/listar-filtro-local-votacion-verificacion")
    public ResponseEntity<ResultConsultarLvOutputDto> listarFiltroLocalLocalVotacionVerificacion(
            @RequestBody @Valid final ListarFiltroLocalVotacionVInputDto paramDto
    ) throws Exception {

        VerificacionLV param = new VerificacionLV();
        VerificacionLVWrapper.ListarLocalVotacionVerificacionDtoToModelWrapper(paramDto, param);

        verificacionLVServicio.listarLocalVotacionVerificacion(param);
        
        VerificacionLV param1 = new VerificacionLV();
        VerificacionLVWrapper.ListarLocalVotacionVerificacionDtoToModelWrapper(paramDto, param1);
        
        verificacionLVServicio.totalLocalesVotacion(param1);
        
        ObtenerListaFiltroLocalVotacionOutputDto outDto = VerificacionLVWrapper.ObtenerListaFiltroLocalVotacionVerificadoModelToDtoWrapper(param.getVerificaciones(), param1);
        
        DatosConsultarLvOutputDto datos = new DatosConsultarLvOutputDto();
        datos.setLocales(outDto);
            
        ResultConsultarLvOutputDto result = new ResultConsultarLvOutputDto();
        result.setResultado(param.getResultado());
        result.setMensaje(param.getMensaje());
        result.setDatos(datos);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    
    @ApiOperation(value = "Lista de locales de votación historico para su validacion",
        notes = "Permite listar los locales de votación historico para su validacion"
    ) 
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization",
                    value = "Access Token",
                    required = true,
                    allowEmptyValue = false,
                    paramType = "header",
                    dataTypeClass = String.class,
                    example = "Bearer access_token")
    )
    @PostMapping(value = "/listar-filtro-local-votacion-validacion")
    public ResponseEntity<ResultConsultarLvOutputDto> listarFiltroLocalLocalVotacionValidacion(
            @RequestBody @Valid final ListarFiltroLocalVotacionVInputDto paramDto
    ) throws Exception {

        VerificacionLV param = new VerificacionLV();
        VerificacionLVWrapper.ListarLocalVotacionVerificacionDtoToModelWrapper(paramDto, param);

        verificacionLVServicio.listarLocalVotacionValidacion(param);
       
        VerificacionLV param1 = new VerificacionLV();
        VerificacionLVWrapper.ListarLocalVotacionVerificacionDtoToModelWrapper(paramDto, param1);
        
        verificacionLVServicio.totalLocalesValidacion(param1);
        
        ObtenerListaFiltroLocalVotacionOutputDto outDto = VerificacionLVWrapper.ObtenerListaFiltroLocalVotacionVerificadoModelToDtoWrapper(param.getVerificaciones(), param1);
        
        DatosConsultarLvOutputDto datos = new DatosConsultarLvOutputDto();
        datos.setLocales(outDto);
        datos.setTotalRegistros(param.getVerificaciones().size()==0?0:param.getVerificaciones().get(0).getTotalRegistros());
  	  
        ResultConsultarLvOutputDto result = new ResultConsultarLvOutputDto();
        result.setResultado(param.getResultado());
        result.setMensaje(param.getMensaje());
        result.setDatos(datos);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @ApiOperation(value = "Actualizar en registro seccion D a terminado/no terminado", notes = "Actualizar en registro seccion D a terminado/no terminado")
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
	@PostMapping("/actualizar-estado-terminado-seccion-d")
	public ResponseEntity<?> actualizarEstadoTerminadoSeccionD(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid ActualizarTerminadoSeccionDInputDto paramInputDto) throws Exception {

		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		
		CabRegistroSeccionD param = new CabRegistroSeccionD();
		VerificacionLV verificacion = new VerificacionLV();
		verificacion.setIdVerificacionLV(paramInputDto.getIdVerificacion());
		param.setVerificacionLV(verificacion);
		param.setTerminado(paramInputDto.getTerminado());
		param.setUsuarioModificacion(numeroDocumento);
		registroSeccionDServicio.actualizarEstadoTerminado(param);

		ActualizarTerminadoSeccionDOutputDto response = new ActualizarTerminadoSeccionDOutputDto();
		response.setMensaje(param.getMensaje());
		response.setResultado(param.getResultado());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
    
    
    @DeleteMapping("/cancelar-registro-verificacion/{idVerificacionLV}")
	  public ResponseEntity<?> cancelarRegistroVerificacion(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = true) String token,
			  @PathVariable("idVerificacionLV") Integer idVerificacionLV) throws Exception {
	
	      
		 VerificacionLV param = new VerificacionLV();
		 param.setIdVerificacionLV(idVerificacionLV);
	
	      verificacionLVServicio.cancelarRegistroVerificacion(param);
	
	      BaseOutputDto response = new BaseOutputDto();
	      response.setResultado(param.getResultado());
	      response.setMensaje(param.getMensaje());
	     
	      return new ResponseEntity<>(response, HttpStatus.OK);
	
	  }


    @ApiOperation(value = "Obtiene los estados Terminado de todas las secciones A,B,C,D y E", notes = "Obtiene los estados Terminado de todas las secciones A,B,C,D y E")
    @ApiImplicitParams(
        @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/obtener-estado-terminado-nuevo-lv")
    public ResponseEntity<?> obtenerEstadoTerminadoNuevoLocal(@Valid @RequestBody EstadoTerminadoNuevoLocalInputDto paramInputDto) throws Exception {
        return new ResponseEntity<>(verificacionLVServicio.obtenerEstadoTerminadoNuevoLocal(paramInputDto), HttpStatus.OK);
    }
}

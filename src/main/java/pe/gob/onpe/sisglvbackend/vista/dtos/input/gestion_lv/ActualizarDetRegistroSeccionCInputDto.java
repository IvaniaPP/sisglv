package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;




import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ActualizarDetRegistroSeccionCInputDto {
	
	@NotNull(message = "ID Registro Seccion C es obligatorio")
	private Integer idDetRegistroSeccionC;
	
	@NotBlank(message = "Número de aula es obligatorio")
	private String aula;
	@NotBlank(message = "Pabellón es obligatorio")
	private String pabellon;
	@NotNull(message = "Número de piso es obligatorio")
	private Integer piso;
	@NotNull(message = "Requiere Toldo es obligatorio")
	private Integer requiereToldo;
	
	private Integer cantidadMesa;
	@NotNull(message = "Tipo de aula es obligatorio")
	private Integer tipoAula;
	
	@NotNull(message = "Tiene luminaria es obligatorio")
	private Integer tieneLuminaria;
	private Integer cantidadLuminariaBuenEstado;
	private Integer cantidadLuminariaMalEstado;
	
	@NotNull(message = "Tiene tomacorriente es obligatorio")
	private Integer tieneTomaCorriente;
	private Integer cantidadTomacorrienteBuenEstado;
	private Integer cantidadTomacorrienteMalEstado;
	
	@NotNull(message = "Tiene interruptor es obligatorio")
	private Integer tieneInterruptor;
	private Integer cantidadInterruptorBuenEstado;
	private Integer cantidadInterruptorMalEstado;
	
	@NotNull(message = "Tiene puerta es obligatorio")
	private Integer tienePuerta;
	private Integer cantidadPuertaBuenEstado;
	private Integer cantidadPuertaMalEstado;
	
	@NotNull(message = "Tiene ventana es obligatorio")
	private Integer tieneVentana;
	private Integer cantidadVentanaBuenEstado;
	private Integer cantidadVentanaMalEstado;
	
	@NotNull(message = "Tiene punto de internet es obligatorio")
	private Integer tienePuntoInternet;
	private Integer tienePuntoWifi;
	
	private Integer activo;
	
}

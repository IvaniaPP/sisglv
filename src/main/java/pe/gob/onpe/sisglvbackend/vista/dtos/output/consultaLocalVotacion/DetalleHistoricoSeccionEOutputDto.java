package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetalleHistoricoSeccionEOutputDto {

    private Integer idDetFichaSeccionE;
    private Integer archivo;
    private Integer categoria;
    private String detalle;

}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.reporte;

import lombok.Getter;
import lombok.Setter;

/**
 * @author glennlq
 * @created 1/6/22
 */
@Getter
@Setter
public class ReporteSeccionABOutputDto {
	private String codigoHistoricoLV;
	private String nombreLv;
	private String codigoMinedu;
	private String dniRegistrador;
	private String nombreApellidoRegistrador;
	private String nombreUUOO;
	private String ambitoGeografico;
	private String ubigeo;
	private String departamento;
	private String provincia;
	private String distrito;
	private String tipoDistrito;
	private String latitudDistrito;
	private String longitudDistrito;
	private String centroPoblado;
	private String tipoVia;
	private String nombreVia;
	private String direccionNumero;
	private String km;
	private String mz;
	private String dep;
	private String lt;
	private String piso;
	private String tipoZona;
	private String nombreZona;
	private String referencia;
	private String latitudPuertaAcceso;
	private String longitudPuertaAcceso;
	private Double distanciaCentroDistrito;
	private String tiempoCentroDistrito;
	private String tipoLv;
	private String numeroLv;
	private String nivelInstruccion;
	private String tipoInstitucion;
	private String categoriaInstitucion;
	private String otraCategoriaInstitucion;
	private String dniRepresentanteLv;
	private String nombreApellidoRepresentanteLv;
	private String cargoRepresentateLv;
	private String telefonoRepresentanteLv;
	private String emailRepresentanteLv;
	private String dniRepresentanteApafa;
	private String nombreApellidoRepresentanteApafa;
	private String telefonoRepresentanteApafa;
	private Integer cantidadAulaDisponible;
	private Integer cantidaMesaDisponible;
	private Integer solucionTecnologica;
	private String direccionInterior;
	private Integer esNuevo;
	private String seccionB;
	private String tieneCercoPerimetrico;
	private String estadoCercoPerimetrico;
	private Double areaAproximada;
	private Double areaConstruida;
	private Double areaSinConstruir;
	private Integer cantidadPuertasAcceso;
	private String estadoPuertaAcceso;
	private Integer cantidadSshh;
	private String estadoSshh;
	private Integer cantidadCirculosSeguridad;
	private String tieneAgua;
	private String horarioInicioAgua;
	private String horarioTerminoAgua;
	private String tieneLuz;
	private String horarioInicioLuz;
	private String horarioTerminoLuz;
	private String suministroLuz;
	private String tieneInternet;
	private String tienePatio;
	private Integer cantidadPatio;
	private String estadoTechoAula;
	private String estadoParedAula;
	private String estadoPisoAula;
	private String estadoPuertaAula;
	private String estadoVentanaAula;
	private String proveedorInternet;
}

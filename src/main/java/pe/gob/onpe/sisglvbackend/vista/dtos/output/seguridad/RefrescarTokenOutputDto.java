package pe.gob.onpe.sisglvbackend.vista.dtos.output.seguridad;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.BaseInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class RefrescarTokenOutputDto extends BaseOutputDto  {
	
	private String token;

}

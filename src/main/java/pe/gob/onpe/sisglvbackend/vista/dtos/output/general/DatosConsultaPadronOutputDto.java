package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosConsultaPadronOutputDto {
	
	private ConsultaPadronNombreOutputDto persona;

}

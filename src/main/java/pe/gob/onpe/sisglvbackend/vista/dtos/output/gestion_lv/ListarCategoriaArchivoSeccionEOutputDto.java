package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarCategoriaArchivoSeccionEOutputDto {
	
	private Integer codigo;
	private String  nombre;
	private Integer  activo;
	private Integer orden;
	private Integer obligatorio;
	private Long idDetRegistroSeccionE;
	
	
}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.habilitarProcesoVerificacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosRegistrarProcesoVerificacionOutputDto {
	
	private Integer idProcesoVerificacion;

}

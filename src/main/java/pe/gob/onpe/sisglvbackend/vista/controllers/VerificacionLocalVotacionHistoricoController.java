package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import pe.gob.onpe.sisglvbackend.negocio.modelos.CabRegistroSeccionC;
import pe.gob.onpe.sisglvbackend.negocio.modelos.VerificacionLV;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarEstadoDisponibleInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ActualizarTerminadoSeccionCInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv.ListarEditarRegistroNuevoLVInputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ActualizarTerminadoSeccionCOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.DatosListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseActualizarEstadoDisponibleDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv.ResponseListarEditarRegistroNuevoLVOutputDto;
import pe.gob.onpe.sisglvbackend.vista.wrapper.VerificacionLVWrapper;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.RegistroSeccionCServicio;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.VerificacionLVServicio;
import pe.gob.onpe.sisglvbackend.seguridad.jwt.JWTTokenProvider;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/local-votacion-historico")
@Validated
public class VerificacionLocalVotacionHistoricoController {
	
	@Autowired
	private JWTTokenProvider jwtTokenProvider;
	
	@Autowired
	RegistroSeccionCServicio registroSeccionCServicio;
	
	@Autowired
    private VerificacionLVServicio verificacionLVServicio;

	@ApiOperation(value = "listar registros de locales para verificar", notes = "listar registros de locales para verificar")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @PostMapping("/listar-verificacion-lv")
    public ResponseEntity<?> listarVerificacionLV(@Valid @RequestBody ListarEditarRegistroNuevoLVInputDto paramInputDto) throws Exception {
	   	VerificacionLV param = new VerificacionLV();
       	VerificacionLVWrapper.ListarEditarRegistroNuevoLVDtoToModelWrapper(paramInputDto, param);
       	verificacionLVServicio.listarEditarVerificacionLocalVotacion(param);
        List<ListarEditarRegistroNuevoLVOutputDto> outputDto = VerificacionLVWrapper.ListarEditarRegistroNuevoLVModelToDtoWrapper(param.getVerificaciones());
        DatosListarEditarRegistroNuevoLVOutputDto datos = new DatosListarEditarRegistroNuevoLVOutputDto();
        datos.setLocales(outputDto);
        datos.setTotalRegistros(param.getVerificaciones().size()==0?0:param.getVerificaciones().get(0).getTotalRegistros());
        ResponseListarEditarRegistroNuevoLVOutputDto response = new ResponseListarEditarRegistroNuevoLVOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	
	@PostMapping("/actualizar-estado-disponible")
    public ResponseEntity<?> actualizarEstadoDisponible(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
    		@Valid @RequestBody ActualizarEstadoDisponibleInputDto paramInputDto) throws Exception {
		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		VerificacionLV param = new VerificacionLV();
		param.setIdVerificacionLV(paramInputDto.getIdVerificacion());
		param.setEstadoDisponible(paramInputDto.getIdEstadoDisponible());
		param.setObsEstadoDisponible(paramInputDto.getObservacion());
		param.setUsuarioModificacion(numeroDocumento);
		verificacionLVServicio.actualizarEstadoDisponiblel(param);
		ResponseActualizarEstadoDisponibleDto response = new ResponseActualizarEstadoDisponibleDto();
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	@ApiOperation(value = "Actualizar en registro seccion C a terminado/no terminado", notes = "Actualizar en registro seccion C a terminado/no terminado")
	@ApiImplicitParams(@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token"))
	@PostMapping("/actualizar-estado-terminado-seccion-c")
	public ResponseEntity<?> actualizarEstadoTerminado(
			@RequestHeader(value = HttpHeaders.AUTHORIZATION) String token,
			@RequestBody @Valid ActualizarTerminadoSeccionCInputDto paramInputDto) throws Exception {

		String numeroDocumento = (String) jwtTokenProvider.obtenerValorToken(token,"numeroDocumento");
		
		CabRegistroSeccionC param = new CabRegistroSeccionC();
		param.setIdRegistroSeccionC(paramInputDto.getIdRegistroSeccionC());
		param.setTerminado(paramInputDto.getTerminado());
		param.setUsuarioModificacion(numeroDocumento);
		registroSeccionCServicio.actualizarEstadoTerminado(param);

		ActualizarTerminadoSeccionCOutputDto response = new ActualizarTerminadoSeccionCOutputDto();
		response.setMensaje(param.getMensaje());
		response.setResultado(param.getResultado());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	
}

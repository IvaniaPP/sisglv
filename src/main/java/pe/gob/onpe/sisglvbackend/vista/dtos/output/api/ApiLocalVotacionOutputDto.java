package pe.gob.onpe.sisglvbackend.vista.dtos.output.api;

import lombok.*;

/**
 * @author glennlq
 * @created 10/21/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiLocalVotacionOutputDto {
    String codigo;
    String ubigeo;
    String nombre;
    String direccion;
    String mesas;
    String electores;
    String ccpp;
    String latitud;
    String longitud;
    String referencia;
    String tipoSolucion;
    String mesasEspeciales;
}

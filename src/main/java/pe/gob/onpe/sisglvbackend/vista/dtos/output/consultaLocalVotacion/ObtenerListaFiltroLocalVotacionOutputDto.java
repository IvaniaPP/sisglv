package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Setter
@Getter
public class ObtenerListaFiltroLocalVotacionOutputDto extends BaseOutputDto{

    private List<DatosLocalesVotacionOutputDto> datosLocalesVotacionOutputDto = new ArrayList<>();
    private Integer localesRegistrando;
    private Integer localesValidados;
    private Integer localesPorValidar;
    private Integer localesObservados;
    private Integer totalDeLocales;

}

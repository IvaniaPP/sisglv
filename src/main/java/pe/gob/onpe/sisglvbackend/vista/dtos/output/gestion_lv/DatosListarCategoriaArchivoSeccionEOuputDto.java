package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosListarCategoriaArchivoSeccionEOuputDto {
	
	List<ListarCategoriaArchivoSeccionEOutputDto> catalogos;
}

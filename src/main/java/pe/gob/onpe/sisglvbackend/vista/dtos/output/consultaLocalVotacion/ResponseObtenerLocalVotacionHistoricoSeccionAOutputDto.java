package pe.gob.onpe.sisglvbackend.vista.dtos.output.consultaLocalVotacion;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
@Schema(name = "ResponseObtenerLocalVotacionHistoricoSeccionAOutputDto", description = "The entity that represents Episode")
public class ResponseObtenerLocalVotacionHistoricoSeccionAOutputDto extends BaseOutputDto {
    DatosObtenerLocalVotacionHistoricoSeccionAOutputDto datos;
}

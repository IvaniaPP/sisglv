package pe.gob.onpe.sisglvbackend.vista.dtos.output.gestion_lv;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarCaracteristicaAreaOutputDto {
	
	private Integer idRegistroSeccionD;
	private Date fechaObservacion;
	private String observacionRegistro;
	private String usuarioObservacion;
	private Integer estadoVerificacion;

}

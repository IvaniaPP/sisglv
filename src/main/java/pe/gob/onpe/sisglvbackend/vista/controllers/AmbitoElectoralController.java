package pe.gob.onpe.sisglvbackend.vista.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import pe.gob.onpe.sisglvbackend.negocio.modelos.MaeAmbitoElectoral;
import pe.gob.onpe.sisglvbackend.negocio.servicios.interfaces.MaeAmbitoElectoralServicio;
import pe.gob.onpe.sisglvbackend.transversal.constantes.ConstanteApi;
import pe.gob.onpe.sisglvbackend.transversal.utilidades.Funciones;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.DatosListarAmbitoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ListarAmbitoElectoralOutputDto;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.general.ResponseListarAmbitoElectoralOutputDto;


//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(ConstanteApi.VERSION_API + "/api/interno/ambito-electoral")
@Validated
public class AmbitoElectoralController {

    @Autowired
    MaeAmbitoElectoralServicio maeAmbitoElectoralServicio;

    @ApiOperation(value = "listar ambito electoral ODPE/ORC", notes = "Permite listar los ambitos electorales"
    		+ "\n0 = TODOS"
    		+ "\n1 = ODPE"
    		+ "\n2 = ORC"
    		)
     @ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-ambito-electoral-activos/{tipoAmbitoElectoral}")
    public ResponseEntity<?> listaAmbitoElectoralActivos(@PathVariable Optional<Integer> tipoAmbitoElectoral) throws Exception {

        MaeAmbitoElectoral param = new MaeAmbitoElectoral();
        if(tipoAmbitoElectoral.isPresent()) {
        	 param.setTipoAmbitoElectoral(tipoAmbitoElectoral.get());
        }
       
        
        maeAmbitoElectoralServicio.listarAmbitoPorTipo(param);

        List<ListarAmbitoElectoralOutputDto> outputDto = Funciones.mapAll(param.getLista(), ListarAmbitoElectoralOutputDto.class);

        DatosListarAmbitoElectoralOutputDto datos = new DatosListarAmbitoElectoralOutputDto();
        datos.setAmbitos(outputDto);
        
        ResponseListarAmbitoElectoralOutputDto response = new ResponseListarAmbitoElectoralOutputDto();
        response.setDatos(datos);
        response.setMensaje(param.getMensaje());
        response.setResultado(param.getResultado());
        
       return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    
    @ApiOperation(value = "listar ambito electoral por proceso electoral", notes = "Permite listar los ambitos electorales por proceso electoral")
    		@ApiImplicitParams(
            @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    )
    @GetMapping("/listar-ambito-electoral-por-proceso/{tipoProceso}/{idProcesoEletoral}")
    public ResponseEntity<?> listaAmbitoElectoralPorProceso(@PathVariable("tipoProceso") Integer tipoProceso, @PathVariable("idProcesoEletoral") Integer idProceso) throws Exception {

        
        
        List<MaeAmbitoElectoral> lista = maeAmbitoElectoralServicio.listarAmbitoElectoralPorTipoYProceso(idProceso,tipoProceso);

        List<ListarAmbitoElectoralOutputDto> outputDto = Funciones.mapAll(lista, ListarAmbitoElectoralOutputDto.class);

        DatosListarAmbitoElectoralOutputDto datos = new DatosListarAmbitoElectoralOutputDto();
        datos.setAmbitos(outputDto);
        
        ResponseListarAmbitoElectoralOutputDto response = new ResponseListarAmbitoElectoralOutputDto();
        response.setDatos(datos);
        response.setMensaje("Se ejecutó la operación correctamente");
        response.setResultado(1);
        
       return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

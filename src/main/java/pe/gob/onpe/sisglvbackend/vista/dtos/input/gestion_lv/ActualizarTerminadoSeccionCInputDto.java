package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarTerminadoSeccionCInputDto {

	private Integer idRegistroSeccionC;
	private Integer terminado;
	
}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.asignacionProcesoElectoral;

import lombok.*;

/**
 * @author glennlq
 * @created 9/27/21
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AsignarOdpeAProcesoElectoralOutputDto {
    private boolean exito;
    private String mensaje;
}

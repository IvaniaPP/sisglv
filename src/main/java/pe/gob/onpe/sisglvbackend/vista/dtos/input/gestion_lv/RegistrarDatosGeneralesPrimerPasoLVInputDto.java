package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrarDatosGeneralesPrimerPasoLVInputDto {
	
	//private Integer idTipoAmbitoGeografico;
	
	@NotNull(message = "Id Tipo Proceso es obligatorio")
	private Integer idTipoProceso;
	
	//@NotNull(message = "Id Proceso Electoral es obligatorio")
	private Integer idProcesoElectoral;
	
	//@NotNull(message = "Id Proceso Verificación es obligatorio")
	private Integer idProcesoVerificacion;
	
	@NotNull(message = "Id Ubigeo es obligatorio")
	private Integer idUbigeo;
	
	
	private Integer idTipoLocal;
	
	@Size(max = 300, message = "Nombre de Local no puede tener mas de 300 caracteres")
	@NotBlank(message = "Nombre Local es obligatorio")
	private String nombreLocal;
	
	private String numeroLocal;	
	
		
	@NotNull(message = "Id Ambito Electoral es obligatorio")
	private Integer idAmbitoElectoral;
	
	private Integer idTipoAmbitoElectoral;
	
	private Integer idCentroPoblado;

}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EjecutarResultadoProspectivaDto {

	private Long idProyeccion;
	private List<EjecutarResultadoProspectivaNacionalDto> prospectivaNacional;
	private List<EjecutarResultadoProspectivaExtranjeroDto> prospectivaExtranjero;
	
}

package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarEditarRegistroNuevoLVInputDto  {
	
	
	@NotNull(message = "Tipo de proceso es un campos requerido")
	private Integer tipoProceso;
	
	@NotNull(message = "idProceso es un campo requerido")
	private Integer idProceso;
	
	private Integer idUbigeoDepartamento;
	private Integer idUbigeoProvincia;
	private Integer idUbigeoDistrito;
	
	private Integer idCentroPoblado;
	
	private Integer idAmbitoElectoral;
	
	@NotNull(message = "pagina es un campo requerido")
	private Integer pagina;
	@NotNull(message = "total de registros por pagina es un campo requerido")
    private Integer totalRegistroPorPagina;
	
	private String nombreLocal;


}

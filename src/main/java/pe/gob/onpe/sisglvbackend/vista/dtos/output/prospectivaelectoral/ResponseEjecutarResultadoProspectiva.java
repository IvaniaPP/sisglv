package pe.gob.onpe.sisglvbackend.vista.dtos.output.prospectivaelectoral;

import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseEjecutarResultadoProspectiva extends BaseOutputDto {

	private EjecutarResultadoProspectivaDto datos;
	
}

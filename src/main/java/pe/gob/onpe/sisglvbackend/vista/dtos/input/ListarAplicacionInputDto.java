package pe.gob.onpe.sisglvbackend.vista.dtos.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListarAplicacionInputDto {
	private String nombre;
	private String descripcion;
	private String codigo;
	private String estado;
	private Integer idUnidadOrganica;
}

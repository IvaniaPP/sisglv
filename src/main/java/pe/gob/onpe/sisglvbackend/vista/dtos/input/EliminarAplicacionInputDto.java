package pe.gob.onpe.sisglvbackend.vista.dtos.input;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EliminarAplicacionInputDto {
	@NotNull(message = "El campo id de aplicacion usuario no puede ser nulo")
	private Integer idAplicacionUsuario;
}

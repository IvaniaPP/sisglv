package pe.gob.onpe.sisglvbackend.vista.dtos.input.gestion_lv;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarDatosGeneralesPrimerPasoLVInputDto {
	
	
	@NotNull(message = "Id registro seccion A")
	private Integer idRegistroSeccionA;
	
	
	@NotNull(message = "Id Verificacion es obligatorio")
	private Integer idVerificacion;

	@NotNull(message = "Id Ubigeo es obligatorio")
	private Integer idUbigeo;
	
	

	private Integer idTipoLocal;
	
	
	@NotBlank(message = "Nombre Local es obligatorio")
	private String nombreLocal;
	
	private String numeroLocal;	

	private Integer idCentroPoblado;

}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarCentroPobladoBasicoOutputDto {
	private Integer idUbigeo;
//	private String ubigeo;
	private String nombre;
	//private String activo;
	
}

package pe.gob.onpe.sisglvbackend.vista.dtos.output.general;


import lombok.Getter;
import lombok.Setter;
import pe.gob.onpe.sisglvbackend.vista.dtos.output.BaseOutputDto;

@Getter
@Setter
public class ResponseListarCentroPobladoBasicoOuputDto extends BaseOutputDto{
	
	private DatosListarCentroPobladoBasicoOutputDto datos;
}

package pe.gob.onpe.sisglvbackend.transversal.properties;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Properties;

@Component
@Log4j2
public class FtpProperties {
	@Resource(mappedName = "resource/sisglv_properties")
	private Properties properties;

	private String ftpIpServer;
	private String ftpUsuario;
	private String ftpPassword;
	private String ftpPort;
	private String ftpDirhome;

	public String getFtpIpServer() {
		return this.getProperty("FTP_IPSERVER");
	}

	public String getFtpUsuario() {
		return this.getProperty("FTP_USUARIO");
	}

	public String getFtpPassword() {
		return this.getProperty("FTP_PASSWORD");
	}

	public String getFtpPort() {
		return this.getProperty("FTP_PORT");
	}

	public String getFtpDirhome() {
		return this.getProperty("FTP_DIRHOME");
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}

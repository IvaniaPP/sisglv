package pe.gob.onpe.sisglvbackend.transversal.excepciones;

public class NotFoundRegisterInDb extends Exception {
    public NotFoundRegisterInDb(String table, String column, Object value) {
        super("El registro "+column+"="+value.toString()+" , no existe en la tabla:"+table);
    }
}

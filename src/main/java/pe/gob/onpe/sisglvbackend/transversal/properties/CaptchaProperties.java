package pe.gob.onpe.sisglvbackend.transversal.properties;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CaptchaProperties {
	@Resource(mappedName = "resource/sisglv_properties")
	private Properties properties;

	private String site;
	private String secret;

	// reCAPTCHA V3
	private String siteV3;
	private String secretV3;
	private float threshold;

	public String getSite() {
		return this.getProperty("google.recaptcha.key.site");
	}

	public String getSecret() {
		return this.getProperty("google.recaptcha.key.secret");
	}

	public float getThreshold() {
		return Float.parseFloat(this.getProperty("google.recaptcha.key.threshold"));
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}



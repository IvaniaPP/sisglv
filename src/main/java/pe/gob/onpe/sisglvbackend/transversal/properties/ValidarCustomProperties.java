package pe.gob.onpe.sisglvbackend.transversal.properties;

import java.util.Properties;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ValidarCustomProperties {

	@Resource(mappedName = "resource/sisglv_properties")
	private Properties properties;

	private Boolean servicioCaptcha;

	public Boolean getServicioCaptcha() {
		return Boolean.parseBoolean(this.getProperty("validar.custom.servicio-captcha"));
	}

	private String getProperty(String key) {
		Object value = properties.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
}

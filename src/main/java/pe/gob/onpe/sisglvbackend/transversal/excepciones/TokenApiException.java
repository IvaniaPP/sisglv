package pe.gob.onpe.sisglvbackend.transversal.excepciones;

import org.springframework.http.HttpStatus;

/**
 * @author glennlq
 * @created 10/26/21
 */
public class TokenApiException extends RuntimeException {
    private final String message;
    private final HttpStatus httpStatus;

    public TokenApiException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }


}
